Attribute VB_Name = "SQL"
Option Explicit
Option Compare Text
Global DB As ADODB.Recordset

Public Function OpenSQL(sqlCmd$, Optional forWrite As Boolean) As Integer

   Dim strConnStr$, myCursorType As Integer, myLockType As Integer
   
   If forWrite = True Then
      myCursorType = adOpenDynamic
      myLockType = adLockOptimistic
   Else
      myCursorType = adOpenKeyset
      myLockType = adLockReadOnly
   End If
   
   Set DB = New ADODB.Recordset
   strConnStr = "Provider=SQLOLEDB;Initial Catalog=RISKdb;" & "Data Source=dbserver;User Id=yuri;Password=yuri;"
On Error GoTo ErrHand
   DB.Open sqlCmd$, strConnStr, myCursorType, myLockType, adCmdText
On Error GoTo 0
   OpenSQL = 0
   Exit Function
   
ErrHand:
   MsgBox Err.Description & Chr$(10) & "Error #" & Err.Number, vbCritical, "(QF) SQL Parser Error !"
   OpenSQL = -1 'Err.Number
   On Error GoTo 0

End Function

Public Sub CloseSQL()
On Error Resume Next
   DB.Close
   Set DB = Nothing
On Error GoTo 0
End Sub

