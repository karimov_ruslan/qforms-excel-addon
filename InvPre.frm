VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} InvPre 
   Caption         =   "InvPre"
   ClientHeight    =   4545
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8250
   OleObjectBlob   =   "InvPre.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "InvPre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Option Explicit
Option Compare Text

Private Sub cmdACT_Click()
   trAnswer = True
   cmdCancel_Click
End Sub

Private Sub cmdCancel_Click()
   Me.Hide
   Unload Me
End Sub


Private Sub UserForm_Initialize()
   
   Dim xCur As Integer
      
   InitVariables
   If trMode = 2 Then GoTo PrevPreview:
   
   mInvDate.Caption = Format(trDate, "dd/MM/yyyy")
   If DateDiff("m", trDate, Now) >= 12 Then
      iDate.Caption = "Invoice Date [VERY OLD!] :"
      iDate.ForeColor = &HC0&
      mInvDate.ForeColor = &HC0&
   End If
   
   mInvNo.Caption = trInvNo
   mTIN.Caption = trTIN
   mCustomer.Caption = trCName$
   mTotal.Caption = Format(trTotal, defNumb$)
   mVAT.Caption = Format(trTax, defNumb$)
   mGoods.Caption = Format(trCostGoods, defNumb$)
   mServices.Caption = Format(trCostServices, defNumb$)
   mGT.Caption = Format(trGT, defNumb$)
   
   xCur = CurrentCurrency()
   cur1.Caption = crSign$(xCur, 1)
   cur2.Caption = crSign$(xCur, 1)
   cur3.Caption = crSign$(xCur, 1)
   cur4.Caption = crSign$(xCur, 1)
   cur5.Caption = crSign$(xCur, 1)
   
   cmdACT.Caption = "Publish!"
   msg.Caption = "You're about to publish Invoice to DataBase." + Chr$(10) + "Are You Sure ?"
   InvPre.Caption = "(QF) Invoice Preview. [Prior to Publishing]"
   trAnswer = False
   
   'trCID'


PrevPreview: '--------------------------------------------------


End Sub
