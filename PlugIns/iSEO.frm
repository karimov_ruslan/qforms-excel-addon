VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} iSEO 
   Caption         =   "SEO Issuer"
   ClientHeight    =   5820
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6270
   OleObjectBlob   =   "iSEO.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "iSEO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub addGoods_Change()
   addGoods_Click
End Sub

Private Sub addGoods_Click()
   HasAdditionalGoods = addGoods.Value
End Sub

Private Sub ap1_Change()
   ap1_Click
End Sub

Private Sub ap1_Click()
   If ap1.Value = True Then
      PaidAs = 2
      PaidCurr = 0
      PaidPercent = 0
      apvFrame.Enabled = False
      c1.Enabled = False
      c2.Enabled = False
      ap2Val.Enabled = False
      ap3Val.Enabled = False
   Else
      'apvFrame.Enabled = True
      'c1.Enabled = True
      'c2.Enabled = True
   End If

   ap2_Click
   ap3_Click

End Sub

Private Sub ap2_Change()
   ap2_Click
End Sub

Private Sub ap2_Click()
   If ap2.Value = True Then
      PaidAs = 3
      apvFrame.Enabled = True
      c1.Enabled = True
      c2.Enabled = True
      ap2Val.Enabled = True
      ap3Val.Enabled = False
   End If
End Sub

Private Sub ap2Val_Change()
   PaidCurr = Val(ap2Val.Text)
End Sub

Private Sub ap3_Change()
   ap3_Click
End Sub

Private Sub ap3_Click()
   If ap3.Value = True Then
      PaidAs = 4
      apvFrame.Enabled = True
      c1.Enabled = True
      c2.Enabled = True
      ap2Val.Enabled = False
      ap3Val.Enabled = True
   End If

End Sub

Private Sub ap3Val_Change()
   PaidPercent = Val(ap3Val.Text)
End Sub

Private Sub CommandButton1_Click()
FixProc "iSEO.OK" '##-##'
   SEO_Confirm = True
   iSEO.Hide
   Unload iSEO
End Sub

Private Sub CommandButton2_Click()
FixProc "iSEO.Cancel" '##-##'
   SEO_Confirm = False
   iSEO.Hide
   Unload iSEO
End Sub

Private Sub mType1_Change()
   mType1_Click
End Sub

Private Sub mType1_Click()
   If mType1.Value = True Then PaymentType$(2) = "Transfer"
End Sub

Private Sub mType2_Change()
   mType2_Click
End Sub

Private Sub mType2_Click()
   If mType2.Value = True Then PaymentType$(2) = "Cash"
End Sub


Private Sub pType1_Change()
   pType1_Click
End Sub

Private Sub pType1_Click()

   If pType1.Value = True Then
      PaymentType$(1) = "Advance Payment"
      apFrame.Enabled = True
      ap0.Enabled = True
      ap1.Enabled = True
      'ap2.Enabled = True
      'ap3.Enabled = True
   End If

   ap1_Click
   
End Sub

Private Sub pType2_Change()
   pType2_Click
End Sub

Private Sub pType2_Click()
   
   If pType2.Value = True Then
      PaidAs = 0
      PaymentType$(1) = "Credit"
      apFrame.Enabled = False
      ap0.Enabled = False
      ap1.Enabled = False
      ap2.Enabled = False
      ap3.Enabled = False
      apvFrame.Enabled = False
      c1.Enabled = False
      c2.Enabled = False
      ap2Val.Enabled = False
      ap3Val.Enabled = False
   End If

End Sub

Private Sub UserForm_Initialize()
FixProc "iSEO.Initialize" '##-##'
   SEO_Confirm = False
   PaymentType$(1) = "Advance Payment"
   PaymentType$(2) = "Transfer"
   PaidAs = 1
   PaidCurr = 0
   PaidPercent = 0
End Sub
