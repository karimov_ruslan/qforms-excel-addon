Attribute VB_Name = "modHTML"
Option Explicit
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private LastWord$(3)
Private CR$
Private II$
Private HTMLbuffer$

Public Sub MCmodHTML()
FixProc "modHTML.MCmodHTML" '##-##'

If ErrHandMode = False Then GoTo NoErrHand
On Error GoTo GlobalErrorHandler

NoErrHand:

   Select Case CommandBars.ActionControl.Parameter
      Case "1"
         MakeEMLFile
      Case "2"
         MakeHTMLFile
   End Select

Exit Sub

GlobalErrorHandler:

   FixProc "#GlobalErrorHandler#:modHTML.MCmodHTML"
   Select Case LogThisError()
      Case vbAbort
         End
      Case vbRetry
         Resume
      Case vbIgnore
         Resume Next
   End Select

End Sub


Public Sub MakeHTMLFile()
FixProc "modHTML.MakeHTMLFile" '##-##'
   MakeHTML 1
End Sub

Public Sub MakeEMLFile()
FixProc "modHTML.MakeEMLFile" '##-##'
   If GetIniPar("MailClient") = "Outlook" Then MakeHTML 1, True Else MakeHTML 2
End Sub

Private Sub MakeHTML(xMode, Optional forOutlook As Boolean)
FixProc "modHTML.MakeHTML" '##-##'

    If GetMode() = 3 Then Exit Sub
    If CurrentLang() = 3 Then qMsg 3, vbExclamation: Exit Sub
    If GetMode() = 2 Then
        If qVerify() > 0 Then qMsg 4, vbExclamation: Exit Sub
    End If
    
' the process --------------
    
    CR$ = Chr$(13) + Chr$(10)
    II$ = Chr$(34)
    
    LastWord$(1) = "Sincerely,"
    LastWord$(2) = "� ���������,"
    HTMLbuffer$ = ""
    PreloadTemplate qfweb$
    BuildHead
    BuildDataArea
    BuildEnd
    WriteHTML xMode, forOutlook

End Sub

Private Sub BuildHead()
FixProc "modHTML.BuildHead" '##-##'

    Dim i As Integer, HTMLdata$
    
    i = 1
    HTMLdata$ = ""
    While HTMLdata$ <> "[HEAD END]"
        HTMLbuffer$ = HTMLbuffer$ + DataParser$(HTMLdata$)
        HTMLdata$ = Workbooks("qfweb.xla").Worksheets("htmlhead").Cells(i, 2).Value
        i = i + 1
    Wend

End Sub

Private Sub BuildDataArea()
FixProc "modHTML.BuildDataArea" '##-##'
    
    Dim gtRow As Integer, GTWrow As Integer, i As Integer
    
    Worksheets(FormTab$).Activate
    gtRow = FormulaRow(qsGT$)
    GTWrow = FormulaRow(qsDIGI$)
    
    For i = FirstDataRow To gtRow - 2
        HTMLbuffer$ = HTMLbuffer$ + HTMLrow$(i)
    Next
    
    If GetMode() = 1 Then
        HTMLbuffer$ = HTMLbuffer$ + "</table>" + CR$
        Exit Sub
    End If
    
    HTMLbuffer$ = HTMLbuffer$ + RowGT$(gtRow)
    HTMLbuffer$ = HTMLbuffer$ + RowGTW$(GTWrow)
    HTMLbuffer$ = HTMLbuffer$ + RowEmpty$() + RowEmpty$() + "</table>" + CR$
    
End Sub

Private Sub BuildEnd()
FixProc "modHTML.BuildEnd" '##-##'

   Dim EmailTo$, tmp$
   
   HTMLbuffer$ = HTMLbuffer$ + RowDesc$("Text Box 66")
    
   If GetMode() = 1 Then GoTo CloseHTML
   
' Build Confirmation Button ---------------------------------
    
If GetIniPar$("Ready2Buy") = "YES" Then
    EmailTo$ = GetQFemail$(DocumentOwner(), "")
    tmp$ = "<tr><td class=" + II$ + "terms" + II$ + "><form name=" + II$ + "QFsubmit" + II$ + " action=" + II$ + "http://www.risk.az/cgi-bin/cgiemail/qforms.txt" + II$ + " method=" + II$ + "post" + II$ + " target=" + II$ + "_top" + II$ + ">"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + "http://www.risk.az/forms/submitted.html" + II$ + " name=" + II$ + "success" + II$ + ">"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + EmailTo$ + II$ + " name=" + II$ + "sp" + II$ + ">"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + DataParser$("###headTo") + II$ + " name=" + II$ + "company" + II$ + "><BR>"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + DataParser$("###headAttn") + II$ + " name=" + II$ + "name" + II$ + ">"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + DataParser$("###headDate") + II$ + " name=" + II$ + "odate" + II$ + ">"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + DataParser$("###headRef") + II$ + " name=" + II$ + "oref" + II$ + "><BR>"
    tmp$ = tmp$ + "<input type=" + II$ + "submit" + II$ + " name=" + II$ + "SB" + II$ + " value=" + II$ + "I'm Ready to Buy, Contact Me." + II$ + ">"
    tmp$ = tmp$ + "</form></td></tr>"
    HTMLbuffer$ = HTMLbuffer$ + tmp$ + CR$
End If

CloseHTML:
    
   HTMLbuffer = HTMLbuffer$ + "</table></div></body></html>"

End Sub

Private Sub WriteHTML(xMode, Optional forOutlook As Boolean)
FixProc "modHTML.WriteHTML" '##-##'

    Dim HTMLpath$, HTMLfile$, EMLpath$, EMLfile$, EMLbuffer$
    Dim ExecID, SW_SHOWMAXIMIZED, oParam$

    HTMLpath$ = QFHTMLpath$
    HTMLfile$ = HTMLpath$ + WhatFile$(0) + ".html"
    EMLpath$ = QFTempPath$
    EMLfile$ = EMLpath$ + WhatFile$(0) + ".eml"

   If xMode = 1 Then
      Open HTMLfile$ For Output As 1
      Print #1, HTMLbuffer$
      Close #1
      If forOutlook = True Then oParam$ = "outlook.exe " Else oParam$ = "explorer "
      ExecID = Shell(oParam$ + HTMLfile$, vbNormalFocus)
      Exit Sub
   End If
    
    If xMode = 2 Then
        EMLbuffer$ = Workbooks("qfweb.xla").Worksheets("emailhead").Cells(1, 2).Value
        HTMLbuffer$ = Replace(EMLbuffer$, Chr$(10), CR$, 1, -1, vbBinaryCompare) + CR$ + HTMLbuffer$
        Open EMLfile$ For Output As 1
        Print #1, HTMLbuffer$
        Close #1
        Dim ret As Long
        ExecID = ShellExecute(0&, "Open", EMLfile$, 0&, 0&, SW_SHOWMAXIMIZED)
    End If

End Sub

Private Function DataParser$(inputData$)
FixProc "modHTML.DataParser$" '##-##'

    Dim DataRange$
    Dim CellY As Integer, CellX As Integer
        
    If inputData$ = "###CODEPAGE" Then
        DataParser$ = CodePage$(CurrentLang())
        Exit Function
    End If
    
    If Left$(inputData$, 3) = "###" Then
        DataRange$ = Trim$(Mid$(inputData$, 4))
        CellY = LabelY(DataRange$)
        CellX = LabelX(DataRange$)
        DataParser$ = Replace(Worksheets(FormTab$).Cells(CellX, CellY).Value, Chr$(10), "<BR>", 1, -1, vbBinaryCompare)
        Exit Function
    End If

    DataParser$ = inputData$

End Function

Private Function CodePage$(LangMode)
FixProc "modHTML.CodePage$" '##-##'

    Dim cP$(3)
    cP$(1) = "<meta http-equiv=" + II$ + "content-type" + II$ + " content=" + II$ + "text/html;charset=iso-8859-1" + II$ + ">"
    cP$(2) = "<meta http-equiv=" + II$ + "content-type" + II$ + " content=" + II$ + "text/html;charset=windows-1251" + II$ + ">"
    cP$(3) = ""

    CodePage$ = cP$(LangMode)

End Function

Private Function RowStd$(SourceRow)
FixProc "modHTML.RowStd$" '##-##'

   Dim oMode As Integer, hasPNcolumn As Boolean, AdaptMode As Boolean
   Dim d2$, d3$, d4$, d5$, d6$, d7$, tmp$, tmp2$, colDesc2 As Integer
   
   If colDesc - dColFirst > 1 Then hasPNcolumn = True
   If GetIniPar$("pnAdaptive") = "YES" Then AdaptMode = True

        oMode = GetMode()

   With Worksheets(FormTab$)
      d2$ = .Cells(SourceRow, colNumb).Value
      If .Cells(SourceRow, colDesc).MergeCells = True Then
         d3$ = Replace(.Cells(SourceRow, colNumb + 1).Value, Chr$(10), "<BR>", 1, -1, vbBinaryCompare)
      Else
         d3$ = Replace(.Cells(SourceRow, colDesc).Value, Chr$(10), "<BR>", 1, -1, vbBinaryCompare)
      End If
      If AdaptMode = True And hasPNcolumn = True Then
         tmp2$ = Replace(.Cells(SourceRow, colNumb + 1).Value, Chr$(10), "<BR>", 1, -1, vbBinaryCompare)
         If .Cells(SourceRow, colDesc).MergeCells = False Then
            If Trim$(tmp2$) <> "" Then d3$ = "[P/N:" + tmp2$ + "]<BR>" + d3$
         End If
      End If
      If oMode = 1 Then d4$ = "" Else d4$ = .Cells(SourceRow, colQty).Value
      d5$ = Format(.Cells(SourceRow, colUnit).Value, defNumb$)
      If oMode = 1 Then d6$ = "" Else d6$ = Format(.Cells(SourceRow, colTotal).Value, defNumb$)
      d7$ = .Cells(SourceRow, colCmt).Value
  End With

tmp$ = "<tr><td class=" + II$ + "c1" + II$ + ">" + d2$ + "</td>"
tmp$ = tmp$ + "<td class=" + II$ + "c2" + II$ + ">" + d3$ + "</td>"
tmp$ = tmp$ + "<td class=" + II$ + "c1" + II$ + ">" + d4$ + "</td>"
tmp$ = tmp$ + "<td class=" + II$ + "c4" + II$ + ">" + d5$ + "</td>"
tmp$ = tmp$ + "<td class=" + II$ + "c4" + II$ + ">" + d6$ + "</td>"
tmp$ = tmp$ + "<td class=" + II$ + "c1" + II$ + ">" + d7$ + "</td></tr>"

    RowStd$ = tmp$ + CR$

End Function

Private Function RowEmpty$(Optional SourceRow)
FixProc "modHTML.RowEmpty$" '##-##'

    Dim tmp$
    
    tmp$ = "<tr height=" + II$ + "10" + II$ + "><td height=" + II$ + "10" + II$ + " colspan=" + II$ + "6" + II$ + "></td></tr>"
    RowEmpty$ = tmp$ + CR$

End Function

Private Function RowST$(SourceRow)
FixProc "modHTML.RowST$" '##-##'

    Dim STtext$, STdata$, tmp$
    
    STtext$ = Worksheets(FormTab$).Cells(SourceRow, 4).Value
    STdata$ = Worksheets(FormTab$).Cells(SourceRow, 5).Value
    tmp$ = "<tr><td colspan=" + II$ + "3" + II$ + "></td><td class=" + II$ + "st" + II$ + ">" + STtext$
    tmp$ = tmp$ + "</td><td class=" + II$ + "st" + II$ + ">" + Format(STdata$, defNumb$)
    tmp$ = tmp$ + "</td><td></td></tr>"
    RowST$ = tmp$ + CR$

End Function

Private Function RowGRP$(SourceRow)
FixProc "modHTML.RowGRP$" '##-##'

    Dim GrpName$, tmp$
    
    GrpName$ = Worksheets(FormTab$).Cells(SourceRow, 3).Value
    tmp$ = "<tr><td class=" + II$ + "grp" + II$ + " width=" + II$ + "25" + II$ + "></td><td class=" + II$ + "grp" + II$ + " colspan=" + II$ + "5" + II$ + ">"
    tmp$ = tmp$ + GrpName$ + "</td></tr>"
    RowGRP$ = tmp$ + CR$
    
End Function

Private Function RowGT$(SourceRow)
FixProc "modHTML.RowGT$" '##-##'

    Dim GTtext$, GTdata$, tmp$
        
    GTtext$ = Worksheets(FormTab$).Cells(SourceRow, colQty).Value
    GTdata$ = Format(Worksheets(FormTab$).Cells(SourceRow, colTotal).Value, defNumb$)

tmp$ = "<tr><td colspan=" + II$ + "4" + II$ + " class=" + II$ + "gt" + II$ + ">" + GTtext$
tmp$ = tmp$ + "</td><td class=" + II$ + "gt" + II$ + ">" + GTdata$
tmp$ = tmp$ + "</td><td></td></tr>"

    RowGT$ = tmp$ + CR$

    If GetVATmode() <> 1 Then Exit Function

    GTtext$ = Worksheets(FormTab$).Cells(SourceRow + 1, colQty).Value
    GTdata$ = Format(Worksheets(FormTab$).Cells(SourceRow + 1, colTotal).Value, defNumb$)
tmp$ = tmp$ + "<tr><td colspan=" + II$ + "4" + II$ + " class=" + II$ + "gt" + II$ + ">" + GTtext$
tmp$ = tmp$ + "</td><td class=" + II$ + "gt" + II$ + ">" + GTdata$
tmp$ = tmp$ + "</td><td></td></tr>"
    GTtext$ = Worksheets(FormTab$).Cells(SourceRow + 2, colQty).Value
    GTdata$ = Format(Worksheets(FormTab$).Cells(SourceRow + 2, colTotal).Value, defNumb$)
tmp$ = tmp$ + "<tr><td colspan=" + II$ + "4" + II$ + " class=" + II$ + "gt" + II$ + ">" + GTtext$
tmp$ = tmp$ + "</td><td class=" + II$ + "gt" + II$ + ">" + GTdata$
tmp$ = tmp$ + "</td><td></td></tr>"

    RowGT$ = tmp$ + CR$

End Function

Private Function RowGTW$(SourceRow)
FixProc "modHTML.RowGTW$" '##-##'

    Dim GTWtext$, tmp$

    GTWtext$ = Worksheets(FormTab$).Cells(SourceRow, colTotal).Value

tmp$ = "<tr><td colspan=" + II$ + "5" + II$ + " class=" + II$ + "gtw" + II$ + ">" + GTWtext$
tmp$ = tmp$ + "</td><td></td></tr>"

    RowGTW$ = tmp$ + CR$

End Function

Private Function RowDesc$(sourceShape$)
FixProc "modHTML.RowDesc$" '##-##'
    
    Dim EmailTo$, tmp$, ds$, dsHTML$, DescText$
    Dim LangMode As Integer

On Error Resume Next
    Worksheets(FormTab$).Shapes(sourceShape$).Select
    DescText$ = Replace(Selection.Characters.Text, Chr$(10), "<BR>", 1, -1, vbBinaryCompare)
On Error GoTo 0
    
    EmailTo$ = GetQFemail$(DocumentOwner(), "")
    LangMode = CurrentLang()

tmp$ = "<tr><td class=" + II$ + "terms" + II$ + "><br>" + DescText$
tmp$ = tmp$ + "<BR><BR><div align=" + II$ + "left" + II$ + "><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + LastWord$(LangMode)
tmp$ = tmp$ + "<br><br><br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + SubmitterName$(LangMode)
tmp$ = tmp$ + "</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=" + II$ + "mailto:" + EmailTo$ + II$ + ">" + EmailTo$ + "</A>"

'Build Digital Signature -----------------------------------
    
   If GetIniPar("noDigiSignHTML") = "YES" Then GoTo NX2
   ds$ = AssmbleDigiSignQF$
   dsHTML$ = "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font face=""Arial"" size=2><A href=""" + vLink$ + ds$ + """>[ This Document is Digitally Signed with R.I.S.K. DigiSign ]</A></font><br>"

NX2:
tmp$ = tmp$ + dsHTML$ + "<br><br></a></p></div></td></tr>"
   
    RowDesc$ = tmp$ + CR$
   
End Function

Private Function HTMLrow$(SourceRow As Integer)
FixProc "modHTML.HTMLrow$" '##-##'

    Dim RowType As Integer

    HTMLrow$ = ""
        
    Worksheets(FormTab$).Activate
    RowType = qRowType(SourceRow)

    Select Case RowType
        Case 0
            HTMLrow$ = RowEmpty$(SourceRow)
        Case 1
            HTMLrow$ = RowStd$(SourceRow)
        Case 2
            HTMLrow$ = RowGRP$(SourceRow)
        Case 3
            HTMLrow$ = RowST$(SourceRow)
    End Select

End Function

