Attribute VB_Name = "modSEO"
Option Explicit
Option Compare Text
Public PaymentType$(1 To 2)
Public HasAdditionalGoods As Boolean
Public PaidAs As Byte
Public PaidCurr As Double
Public PaidPercent As Double
Public SEO_Confirm As Boolean
Public Const seoTab$ = "SEO"
Public SEO_ID_File$

Private Sub InsertSEO()
FixProc "modSEO.InsertSEO" '##-##'

   InitVariables

   Dim ws
   For Each ws In ActiveWorkbook.Worksheets
     If ws.Name = seoTab$ Then
       Application.DisplayAlerts = False
       Worksheets(seoTab$).Delete
       Application.DisplayAlerts = True
     End If
   Next
   
   Sheets.Add After:=Worksheets(Worksheets.Count)
   ActiveSheet.Name = seoTab$
   ActiveWindow.DisplayGridlines = False
   
   Cells.VerticalAlignment = xlTop
   
   Columns("A").ColumnWidth = 6.7
   Columns("B").ColumnWidth = 4.2
   Columns("B").HorizontalAlignment = xlCenter
   Columns("C").ColumnWidth = 48.2
   Columns("D").ColumnWidth = 4.1
   Columns("D").HorizontalAlignment = xlCenter
   Columns("E:H").ColumnWidth = 12
   Columns("E:H").NumberFormat = "#,##0"
   Columns("I").ColumnWidth = 20
   
   Rows(1).RowHeight = 6
   Rows(2).RowHeight = 23
   Rows(3).RowHeight = 12
   Rows("4:7").RowHeight = 16
   Rows(8).RowHeight = 12
   Rows(9).RowHeight = 25.5
   Rows(9).HorizontalAlignment = xlCenter
   Rows(9).WrapText = True

   Range("A2:I2").Merge

   Range("A4:B4").Merge
   Range("A5:B5").Merge
   Range("A6:B6").Merge
   Range("A7:B7").Merge
   
   Range("G4:I4").Merge
   Range("E4:F4").Merge
   Range("G5:H5").Merge
   Range("E5:F5").Merge
   Range("G7:H7").Merge
   Range("E7:F7").Merge
   
   Range("A4:C7,E4:I5,E7:I7,B9:I9").Select
   AddGrid xlHairline
   Range("A2:I2").Select
   AddGrid xlThin
   
   DecoCell 2, 1, "STOCK EXTRACTION ORDER", True, , xlCenter, , True, , True, , 16, , , , xlCenter
   
   DecoCell 4, 1, "DATE:", , , xlRight, , True, , , , , , , , xlCenter
   DecoCell 5, 1, "DOC ID:", , , xlRight, , True, , , , , , , , xlCenter
   DecoCell 6, 1, "CUSTOMER:", , , xlRight, , True, , , , , , , , xlCenter
   DecoCell 7, 1, "CONTACT:", , , xlRight, , True, , , , , , , , xlCenter

   DecoCell 4, 5, "Responsible Person:", , , xlRight, , True, , , , , , , , xlCenter
   DecoCell 5, 5, "Based on CO [Ref/Date]:", , , xlRight, , True, , , , , , , , xlCenter
   DecoCell 7, 5, "PAYMENT TERMS:", , , xlRight, , True, , , , , , , , xlCenter

   DecoCell 9, 2, "#", , , , , True, , , , , , , , xlCenter
   DecoCell 9, 3, "Description", , , , , True, , , , , , , , xlCenter
   DecoCell 9, 4, "Qty.", , , , , True, , , , , , , , xlCenter
   DecoCell 9, 5, "Unit Price", , , , , True, , , , , True, , , xlCenter
   DecoCell 9, 6, "Total Price", , , , , True, , , , , True, , , xlCenter
   DecoCell 9, 7, "Paid Amount", , , , , True, , , , , True, , , xlCenter
   DecoCell 9, 8, "Pending Amount", , , , , True, , , , , True, , , xlCenter
   DecoCell 9, 9, "Comment", , , , , True, , , , , , , , xlCenter
   
   DecoCell 4, 3, "'" + Format$(Date, "dd mmmm, yyyy"), True, , xlLeft, , True, 11, , , , , , , xlCenter
   DecoCell 5, 3, GetSEO_ID$, True, , xlLeft, , True, 11, True, , , , , , xlCenter
   DecoCell 6, 3, Worksheets(FormTab$).Cells(LabelX("headTo"), LabelY("headTo")), True, , xlLeft, , , 11, , , , , , , xlCenter
   DecoCell 7, 3, Worksheets(FormTab$).Cells(LabelX("headAttn"), LabelY("headAttn")), False, , xlLeft, , , 11, , , , , , , xlCenter

   DecoCell 4, 7, "[" + GetIniPar$("id") + "] " + GetIniPar$("user", True), True, , xlLeft, , True, 11, , , , , , , xlCenter
   DecoCell 5, 7, Worksheets(FormTab$).Cells(LabelX("headRef"), LabelY("headRef")), False, , xlLeft, , , 11, , , , , , , xlCenter
   DecoCell 5, 9, Worksheets(FormTab$).Cells(LabelX("headDate"), LabelY("headDate")), False, , xlLeft, , , 11, , , , , , , xlCenter
      
   
End Sub

Private Function GetSEO_ID$()
FixProc "modSEO.GetSEO_ID$" '##-##'
   
   Dim cCounter As Long, tmp$, cReceived As Boolean
   
   On Error GoTo errHandler
   
   Open qfServer$ + SEOCountFN$ For Input As 99
   Input #99, tmp$
   Close #99

   cCounter = Val(Trim$(tmp$))
   GetSEO_ID$ = "SEO:" + Format$(cCounter, "######000000") + ":" + GetIniPar$("id")
   cReceived = True

   Open qfServer$ + SEOCountFN$ For Output As 98
   Print #98, Trim$(Str$(cCounter + 1))
   Close #98

   On Error GoTo 0

Exit Function

errHandler:
   On Error GoTo 0
   If cReceived = True Then Exit Function
   Randomize
   GetSEO_ID$ = "SEO:T" + Trim$(Str$(Int((99998 * Rnd) + 1))) + ":" + GetIniPar$("id")

End Function

Public Sub IssueSEO()
FixProc "modSEO.IssueSEO" '##-##'

If ErrHandMode = False Then GoTo NoErrHand
On Error GoTo GlobalErrorHandler

NoErrHand:
   If GetMode = 1 Then Exit Sub
   Load iSEO
   iSEO.Show
   
   If SEO_Confirm = False Then Exit Sub
   
   Dim idxForm As Integer, rType As Integer, scanFinished As Boolean
   Dim idxSEO As Integer, i As Integer
         
   Application.ScreenUpdating = False
         
   InsertSEO
   DecoCell 7, 7, PaymentType$(1), True, False, xlCenter, , True, 11, , , , , , , xlCenter
   DecoCell 7, 9, PaymentType$(2), True, False, xlCenter, , True, 11, , , , , , , xlCenter
   
   Worksheets(FormTab$).Activate
   idxForm = FirstDataRow
   idxSEO = 10
   
   While scanFinished = False
      rType = qRowType(idxForm)
      If rType = 0 Or rType = 2 Or rType = 3 Or rType = 10 Then GoTo W
      If rType = 4 Or rType = 5 Or rType = 7 Or rType = 8 Then scanFinished = True: GoTo W
      If rType = 1 Then
         With Worksheets(seoTab$)
            .Cells(idxSEO, 2) = Cells(idxForm, colNumb)
            .Cells(idxSEO, 3) = CompText$(Cells(idxForm, colDesc))
            .Cells(idxSEO, 3).Font.Size = 9
            If Val(Cells(idxForm, colQty).Value) = 0 Then
               .Cells(idxSEO, 4) = 0
            Else
               .Cells(idxSEO, 4) = Cells(idxForm, colQty)
            End If
            .Cells(idxSEO, 5) = Cells(idxForm, colUnit)
            .Cells(idxSEO, 6).Formula = "=D" & idxSEO & "*E" & idxSEO
            If PaidAs = 2 Then .Cells(idxSEO, 7).Formula = "=F" & idxSEO
            .Cells(idxSEO, 8).Formula = "=F" & idxSEO & "-G" & idxSEO
            idxSEO = idxSEO + 1
         End With
      End If
W:   idxForm = idxForm + 1
   Wend
   
   Worksheets(seoTab$).Activate
   Range("B10:I" & idxSEO - 1).Select
   AddGrid xlHairline
   
   Rows(idxSEO).RowHeight = 16
   DecoCell idxSEO, 5, "TOTALS:", True, , xlRight, , True, 11, , , , , , , xlCenter
   DecoCell idxSEO, 6, "=SUM(F10:F" & idxSEO - 1 & ")", True, , xlRight, True, True, 11, , , , , , , xlCenter
   DecoCell idxSEO, 7, "=SUM(G10:G" & idxSEO - 1 & ")", True, , xlRight, True, True, 11, , , , , , , xlCenter
   DecoCell idxSEO, 8, "=SUM(H10:H" & idxSEO - 1 & ")", True, , xlRight, True, True, 11, , , , , , , xlCenter
   
   idxSEO = idxSEO + 1
   
   If HasAdditionalGoods = True Then
      Range("B" & idxSEO + 1 & ":I" & idxSEO + 3).Select
      AddGrid xlHairline
      DecoCell idxSEO, 2, "List of Goods Not Included in CO:", True, , xlLeft, , True, 53
      uLine "B" & idxSEO & ":I" & idxSEO, 4
      Rows(idxSEO + 4).RowHeight = 16
      DecoCell idxSEO + 4, 5, "TOTALS:", True, , xlRight, , True, 53, , , , , , , xlCenter
      DecoCell idxSEO + 4, 6, "=SUM(F" & idxSEO + 1 & ":F" & idxSEO + 3 & ")", True, , xlRight, True, True, 53, , , , , , , xlCenter
      DecoCell idxSEO + 4, 7, "=SUM(G" & idxSEO + 1 & ":G" & idxSEO + 3 & ")", True, , xlRight, True, True, 53, , , , , , , xlCenter
      DecoCell idxSEO + 4, 8, "=SUM(H" & idxSEO + 1 & ":H" & idxSEO + 3 & ")", True, , xlRight, True, True, 53, , , , , , , xlCenter
      Rows(idxSEO + 5).RowHeight = 20
      DecoCell idxSEO + 5, 5, "GRAND TOTALS:", True, , xlRight, , True, 3, , , , , , , xlCenter
      DecoCell idxSEO + 5, 6, "=F" & idxSEO - 1 & "+F" & idxSEO + 4, True, , xlRight, True, True, 3, , , , , , , xlCenter
      DecoCell idxSEO + 5, 7, "=G" & idxSEO - 1 & "+G" & idxSEO + 4, True, , xlRight, True, True, 3, , , , , , , xlCenter
      DecoCell idxSEO + 5, 8, "=H" & idxSEO - 1 & "+H" & idxSEO + 4, True, , xlRight, True, True, 3, , , , , , , xlCenter
      For i = idxSEO + 1 To idxSEO + 3
         Cells(i, 6).Formula = "=D" & i & "*E" & i
         Cells(i, 8).Formula = "=F" & i & "-G" & i
      Next
      idxSEO = idxSEO + 6
   End If
   
   Rows(idxSEO).RowHeight = 17
   Rows(idxSEO + 1).RowHeight = 51.75
   
   DecoCell idxSEO, 2, "Special Marks:", False, True, xlLeft, , True, , , , 8, , , , xlBottom
   DecoCell idxSEO, 5, "Supervisor's Stamp & Signature", False, True, xlLeft, , True, , , , 8, , , , xlBottom
   DecoCell idxSEO, 7, "Stock Manager's Signature", False, True, xlLeft, , True, , , , 8, , , , xlBottom
   DecoCell idxSEO, 9, "Customer's Signature", False, True, xlLeft, , True, , , , 8, , , , xlBottom
   
   Range("E" & idxSEO + 1 & ":F" & idxSEO + 1).Merge
   Range("G" & idxSEO + 1 & ":H" & idxSEO + 1).Merge
   Range("B" & idxSEO + 1 & ":C" & idxSEO + 1).Merge
   
   OutlineCells "E" & idxSEO + 1 & ":I" & idxSEO + 1, 4
   OutlineCells "B" & idxSEO + 1 & ":C" & idxSEO + 1, 3
   
   With Range("E" & idxSEO + 1 & ":I" & idxSEO + 1).Interior
      .ColorIndex = 0
      .Pattern = xlLightUp
      .PatternColorIndex = 20
   End With
   
   With ActiveSheet.PageSetup
       .LeftMargin = Application.InchesToPoints(0.6)
       .RightMargin = Application.InchesToPoints(0.42)
       .TopMargin = Application.InchesToPoints(0.28)
       .BottomMargin = Application.InchesToPoints(0.3)
       .HeaderMargin = Application.InchesToPoints(0.2)
       .FooterMargin = Application.InchesToPoints(0.21)
       .PrintHeadings = False
       .PrintGridlines = False
       .Orientation = xlLandscape
       .Draft = False
       .PaperSize = xlPaperA4
       .Order = xlDownThenOver
       .Zoom = 100
       .PrintErrors = xlPrintErrorsDisplayed
       .FitToPagesWide = 1
       .FitToPagesTall = 1
   End With

   Range("B9").Select
   Application.ScreenUpdating = True
Exit Sub

GlobalErrorHandler:

   FixProc "#GlobalErrorHandler#:modSEO.IssueSEO"
   Select Case LogThisError()
      Case vbAbort
         End
      Case vbRetry
         Resume
      Case vbIgnore
         Resume Next
   End Select


End Sub
