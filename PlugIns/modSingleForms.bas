Attribute VB_Name = "modSingleForms"
Option Compare Text
Option Explicit

Public Sub MCmodSF()
FixProc "modSingleForms.MCmodSF" '##-##'

If ErrHandMode = False Then GoTo NoErrHand
On Error GoTo GlobalErrorHandler

NoErrHand:

   Select Case CommandBars.ActionControl.Parameter
      Case "1"
         InsertCashMemo
      Case "2"
         InsertDeliveryNote
   End Select

Exit Sub

GlobalErrorHandler:

   FixProc "#GlobalErrorHandler#:modSingleForms.MCmodSF"
   Select Case LogThisError()
      Case vbAbort
         End
      Case vbRetry
         Resume
      Case vbIgnore
         Resume Next
   End Select

End Sub

Public Sub InsertCashMemo()
FixProc "modSingleForms.InsertCashMemo" '##-##'

   Dim tmpSize As Integer, shps, shp, LangMode As Integer, i As Integer
   
   SaveLocation True
   RenewSheet "CashMemo"
    
   With Worksheets("CashMemo")
      .Activate
      .Columns(lCol$(colCmt)).EntireColumn.Hidden = True
      .Columns(lCol$(colTotal)).EntireColumn.Hidden = False
      .Columns(lCol$(colUnit)).EntireColumn.Hidden = False
      .Columns(lCol$(colUnit)).ColumnWidth = 13.14
      .Columns(lCol$(colTotal)).ColumnWidth = 17.43
      For i = 1 To 4
         .Columns(lCol$(colCmt + i)).ColumnWidth = 3
      Next
   End With
    
   AddLogo
   
   Cells(LabelX("headRef_label"), LabelY("headRef_label")) = "No.:"
   
   LangMode = CurrentLang()
   If LangMode = 2 Then tmpSize = 15 Else tmpSize = 18
   Cells(LabelX("headRef"), LabelY("headRef")) = "CM~" + Worksheets(FormTab$).Cells(LabelX("headRef"), LabelY("headRef"))
   DecoCell LabelX("title"), LabelY("title"), CM$(1, LangMode), True, False, xlLeft, , , , , , tmpSize
         
   Set shps = Worksheets("CashMemo").Shapes
   For Each shp In shps
       If shp.Top > 200 Then shp.Delete
   Next
    
ChangePAIDwords: '---------------------------------

   Dim gtRow As Integer, GTcol As Integer, CurrMode As Integer

   CurrMode = CurrentCurrency()
   gtRow = FormulaRow(qsGT$, "CashMemo")
   GTcol = FormulaCol
   If gtRow = 0 Then GoTo Finalize
   DecoCell gtRow, colQty, "TOTAL PAID (" + crSign$(CurrMode, 1) + "):", True, , xlRight
   If GetVATsubMode() > 0 Then
      DecoCell gtRow + 1, colQty, "18% VAT (" + crSign$(CurrMode, 1) + "):", True, , xlRight
      DecoCell gtRow + 2, colQty, "GRAND TOTAL PAID (" + crSign$(CurrMode, 1) + "):", True, , xlRight
   End If

Finalize: '----------------------------------------
    
   DoPageSetup
   RestLocation True

End Sub

Public Sub InsertDeliveryNote()
FixProc "modSingleForms.InsertDeliveryNote" '##-##'
   
   Dim TheEnd As Integer, tmpSize As Integer, LangMode As Integer, i As Integer
   Dim fFlag As Integer, fsz As Integer
   
   SaveLocation True
   RenewSheet "DeliveryNote"
   
   TheEnd = FormulaRow(qsGT$, "DeliveryNote")
   LangMode = CurrentLang()

   With Worksheets("DeliveryNote")
      .Activate
      .Columns(lCol$(colCmt)).EntireColumn.Hidden = False
      .Columns(lCol$(colTotal)).EntireColumn.Hidden = True
      .Columns(lCol$(colQty)).EntireColumn.Hidden = False
      .Columns(lCol$(colUnit)).EntireColumn.Hidden = True
      .Columns(lCol$(colCmt)).ColumnWidth = 19
      .Columns(lCol$(colDesc)).ColumnWidth = 52
      For i = 1 To 4
         .Columns(lCol$(colCmt + i)).ColumnWidth = 3
      Next
      .Cells(LabelX("headRef_label"), LabelY("headRef_label")) = "No.:"
   End With
   
   AddLogo

   DecoCell LabelX("capNum"), colCmt, DN$(3, LangMode), , , xlCenter
        
   If LangMode = 2 Then tmpSize = 15 Else tmpSize = 18
   Cells(LabelX("headRef"), LabelY("headRef")) = "DN~" + Worksheets(FormTab$).Cells(LabelX("headRef"), LabelY("headRef"))
   DecoCell LabelX("title"), LabelY("title"), DN$(1, LangMode), True, False, xlLeft, , , , , , tmpSize
   
   For i = FirstDataRow To TheEnd - 2
      Cells(i, colCmt) = DN$(2, LangMode)
      Cells(i, colCmt).Font.Name = xFNT$(LangMode)
   Next
      
   Rows(RowsAddr$(TheEnd, 100)).Delete Shift:=xlUp
     
   If LangMode = 2 Then fFlag = 1: fsz = 9 Else fsz = 10: fFlag = 0
   DecoCell TheEnd + 2, colDesc, GetIniPar$("user") + " on behalf of R.I.S.K. Company", False, True, xlLeft
   DecoCell TheEnd + 6, colDesc, Worksheets(FormTab$).Cells(5, 3) + String$(35, " ") + "on behalf of" + Worksheets(FormTab$).Cells(4, 3), False, True, xlLeft
   uLine CellAddr$(TheEnd + 2, colDesc), 1
   uLine CellAddr$(TheEnd + 6, colDesc), 1
   uLine CellAddr$(TheEnd + 2, colCmt), 1
   uLine CellAddr$(TheEnd + 6, colCmt), 1
    
   DoPageSetup
   RestLocation True
  
End Sub

Private Sub RenewSheet(shName$)
FixProc "modSingleForms.ReNewSheet" '##-##'
   
   Dim ws
   For Each ws In ActiveWorkbook.Worksheets
      If ws.Name = shName$ Then
         Application.DisplayAlerts = False
         Worksheets(shName$).Delete
         Application.DisplayAlerts = True
      End If
   Next
    
   Worksheets(FormTab$).Activate
   Cells.Select
   Selection.Copy
   Sheets.Add After:=Worksheets(Worksheets.Count)
   ActiveSheet.Name = shName$
   ActiveSheet.Paste
   ActiveWindow.DisplayGridlines = False
   ActiveSheet.Range("A1").Select
   RemoveGroups shName$

End Sub
