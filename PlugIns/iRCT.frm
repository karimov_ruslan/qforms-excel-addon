VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} iRCT 
   Caption         =   "Template Selector..."
   ClientHeight    =   1665
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5115
   OleObjectBlob   =   "iRCT.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "iRCT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private CTemplates As Integer
Private CTemplate$(10, 2)

Private Sub CommandButton1_Click()
FixProc "iRCT.Select" '##-##'
    tIDX = SelectBox.ListIndex
    QFCmodName$ = CTemplate$(tIDX + 1, 2)
    iRCT.Hide
    Unload iRCT
    PreloadTemplate QFCmodName$
End Sub

Private Sub SelectBox_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
FixProc "iRCT.Cancel" '##-##'
   CommandButton1_Click
End Sub

Private Sub UserForm_Activate()
FixProc "iRCT.Activate" '##-##'
    SelectBox.Clear
    Open QFTemplatesPath$ + "qfcmods.ini" For Input As 1
        Input #1, tmp$
        CTemplates = 1
        While Not EOF(1)
            Input #1, tmp$
            If Trim$(tmp$) = "" Then GoTo wnd
            CTemplate$(CTemplates, 1) = ParseDivider$(tmp$, ":", 1)
            CTemplate$(CTemplates, 2) = ParseDivider$(tmp$, ":", 2)
            SelectBox.AddItem CTemplate$(CTemplates, 1)
            CTemplates = CTemplates + 1
wnd:   Wend
    Close
    SelectBox.Selected(0) = True
End Sub

