Attribute VB_Name = "qMisc"
Option Compare Text
Option Explicit

Public Sub OpenEditBox()
   Dim RowAbsolute, ColumnAbsolute
   If CheckProtect(False) = True Then Exit Sub
   EditBoxLocX = Val(Mid$(ActiveCell.Address(RowAbsolute, ColumnAbsolute), 2))
   EditBoxLocY = Asc(UCase$(Left$(ActiveCell.Address(RowAbsolute, ColumnAbsolute), 1))) - 64
   Range("A" + Trim$(Str$(EditBoxLocX))).Select
   Load iBOX: iBOX.Show
End Sub

Public Sub ShowPrintDialog()
    Load iPRINT: iPRINT.Show
End Sub

Public Sub editSettings()
    qReConnect
    Load iSET
    iSET.Show
End Sub

Public Sub RequestCTemplate()
    Load iRCT: iRCT.Show
End Sub

Public Sub AboutQF()
   intro.Show
End Sub

Public Sub DoPageSetup()

   Dim SavedName$, II$, rft$, pgOrient, BMargin
   Dim LangMode As Integer, CurrMode As Integer, LMargin, TMargin
   Dim tiLeft As Single, tiTop As Single
   
   tiLeft = Round((Val(GetIniPar$("tiLeft")) / 2.54), 2)
   tiTop = Round((Val(GetIniPar$("tiTop")) / 2.54), 2)
     
   LangMode = CurrentLang()
   ActiveWindow.DisplayGridlines = False
   
   If InStr(1, ActiveSheet.Name, "Contract") > 0 Then LMargin = 0.7 Else LMargin = 0.3
   If InStr(1, ActiveSheet.Name, "TaxInv") Then
       LMargin = 0 + tiLeft
       pgOrient = xlLandscape
       rft$ = ""
       BMargin = 1.3
       TMargin = 0.31 + tiTop
   Else
       pgOrient = xlPortrait
       II$ = Chr$(34)
       rft$ = "&" + II$ + xFNT$(LangMode) + ",Plain" + II$ + "&7" + pgName$(LangMode) + "&P"
       BMargin = 0.39
       TMargin = 0.31
   End If

On Error GoTo ErrHand
   With ActiveSheet.PageSetup
       .LeftMargin = Application.InchesToPoints(LMargin)
       .RightMargin = Application.InchesToPoints(0)
       .HeaderMargin = Application.InchesToPoints(0.21)
       .TopMargin = Application.InchesToPoints(TMargin)
       .FooterMargin = Application.InchesToPoints(0.24)
       .BottomMargin = Application.InchesToPoints(BMargin)
       .PaperSize = xlPaperA4
       .FirstPageNumber = xlAutomatic
       .RightFooter = rft$
       .Orientation = pgOrient
   End With
On Error GoTo 0
Exit Sub

ErrHand:
   MsgBox "Incorrect Page Margins, Check Settings!", vbExclamation, "(QF) Diagnostic..."
   Resume Next
      
End Sub

Public Sub PrintAll(qtyCON, qtyATT, qtyINV)
    PrintCON qtyCON
    PrintATT qtyATT
    PrintINV qtyINV
    iPRINT.Hide: Unload iPRINT
End Sub

Public Sub PrintCON(qtyCON)
    If iPRINT.prCON.Enabled = False Then Exit Sub
    If qtyCON <= 0 Then Exit Sub
    ActiveWorkbook.Worksheets("Contract").PrintOut , , qtyCON
End Sub

Public Sub PrintATT(qtyATT)
    If iPRINT.prATT.Enabled = False Then Exit Sub
    If qtyATT <= 0 Then Exit Sub
    ActiveWorkbook.Worksheets("Attachment").PrintOut , , qtyATT
End Sub

Public Sub PrintINV(qtyINV)
    If iPRINT.prINV.Enabled = False Then Exit Sub
    If qtyINV <= 0 Then Exit Sub
    ActiveWorkbook.Worksheets("Invoice").PrintOut , , qtyINV
End Sub

Public Function AllowCurrencyChange() As Boolean

   Dim ExDocs$, ws, msgText$

   AllowCurrencyChange = True
   If GetMode() <> 3 Then Exit Function
    
   For Each ws In ActiveWorkbook.Worksheets
       If ws.Name = "Invoice" Then ExDocs$ = ExDocs$ + "'Invoice' "
       If ws.Name = "Attachment" Then ExDocs$ = ExDocs$ + "'Attachment' "
   Next
 
   If ExDocs$ = "" Then Exit Function
   
   AllowCurrencyChange = False
   msgText$ = "You can Not make currency changes," + Chr$(10)
   msgText$ = msgText$ + "because you have created " + ExDocs$ + Chr$(10) + Chr$(10)
   msgText$ = msgText$ + "(you have to remove sheet(s) first, and re-create them after changes)"
   MsgBox msgText$, vbExclamation + vbOKOnly, "BrainEngine(R):Warning !"

End Function

Public Function IsRowsHidden() As Boolean

   Dim i As Integer
   IsRowsHidden = False
   
   For i = FirstDataRow To MaximumROWS
      If Worksheets("form").Rows(i).EntireRow.Hidden = True Then
        IsRowsHidden = True
        Exit Function
      End If
   Next

End Function

Public Function CheckProtect(SilentMode As Boolean) As Boolean

    CheckProtect = False
On Error Resume Next
    If ActiveSheet.ProtectContents = True Then
        CheckProtect = True
        If SilentMode = False Then MsgBox "Unable to perform this operation with Protected document !", vbExclamation, "Document is Protected..."
    End If
On Error GoTo 0

End Function

Public Sub SetConStyles()

   Dim st, i As Integer

   For Each st In ActiveWorkbook.Styles
       If Left$(st.Name, 1) = "<" Then st.Delete
   Next

' Style <n> ------------------------------------
   For i = 1 To 3
       ActiveWorkbook.Styles.Add Name:="<n>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<n>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 10
           .Font.Bold = False
           .Font.Italic = False
           .HorizontalAlignment = xlJustify
           .WrapText = True
       End With

' Style <b> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<b>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<b>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 10
           .Font.Bold = True
           .Font.Italic = False
           .HorizontalAlignment = xlJustify
           .WrapText = True
       End With

' Style <h1> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<h1>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<h1>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 16
           .Font.Bold = True
           .Font.Italic = False
           .HorizontalAlignment = xlCenter
           .WrapText = True
       End With

' Style <h2> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<h2>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<h2>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 12
           .Font.Bold = True
           .Font.Italic = False
           .HorizontalAlignment = xlCenter
           .WrapText = True
       End With

' Style <i> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<i>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<i>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 11
           .Font.Bold = True
           .Font.Italic = True
           .HorizontalAlignment = xlRight
           .WrapText = True
       End With

' Style <ls> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<ls>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<ls>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .IncludeBorder = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 10
           .Font.Bold = False
           .Font.Italic = False
           .HorizontalAlignment = xlLeft
           .Borders(xlBottom).LineStyle = xlContinuous
           .Borders(xlBottom).Weight = xlHairline
           .Borders(xlBottom).ColorIndex = 1
           .WrapText = True
       End With

' Style <rs> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<rs>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<rs>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .IncludeBorder = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 10
           .Font.Bold = False
           .Font.Italic = False
           .HorizontalAlignment = xlRight
           .Borders(xlTop).LineStyle = xlContinuous
           .Borders(xlTop).Weight = xlHairline
           .Borders(xlTop).ColorIndex = 1
           .WrapText = True
       End With
   Next

End Sub

Public Sub AddGrid(gridMode, Optional outMode As Boolean)
   Dim i As Integer
On Error Resume Next
   For i = 1 To MaxBrds
      With Selection.Borders(xlBrd(i))
       .LineStyle = xlContinuous
       .Weight = gridMode
       .ColorIndex = xlAutomatic
      End With
   Next
On Error GoTo 0
End Sub

Public Sub OutlineCells(xRange$, oMode As Integer, Optional skipTop As Boolean)

'oMode=0 -no grid
'oMode=1 -light grid
'oMode=2 -red error
'oMode=3 -blue
'oMode=4 -orange
   
   Dim lStyle, cIndex, wght, upTo As Integer, i As Integer
   
   lStyle = xlDash
   wght = xlThin
   Select Case oMode
      Case 0
         lStyle = xlNone
         wght = xlNone
         cIndex = xlNone
      Case 1
         lStyle = xlContinuous
         wght = xlHairline
         cIndex = xlAutomatic
      Case 2
         cIndex = 3
      Case 3
         cIndex = 11
      Case 4
         cIndex = 46
   End Select
  
   On Error Resume Next
   If skipTop = True Then upTo = MaxBrds - 1 Else upTo = MaxBrds
   For i = 1 To upTo
      With Range(xRange$).Borders(xlBrd(i))
        .LineStyle = lStyle
        .Weight = wght
        .ColorIndex = cIndex
      End With
   Next
   On Error GoTo 0

End Sub


Public Sub DecoCell(CellX As Integer, CellY As Integer, Optional cValue$, Optional xBold As Boolean, Optional xItalic As Boolean, Optional hAlign As Long, Optional xFormula As Boolean, Optional ignoreLang As Boolean, Optional ColorIDX As Integer, Optional IgnoreNumbFormat As Boolean, Optional forceLangMode As Integer, Optional forceSize As Integer, Optional forceWrap As Boolean)

   Dim LangMode As Integer, fsz(4) As Integer
   fsz(1) = 10: fsz(2) = 9: fsz(3) = 10: fsz(4) = 10

   If forceLangMode <> 0 Then LangMode = forceLangMode Else LangMode = CurrentLang()
   If forceSize <> 0 Then fsz(LangMode) = forceSize

   With Cells(CellX, CellY)
      If cValue$ <> "" Then
         If xFormula = True Then .Formula = cValue$ Else .Value = cValue$
      End If
      If ignoreLang <> True Then
         .Font.Name = xFNT$(LangMode)
         .Font.Size = fsz(LangMode)
      End If
      If ColorIDX <> 0 Then .Font.ColorIndex = ColorIDX
      If xBold = True Then .Font.Bold = True
      If xItalic = True Then .Font.Italic = True
      If hAlign <> 0 Then .HorizontalAlignment = hAlign
      If IgnoreNumbFormat = False Then
         If IsNumeric(Cells(CellX, CellY)) = True Then .NumberFormat = defNumb$
      End If
      .VerticalAlignment = xlTop
      .WrapText = forceWrap
   End With

End Sub

Public Sub DecoInter(xRange$, Optional bgColorIdx, Optional txtColorIdx As Integer, Optional txtBold As Boolean, Optional hAlign As Long)

   With Range(xRange$)
      If bgColorIdx <> 0 Then
         .Interior.ColorIndex = bgColorIdx
         .Interior.Pattern = xlSolid
         .Interior.PatternColorIndex = xlAutomatic
      End If
      If txtColorIdx <> 0 Then .Font.ColorIndex = txtColorIdx
      .Font.Bold = txtBold
      If hAlign <> 0 Then .HorizontalAlignment = hAlign
   End With

End Sub

Public Sub uLine(xRange$, xMode As Integer)

   'xMode, 0=no grid, 1=hairline, 2=double-blue, 3=thick-red
   
   Dim m(3, 3) As Long
   m(0, 1) = xlNone:       m(0, 2) = xlNone:     m(0, 3) = xlNone
   m(1, 1) = xlContinuous: m(1, 2) = xlHairline: m(1, 3) = xlAutomatic
   m(2, 1) = xlDouble:     m(2, 2) = xlThick:    m(2, 3) = 55
   m(3, 1) = xlContinuous: m(3, 2) = xlMedium:   m(3, 3) = 3
   
   On Error Resume Next
   With Range(xRange$).Borders(xlEdgeBottom)
        .LineStyle = m(xMode, 1)
        .Weight = m(xMode, 2)
        .ColorIndex = m(xMode, 3)
   End With
   On Error GoTo 0

End Sub

Public Function InsideCodeLines() As Integer
   Dim vbc, tLines As Integer
   For Each vbc In Application.VBE.VBProjects("QFADDINS").VBComponents
      tLines = tLines + vbc.CodeModule.CountOfLines
   Next
   InsideCodeLines = tLines
End Function

Public Sub UpdateDocHead(LangMode As Integer)
   Dim xFsz As Integer
   If LangMode = 2 Then xFsz = 11 Else xFsz = 16
   DecoCell 1, 3, aCO$(LangMode), True, False, xlLeft
   Cells(1, 3).Font.Size = xFsz
End Sub

Public Sub DoSortTotal()
   DoSort 1
End Sub

Public Sub DoSortUnit()
   DoSort 2
End Sub

Public Sub DoSortQty()
   DoSort 3
End Sub

Public Sub DoSort(sMode As Integer) '1=total, 2=unit, 3=qty

   Dim sKey$(3, 3) 'mode,key
   Dim rRange$
   sKey$(1, 1) = "F11":   sKey$(1, 2) = "E11":   sKey$(1, 3) = "D11"
   sKey$(2, 1) = "E11":   sKey$(2, 2) = "F11":   sKey$(2, 3) = "D11"
   sKey$(3, 1) = "D11":   sKey$(3, 2) = "F11":   sKey$(3, 3) = "E11"
   
   SaveLocation True
   rRange$ = FirstDataRow & ":" & GetLastRow()
   Rows(rRange$).Sort Key1:=Range(sKey$(sMode, 1)), Order1:=xlDescending, _
      Key2:=Range(sKey$(sMode, 2)), Order2:=xlDescending, Key3:=Range(sKey$(sMode, 3)), _
      Order3:=xlDescending, Header:=xlGuess, OrderCustom:=1, MatchCase:=False, _
      Orientation:=xlTopToBottom
   DoRenumber
   RestLocation True
   
End Sub

Public Sub MaintLoader()
   If CopyFileFSO(MainServer$ + "qfSTART.xla", WhereIsAddins$("QFSTART"), "") = True Then
      MsgBox "QF-Starter Successfully Updated.", vbInformation
   Else
      MsgBox "QF-Starter Update FAILED !", vbExclamation
   End If
End Sub

Public Sub MaintTemplate()
   If CopyFileFSO(MainServer$ + qftem$, SysRoot$ + CacheRoot$ + "\" + qftem$, "") = True Then
      MsgBox "QF Template Successfully Updated.", vbInformation
   Else
      MsgBox "QF Template Update FAILED !", vbExclamation
   End If
End Sub

Public Function WhereIsAddins$(inName$)
   Dim adi
   For Each adi In Application.AddIns
      If InStr(1, adi.Name, inName$) > 0 Then
         WhereIsAddins$ = adi.Path + "\" + adi.Name
         Exit Function
      End If
   Next
End Function

Public Function MakeSimpleTR(CellX As Integer)

   DecoInter CellAddr$(CellX, 2, 6), xlAutomatic, xlAutomatic, True, xlLeft
   Cells(CellX, 2).Value = ""
   With Range(CellAddr$(CellX, 2, 6))
      .Borders(xlEdgeLeft).LineStyle = xlNone
      .Borders(xlEdgeRight).LineStyle = xlNone
      .Borders(xlInsideVertical).LineStyle = xlNone
   End With

End Function

Public Sub NoScreen()
Application.ScreenUpdating = False
End Sub

Public Sub PasteSp()
On Error Resume Next
   Selection.PasteSpecial Paste:=xlValues, Operation:=xlNone, SkipBlanks:=False, Transpose:=False
On Error GoTo 0
End Sub

Public Sub CopySp()
   Selection.Copy
End Sub

Public Sub ChangeToolbarCapts()
   Dim cb
   Set cb = Application.CommandBars(1).Controls(3)
   cb.Caption = "Vie&w"
End Sub

Public Function IsInvIdent() As Boolean
   If RealShSerial("invoice") = Val(Worksheets(RealShName$("invoice")).Cells(1, 5).Value) Then
      IsInvIdent = True
   Else
      IsInvIdent = False
   End If
End Function

Public Function IsConIdent() As Boolean

End Function

Public Sub Wait(wSec As Byte)
   Dim Start, totalTime
   Start = Timer
   Do While Timer < Start + wSec
      DoEvents
   Loop
End Sub

Public Function IsQF(wbName$) As Boolean
   Dim HasSheets As Integer, tBit As Integer
   HasSheets = SheetBits(wbName$)
   tBit = HasSheets And fsForm
   If tBit <= 0 Then IsQF = False Else IsQF = True
End Function

Public Sub CheckRefValid()

    Dim tmpRef$
    Dim i As Integer, Slash As Integer
    tmpRef$ = Worksheets("Form").Cells(7, 3)
    
    If Len(tmpRef$) < 12 Then GoTo BadRef
    If Asc(Left$(tmpRef$, 1)) < 65 Then GoTo BadRef
    If Val(Mid$(tmpRef$, 2, 3)) < 1 Then GoTo BadRef
    If Mid$(tmpRef$, 5, 1) <> "/" Then GoTo BadRef
    For i = 1 To Len(tmpRef$)
        If Mid$(tmpRef$, i, 1) = "/" Then Slash = Slash + 1
    Next
    If Slash <> 2 Then GoTo BadRef

    Exit Sub

BadRef:

    MsgBox "Document's Reference No. is incorrect" _
     + Chr$(10) + "Reference  will be automatically corrected now" _
     + Chr$(10) + "Pay Attention to the Ref.No Again Please!", vbExclamation, _
     "(QF) Bad Reference Detected..."
     
     Worksheets("Form").Cells(7, 3) = NewReference$()
 
End Sub
