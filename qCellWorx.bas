Attribute VB_Name = "qCellWorx"
Option Explicit
Option Compare Text

Public Function CellInfo(CellX As Integer, CellY As Integer, Optional shName$) As Integer

'Bits --------------------
'       0 if Empty
'1 = Number (1)
'2 = Text (2)
'3 = Formula (4)
'4 = Date (8)
'5 =
'6 =
'7 = Formula Error (64)
'8 = Procedure ERROR (128)

' Extended bits ----------
'9  = SUM Formula (256)
'10 = DigiWords Formula (512)
'11 = VAT 18% Formula (1024)
'12 = SUM+VAT Formula (2048)
  
  Dim cInfo As Integer
  Dim CellFrm$
  Dim cellData
    

On Error GoTo ErrHand

   If shName$ = "" Then shName$ = ActiveSheet.Name
   
   cellData = Worksheets(shName$).Cells(CellX, CellY)
   CellFrm$ = Worksheets(shName$).Cells(CellX, CellY).Formula
   
   If IsEmpty(cellData) Then CellInfo = 0: Exit Function
   If IsError(cellData) Then cInfo = 64 + 4: GoTo ExtMode
   If IsDate(cellData) Then CellInfo = 8: Exit Function
   If IsNumeric(cellData) Then cInfo = cInfo + 1
   If Left$(CellFrm$, 1) = "=" Then cInfo = cInfo + 4

   If cInfo = 0 Then CellInfo = 2: Exit Function
   
ExtMode:
   
   If cInfo - 4 >= 0 Then
      If Len(CellFrm$) > 5 Then
         If Left$(CellFrm$, 5) = "=SUM(" Then CellInfo = cInfo + 256: Exit Function
      End If
      If Len(CellFrm$) > 11 Then
         If Left$(CellFrm$, 11) = "=HowMuchIs(" Then cInfo = cInfo + 256
      End If
   End If
   
   CellInfo = cInfo

Exit Function

ErrHand:
   On Error GoTo 0
   CellInfo = cInfo + 128

End Function

Public Function BitGet(InByte As Integer, BitID As Integer) As Boolean
   Dim BitVal As Integer
   BitID = BitID - 1
   BitVal = 2 ^ BitID
   BitGet = InByte And BitVal
End Function

Public Function BitLog(InByte1 As Integer, InByte2 As Integer) As Boolean
   Dim tmpLog As Integer
   tmpLog = InByte1 And InByte2
   If tmpLog >= InByte2 Then BitLog = True Else BitLog = False
End Function

Public Function qRowType(xRow As Integer, Optional shName$, Optional usePO As Boolean, Optional inCF As Boolean) As Integer

'=0 - Empty (non data or formula row)
'=1 - qty / unit price / total row
'=2 - decorative row (group title)
'=3 - group subtotal row
'=4 - grand total row
'=5 - HowMuchIs formula row
'=6 - only formula in 6th column
'=7 - VAT 18%
'=8 - SUM + VAT
'=9 - GT w/ Error
'=10 - group title (2) when inCF

   Dim ci(2 To 6) As Integer
   Dim i As Integer

   If shName$ = "" Then shName$ = ActiveSheet.Name

   For i = 2 To 6
      ci(i) = CellInfo(xRow, i, shName$)
   Next

   qRowType = 0
   
   If BitLog(ci(4), bNum) Or BitLog(ci(5), bNum) Then
      If BitLog(ci(6), bFormula) Or BitLog(ci(6), bNum) Or BitLog(ci(3), bText) Then qRowType = 1
   End If
   
   If BitLog(ci(2), bText) And BitLog(ci(3), bText) And ci(5) = 0 And ci(6) = 0 Then
      qRowType = 2: Exit Function
   End If

   If BitLog(ci(5), bNum + bFormula + bFrmSUM) Then
      qRowType = 3: Exit Function
   End If
   
   If BitLog(ci(6), bNum + bFormula + bFrmSUM) Then
      qRowType = 4: Exit Function
   End If
   
   If BitLog(ci(6), bFrmErr + bFormula + bFrmSUM) Then
      qRowType = 9: Exit Function
   End If
   
   If BitLog(ci(6), bFormula + bFrmDIGI) Then
      qRowType = 5: Exit Function
   End If
  
   If BitLog(ci(4), bText) And BitLog(ci(5), bNum + bFormula + bFrmSUM) Then
      qRowType = 6: Exit Function
   End If
   
   If BitLog(ci(6), bNum + bFormula + bFrmVAT) Then
      qRowType = 7: Exit Function
   End If
   
   If BitLog(ci(6), bNum + bFormula + bFrmSUMVAT) Then
      qRowType = 8: Exit Function
   End If

   If BitLog(ci(3), bText) And usePO = True Then
      qRowType = 1: Exit Function
   End If

   If inCF = True Then
      If BitLog(ci(3), bText) = True And ci(2) = 0 And ci(4) = 0 And ci(5) = 0 And ci(6) = 0 Then qRowType = 10
   End If

End Function

Public Function qVerify(Optional KeepOutlines As Boolean, Optional CountOnly As Boolean, Optional DontSaveLoc As Boolean) As Integer

   Dim i As Integer, x As Integer, rType As Integer
   Dim fErrors As Integer, nErrors As Integer, lErrors As Integer, gErrors As Integer
   Dim tRng$, msgText$, vatCalibrator As Currency, vatCalFrm$
   Dim tQty As Double, tPrice As Double, tTotal As Double, tGrandTotal As Double
   Dim LastDataRow As Integer, DisVatTune As Boolean
   
   If GetMode() = 1 Then Exit Function
   If (SheetBits() And fsForm) <= 0 Then Exit Function
   If ActiveSheet.Name <> "form" Then Exit Function
   If GetIniPar("DisVatt") = "YES" Then DisVatTune = True
       
   If DontSaveLoc = False Then Application.ScreenUpdating = False
   Worksheets("form").Activate

On Error GoTo ErrHandler
 
rest:
   TotalGroups = 0
   fErrors = 0
   nErrors = 0
   lErrors = 0
   tGrandTotal = 0
   LastDataRow = FirstDataRow + 1
 
   For i = FirstDataRow To MaximumROWS

      rType = qRowType(i)
      
      If rType = 2 Then TotalGroups = TotalGroups + 1
      If CountOnly = True Then GoTo n
      
      If rType = 1 Then
         If IsError(Cells(i, 6)) Then
            fErrors = fErrors + 1
            tRng$ = CellAddr$(i, 4, 2)
            OutlineCells tRng$, 2
            GoTo n
         End If
      
         If BitGet(CellInfo(i, 4), 1) = False Or BitGet(CellInfo(i, 5), 1) = False Then
            nErrors = nErrors + 1
            tRng$ = CellAddr$(i, 4, 2)
            OutlineCells tRng$, 3
         Else
            tRng$ = CellAddr$(i, 4, 2)
            If KeepOutlines = False Then OutlineCells tRng$, 1, True
         End If
      
         tQty = Cells(i, 4).Value
         tPrice = Cells(i, 5).Value
         tTotal = Cells(i, 6).Value
      
         If tQty * tPrice <> tTotal Then
            lErrors = lErrors + 1
            Cells(i, 6).Formula = "=D" + Trim$(Str$(i)) + "*E" + Trim$(Str$(i))
            tTotal = Val(Cells(i, 6).Value)
         End If
      
         tGrandTotal = tGrandTotal + tTotal
         LastDataRow = i
      
      End If

      If i - LastDataRow >= 5 Then
         msgText$ = "Template Stucture is Corrupted !!!" + Chr$(10) + Chr$(10)
         msgText$ = msgText$ + "GrandTotal Formula '=SUM()' is missing (possibly deleted)." + Chr$(10) + Chr$(10)
         msgText$ = msgText$ + "Would you like try to restore the entire structure ?" + Chr$(10)
         msgText$ = msgText$ + "(and then verify process will be restarted)" + Chr$(10) + Chr$(10)
         x = MsgBox(msgText$, vbCritical + vbYesNo, " BrainEngine(R), Error:5000")
      
         If x = 7 Or 0 Then
            qVerify = 2002
            GoTo q1
         Else
            RestoreGrandTotal LastDataRow
            GoTo rest
         End If
      End If

      If rType = 4 Or rType = 9 Then
       
         Cells(i + 1, 10).Formula = "=SUM(J" + Trim$(Str$(FirstDataRow)) + ":J" + Trim$(Str$(i - 1)) + ")"
         Cells(i + 1, 11).Formula = "=ROUND(" + CellAddr$(i, 6) + "/100*18,0)"
         
         If fErrors > 0 Then
            msgText$ = fErrors & " Critical Cell Error(s) were found." & Chr$(10)
            msgText$ = msgText$ & "All Errors were marked with RED Outline," & Chr$(10)
            msgText$ = msgText$ & "Please Correct them and Restart Verification."
            MsgBox msgText$, vbCritical, "(QF) BrainEngine, Error Message !"
            qVerify = 1000
            GoTo q1
         End If
         
         If Cells(i, 6).Value <> tGrandTotal Then
            If gErrors > 3 Then GoTo DeathCircle2
            gErrors = gErrors + 1
            Cells(i, 6).Formula = "=SUM(F" + Trim$(Str$(FirstDataRow)) + ":F" + Trim$(Str$(i - 1)) + ")"
            GoTo rest
         End If
         
         If gErrors >= 1 Then
            msgText$ = "GRAND TOTAL Error was found !" + Chr$(10)
            msgText$ = msgText$ & "BrainEngine has corrected this error Successfully."
            MsgBox msgText$, vbInformation, "(QF) BrainEngine, Success Correction..."
         End If
      
         If lErrors >= 1 Then
            msgText$ = lErrors & " InLine Formula Error(s) were found." & Chr$(10)
            msgText$ = msgText$ & "BrainEngine has corrected this error(s) Successfully."
            MsgBox msgText$, vbInformation, "(QF) BrainEngine, Success Correction..."
         End If
      
         If nErrors >= 1 Then
            msgText$ = nErrors & " Zero-Values and/or Not-Numeric cells detected." & Chr$(10)
            msgText$ = msgText$ & "BrainEngine has marked these cells with BLUE Outline." & Chr$(10)
            msgText$ = msgText$ & "Please pay attention to those cells !"
            MsgBox msgText$, vbInformation, "(QF) BrainEngine, Notification !"
         End If
      
         If GetVATmode() = 1 And CurrentCurrency() = 2 Then
            If Cells(i + 1, 6).Formula = 0 Then GoTo ex
            AddVAT
            If DisVatTune = True Then
                vatCalFrm$ = Cells(i + 1, 6).Formula
            Else
                vatCalibrator = Cells(i + 1, 10).Value - Cells(i + 1, 11).Value
                vatCalFrm$ = Cells(i + 1, 6).Formula + "+" + Trim$(Str$(vatCalibrator))
            End If
            Cells(i + 1, 6).Formula = vatCalFrm$
         End If

ex:         qVerify = 0
            GoTo q1
      End If
n: Next

GoTo q1

DeathCircle:
   MsgBox "!!!! Template Malfunctions Detected !!!!" + Chr$(10) + "Please Restart Verification !!!", vbCritical, "(QF) Critical State !"
   qVerify = -2
   GoTo q1

DeathCircle2:
   MsgBox "'Unit Price' column content something wrong !!" + Chr$(10) + "Please Correct and Restart Verification !!!", vbCritical, "(QF) Critical State !"
   qVerify = -3
   GoTo q1
   
ErrHandler:
   msgText$ = "Sorry, Internal Error has Occured." + Chr$(10)
   msgText$ = msgText$ + "Unfortunately, BrainEngine can not recognize dependencies." + Chr$(10) + Chr$(10)
   msgText$ = msgText$ + "Verification is NOT Completed, Due to Error # " & Err.Number
   MsgBox msgText$, vbCritical, "(QF) Internal Error."
   qVerify = -4
q1:
   On Error GoTo 0
   If DontSaveLoc = False Then Application.ScreenUpdating = True
End Function

Public Function FormulaRow(inPar$, Optional shName$, Optional sRange$) As Integer

   Dim LocX As Integer, saveShName$, c
   Dim RowAbsolute, ColumnAbsolute
        
   If shName$ <> "" Then
      saveShName$ = ActiveSheet.Name
      Worksheets(shName$).Activate
   End If
        
   If sRange$ = "" Then sRange$ = qsAREA$
   Set c = ActiveSheet.Range(sRange$).Find(What:=inPar$, LookIn:=xlFormulas, LookAt:= _
       xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, MatchCase:=False)
    
   If c Is Nothing Then FormulaRow = 0: GoTo ex
      
   LocX = Val(Mid$(c.Address(RowAbsolute, ColumnAbsolute), 2))
   FormulaRow = LocX
ex:
   If shName$ <> "" Then Worksheets(saveShName$).Activate
   
End Function

Public Function preVerify(Optional screamMode As Boolean, Optional DontSaveLoc As Boolean) As Boolean
   
   If DontSaveLoc = False Then NoScreen
   DoRenumber
   If qVerify(, , DontSaveLoc) = 0 Then
      preVerify = True
   Else
      preVerify = False
      If screamMode = True Then
         MsgBox "The FORM contain Errors !" + Chr$(10) + "Your current action will be Stopped !", vbCritical, "(QF) Verification Errors"
      End If
   End If
      
End Function
