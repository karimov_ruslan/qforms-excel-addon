Attribute VB_Name = "digiwords"

Public Function HowMuchIs(inpnum As Double, curr As String, lang As String) As String
lang$ = Left$(Trim$(UCase$(lang$)), 1)
  Select Case lang$
      Case "A"
        If curr = "AZM" Then curr = "Manat"
        HowMuchIs = azHowMuch(inpnum, curr)
        Exit Function
      Case "R"
        If curr = "AZM" Then curr = "�����"
        HowMuchIs = rusHowMuch(inpnum, curr)
        Exit Function
      Case "E"
        HowMuchIs = engHowMuch(inpnum, curr)
        Exit Function
  End Select
        
    HowMuchIs = "Wrong Language Option! (Usage: R=Russian, E=English, A=Azeri)"

End Function

Public Function rusHowMuch(inpnum As Double, curr As String) As String

CurrencyType$ = curr

Dim rng$(10)
Dim Cycles As Single
Dim InpVal As String
Dim Rounds$(5, 4)
Dim Gender$(4)

Dim ShowNum$

Rounds$(1, 1) = "������"
Rounds$(1, 2) = "������"
Rounds$(1, 3) = "�����"
Rounds$(2, 1) = "������"
Rounds$(2, 2) = "��������"
Rounds$(2, 3) = "���������"
Rounds$(3, 1) = "��������"
Rounds$(3, 2) = "���������"
Rounds$(3, 3) = "����������"
Rounds$(4, 1) = "�����"
Rounds$(4, 2) = "������"
Rounds$(4, 3) = "�������"
Gender$(0) = 0

Gender$(1) = 1
Gender$(2) = 0
Gender$(3) = 0
Gender$(4) = 0

If inpnum = 0 Then ShowNum$ = "���� ": GoTo Go

InpVal = Trim$(Str$(Int(inpnum)))

Cycles = Len(InpVal) / 3

If Cycles <> Int(Cycles) Then
    Cycles = Int(Cycles) + 1
    xx = Cycles * 3 - Len(InpVal)
    InpVal$ = String$(xx, "0") + InpVal$
End If

For z = Cycles To 1 Step -1
    rng$(z) = Mid$(InpVal, 3 * z - 2, 3)
    If Right$(rng$(z), 1) = "1" Then idx = 1 Else If (Right$(rng$(z), 1) = "2") Or (Right$(rng$(z), 1) = "3") Or (Right$(rng$(z), 1) = "4") Then idx = 2 Else idx = 3
    If Val(rng$(z)) = 0 Then idx = 1
    If Mid$(rng$(z), 2, 1) = "1" Then idx = 3
    ShowNum$ = rusTellMe$(rng$(z), Gender$(Cycles - z)) + " " + Rounds$(Cycles - z, idx) + ShowNum$
Next

Go:
rusHowMuch = LTrim$(ShowNum$ + "" + CurrencyType$)

End Function
Private Function rusTellMe$(InpDigits$, Genderr$)

Dim Tell2$
Dim Nums$(0 To 900)

Nums$(0) = ""
Nums$(1) = "����"
Nums$(2) = "���"
Nums$(3) = "���"
Nums$(4) = "������"
Nums$(5) = "����"
Nums$(6) = "�����"
Nums$(7) = "����"
Nums$(8) = "������"
Nums$(9) = "������"
Nums$(10) = "������"
Nums$(11) = "�����������"
Nums$(12) = "����������"
Nums$(13) = "����������"
Nums$(14) = "������������"
Nums$(15) = "����������"
Nums$(16) = "�����������"
Nums$(17) = "����������"
Nums$(18) = "������������"
Nums$(19) = "������������"
Nums$(20) = "��������"
Nums$(30) = "��������"
Nums$(40) = "�����"
Nums$(50) = "���������"
Nums$(60) = "����������"
Nums$(70) = "���������"
Nums$(80) = "�����������"
Nums$(90) = "���������"
Nums$(100) = "���"
Nums$(101) = "����"
Nums$(102) = "���"
Nums$(103) = ""
Nums$(200) = "������"
Nums$(300) = "������"
Nums$(400) = "���������"
Nums$(500) = "�������"
Nums$(600) = "��������"
Nums$(700) = "�������"
Nums$(800) = "���������"
Nums$(900) = "���������"

Right2 = Val(Right$(InpDigits$, 2))

If Val(InpDigits$) = 0 Then rusTellMe$ = "": Exit Function 'zero

If Right2 = 0 Then Tell2$ = "": GoTo Full3
If (Right2 <= 20) And (Right2 >= 10) Then Tell2$ = Trim$(Nums$(Right2)): GoTo Full3

x = Int(Right2 / 10) * 10
y = (Right2 Mod 10)

If x = 0 Then x = 103
If (y < 3) And (y > 0) Then y = y + Genderr$ * 100 '���

If y = 0 Then
    Tell2$ = Trim$(Nums$(x))
Else
    Tell2$ = Trim$(Nums$(x)) + " " + Trim$(Nums$(y))
End If

Full3:
    If Trim$(Str$(Left$(InpDigits$, 1))) = "0" Then
        rusTellMe$ = " " + Tell2$
    Else
        rusTellMe$ = " " + Nums$(Val(Trim$(Left$(InpDigits$, 1))) * 100) + " " + Tell2$
    End If

End Function
Public Function engHowMuch(inpnum As Double, curr As String) As String

CurrencyType$ = curr

Dim rng$(10)
Dim Cycles As Single
Dim InpVal As String
Dim Rounds$(4, 3)
Dim ShowNum$

Rounds$(0, 1) = ""
Rounds$(0, 2) = ""
Rounds$(1, 1) = "Thousand"
Rounds$(1, 2) = "Thousands"
Rounds$(2, 1) = "Million"
Rounds$(2, 2) = "Millions"
Rounds$(3, 1) = "Billion"
Rounds$(3, 2) = "Billions"

If inpnum = 0 Then ShowNum$ = "Zero ": GoTo Go
InpVal = Trim(Str$(Int(inpnum)))
Cycles = Len(InpVal) / 3

If Cycles <> Int(Cycles) Then
    Cycles = Int(Cycles) + 1
    xx = Cycles * 3 - Len(InpVal)
    InpVal$ = String$(xx, "0") + InpVal$
End If

For z = Cycles To 1 Step -1
    rng$(z) = Mid$(InpVal, 3 * z - 2, 3)
    If Right$(rng$(z), 1) = "1" Then idx = 1 Else idx = "2"
    If rng$(z) = "000" Then idx = 0
    ShowNum$ = TellMe$(rng$(z)) + " " + Rounds$(Cycles - z, idx) + ShowNum$
Next

Go: engHowMuch = ShowNum$ + "" + CurrencyType$

End Function

Private Function TellMe$(InpDigits$)

Dim Tell2$
Dim Nums$(0 To 100)

Nums$(0) = ""
Nums$(1) = "One"
Nums$(2) = "Two"
Nums$(3) = "Three"
Nums$(4) = "Four"
Nums$(5) = "Five"
Nums$(6) = "Six"
Nums$(7) = "Seven"
Nums$(8) = "Eight"
Nums$(9) = "Nine"
Nums$(10) = "Ten"
Nums$(11) = "Eleven"
Nums$(12) = "Twelve"
Nums$(13) = "Thirteen"
Nums$(14) = "Fourteen"
Nums$(15) = "Fifteen"
Nums$(16) = "Sixteen"
Nums$(17) = "Seventeen"
Nums$(18) = "Eighteen"
Nums$(19) = "Nineteen"
Nums$(20) = "Twenty"
Nums$(30) = "Thirty"
Nums$(40) = "Forty"
Nums$(50) = "Fifty"
Nums$(60) = "Sixty"
Nums$(70) = "Seventy"
Nums$(80) = "Eighty"
Nums$(90) = "Ninety"
Nums$(100) = "Hundreed"

Right2 = Val(Right$(InpDigits$, 2))

If Val(InpDigits$) = 0 Then TellMe$ = "": Exit Function
If Right2 = 0 Then Tell2$ = "": GoTo Full3
If Right2 <= 20 Then Tell2$ = Trim$(Nums$(Right2)): GoTo Full3

x = Int(Right2 / 10) * 10
y = Right2 Mod 10

If y = 0 Then
    Tell2$ = Trim$(Nums$(x))
Else
    Tell2$ = Trim$(Nums$(x)) + " " + Trim$(Nums$(y))
End If

Full3:

    If Trim$(Str$(Left$(InpDigits$, 1))) = "0" Then
        TellMe$ = " " + Tell2$
    Else
        TellMe$ = " " + Nums$(Val(Trim$(Left$(InpDigits$, 1)))) + " Hundred " + Tell2$
    End If

End Function

Public Function azHowMuch(inpnum As Double, curr As String) As String

CurrencyType$ = curr

Dim rng$(10)
Dim Cycles As Single
Dim InpVal As String
Dim Rounds$(4, 3)
Dim ShowNum$

Rounds$(0, 1) = ""
Rounds$(0, 2) = ""
Rounds$(1, 1) = "���"
Rounds$(1, 2) = "���"
Rounds$(2, 1) = "������"
Rounds$(2, 2) = "Millions"
Rounds$(3, 1) = "�������"
Rounds$(3, 2) = "Billions"

If inpnum = 0 Then ShowNum$ = "����� ": GoTo Go

InpVal = Trim$(Str$(Int(inpnum)))
Cycles = Len(InpVal) / 3

If Cycles <> Int(Cycles) Then
    Cycles = Int(Cycles) + 1
    xx = Cycles * 3 - Len(InpVal)
    InpVal$ = String$(xx, "0") + InpVal$
End If

For z = Cycles To 1 Step -1
    rng$(z) = Mid$(InpVal, 3 * z - 2, 3)
    idx = "1"
    If rng$(z) = "000" Then idx = 0
    ShowNum$ = AzbTellMe$(rng$(z)) + " " + Rounds$(Cycles - z, idx) + ShowNum$
Next

Go: azHowMuch = LTrim$(ShowNum$ + "" + CurrencyType$)

End Function

Private Function AzbTellMe$(InpDigits$)

Dim Tell2$
Dim Nums$(0 To 100)

Nums$(0) = ""
Nums$(1) = "���"
Nums$(2) = "���"
Nums$(3) = "��"
Nums$(4) = "����"
Nums$(5) = "���"
Nums$(6) = "����"
Nums$(7) = "�����"
Nums$(8) = "������"
Nums$(9) = "������"
Nums$(10) = "��"
Nums$(20) = "������"
Nums$(30) = "����"
Nums$(40) = "����"
Nums$(50) = "����"
Nums$(60) = "������"
Nums$(70) = "������"
Nums$(80) = "������"
Nums$(90) = "������"
Nums$(100) = "���"

Right2 = Val(Right$(InpDigits$, 2))

If Val(InpDigits$) = 0 Then AzbTellMe$ = "": Exit Function
If Right2 = 0 Then Tell2$ = "": GoTo Full3

x = Int(Right2 / 10) * 10
y = Right2 Mod 10

If y = 0 Then
    Tell2$ = Trim$(Nums$(x))
Else
    Tell2$ = Trim$(Nums$(x)) + " " + Trim$(Nums$(y))
End If

Full3:
    If Trim$(Str$(Left$(InpDigits$, 1))) = "0" Then
        AzbTellMe$ = " " + Tell2$
    Else
        AzbTellMe$ = " " + Nums$(Val(Trim$(Left$(InpDigits$, 1)))) + " ��� " + Tell2$
    End If
End Function
