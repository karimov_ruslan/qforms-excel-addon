Attribute VB_Name = "HTMLmod"
Option Explicit
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private LastWord$(3)
Private CR$
Private II$
Private HTMLbuffer$
Public Sub MakeHTMLFile()
MakeHTML 1
End Sub

Public Sub MakeEMLFile()
If GetIniPar("MailClient") = "Outlook" Then
   MakeHTML 1, True
Else
   MakeHTML 2
End If
End Sub

Private Sub MakeHTML(xMode, Optional forOutlook As Boolean)
    
    If GetMode() = 3 Then Exit Sub
    
    If CurrentLang() = 3 Then
        MsgBox "Sorry, WebOffer is not available in Azerbaijan Languge!"
        Exit Sub
    End If
    
    If GetMode() = 2 Then
        If qVerify() > 0 Then
            MsgBox "Sorry, WebOffer creation is impossible due to errors !"
            Exit Sub
        End If
    End If
    
' the process --------------
    
    CR$ = Chr$(13) + Chr$(10)
    II$ = Chr$(34)
    
    LastWord$(1) = "Sincerely,"
    LastWord$(2) = "� ���������,"
    
    HTMLbuffer$ = ""
    
    PreloadTemplate qfweb$
    BuildHead
    BuildDataArea
    BuildEnd
    WriteHTML xMode, forOutlook

End Sub

Private Sub BuildHead()
    
    Dim i As Integer
    Dim HTMLdata$
    
    i = 1
    HTMLdata$ = ""
    While HTMLdata$ <> "[HEAD END]"
        HTMLbuffer$ = HTMLbuffer$ + DataParser$(HTMLdata$)
        HTMLdata$ = Workbooks("qfweb.xla").Worksheets("htmlhead").Cells(i, 2).Value
        i = i + 1
    Wend

End Sub

Private Sub BuildDataArea()
    
    Dim GTrow As Integer, GTWrow As Integer, i As Integer
    
    Worksheets("Form").Activate
    GTrow = FormulaRow(qsGT$)
    GTWrow = FormulaRow(qsDIGI$)
    
    For i = FirstDataRow To GTrow - 2
        HTMLbuffer$ = HTMLbuffer$ + HTMLrow$(i)
    Next
    
    If GetMode() = 1 Then
        HTMLbuffer$ = HTMLbuffer$ + "</table>" + CR$
        Exit Sub
    End If
    
    HTMLbuffer$ = HTMLbuffer$ + RowGT$(GTrow)
    HTMLbuffer$ = HTMLbuffer$ + RowGTW$(GTWrow)
    HTMLbuffer$ = HTMLbuffer$ + RowEmpty$() + RowEmpty$() + "</table>" + CR$
    
End Sub

Private Sub BuildEnd()

    Dim EmailTo$, tmp$
    
    HTMLbuffer$ = HTMLbuffer$ + RowDesc$("Text Box 66")
    
    If GetMode() = 1 Then GoTo CloseHTML

' Build Confirmation Button ---------------------------------
    
If GetIniPar$("Ready2Buy") = "YES" Then
    EmailTo$ = GetQFemail$(DocumentOwner(), "")
    tmp$ = "<tr><td class=" + II$ + "terms" + II$ + "><form name=" + II$ + "QFsubmit" + II$ + " action=" + II$ + "http://www.risk.az/cgi-bin/cgiemail/qforms.txt" + II$ + " method=" + II$ + "post" + II$ + " target=" + II$ + "_top" + II$ + ">"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + "http://www.risk.az/forms/submitted.html" + II$ + " name=" + II$ + "success" + II$ + ">"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + EmailTo$ + II$ + " name=" + II$ + "sp" + II$ + ">"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + DataParser$("###C4") + II$ + " name=" + II$ + "company" + II$ + "><BR>"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + DataParser$("###C5") + II$ + " name=" + II$ + "name" + II$ + ">"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + DataParser$("###C6") + II$ + " name=" + II$ + "odate" + II$ + ">"
    tmp$ = tmp$ + "<input type=" + II$ + "hidden" + II$ + " value=" + II$ + DataParser$("###C7") + II$ + " name=" + II$ + "oref" + II$ + "><BR>"
    tmp$ = tmp$ + "<input type=" + II$ + "submit" + II$ + " name=" + II$ + "SB" + II$ + " value=" + II$ + "I'm Ready to Buy, Contact Me." + II$ + ">"
    tmp$ = tmp$ + "</form></td></tr>"
    HTMLbuffer$ = HTMLbuffer$ + tmp$ + CR$
End If

CloseHTML:
    HTMLbuffer = HTMLbuffer$ + "</table></div></body></html>"

End Sub

Private Sub WriteHTML(xMode, Optional forOutlook As Boolean)

    Dim HTMLpath$, HTMLfile$, EMLpath$, EMLfile$, EMLbuffer$
    Dim ExecID, SW_SHOWMAXIMIZED, oParam$

    HTMLpath$ = QFHTMLpath$
    HTMLfile$ = HTMLpath$ + WhatFile$(0) + ".html"
    EMLpath$ = QFTempPath$
    EMLfile$ = EMLpath$ + WhatFile$(0) + ".eml"

   If xMode = 1 Then
      Open HTMLfile$ For Output As 1
      Print #1, HTMLbuffer$
      Close #1
      If forOutlook = True Then oParam$ = "outlook.exe " Else oParam$ = "explorer "
      ExecID = Shell(oParam$ + HTMLfile$, vbNormalFocus)
      Exit Sub
   End If
    
    If xMode = 2 Then
        EMLbuffer$ = Workbooks("qfweb.xla").Worksheets("emailhead").Cells(1, 2).Value
        HTMLbuffer$ = ReplaceChar$(EMLbuffer$, Chr$(10), CR$) + CR$ + HTMLbuffer$
        Open EMLfile$ For Output As 1
        Print #1, HTMLbuffer$
        Close #1
        Dim ret As Long
        ExecID = ShellExecute(0&, "Open", EMLfile$, 0&, 0&, SW_SHOWMAXIMIZED)
    End If

End Sub

Private Function DataParser$(inputData$)
        
    Dim DataRange$
    Dim CellY As Integer, CellX As Integer
        
    If inputData$ = "###CODEPAGE" Then
        DataParser$ = CodePage$(CurrentLang())
        Exit Function
    End If
    
    If Left$(inputData$, 3) = "###" Then
        DataRange$ = UCase$(Trim$(Mid$(inputData$, 4)))
        CellY = Asc(Mid$(inputData$, 4, 1)) - 64
        CellX = Val(Mid$(inputData$, 5))
        DataParser$ = UseBR$(Worksheets("Form").Cells(CellX, CellY).Value)
        Exit Function
    End If

    DataParser$ = inputData$

End Function

Private Function CodePage$(LangMode)
    
    Dim cP$(3)
    cP$(1) = "<meta http-equiv=" + II$ + "content-type" + II$ + " content=" + II$ + "text/html;charset=iso-8859-1" + II$ + ">"
    cP$(2) = "<meta http-equiv=" + II$ + "content-type" + II$ + " content=" + II$ + "text/html;charset=windows-1251" + II$ + ">"
    cP$(3) = ""

    CodePage$ = cP$(LangMode)

End Function

Private Function UseBR$(inpText$)
    UseBR$ = ReplaceChar$(inpText$, Chr$(10), "<BR>")
End Function

Private Function RowStd$(SourceRow)

    Dim oMode As Integer
    Dim d2$, d3$, d4$, d5$, d6$, d7$, tmp$

        oMode = GetMode()

    With Worksheets("Form")
    
        d2$ = .Cells(SourceRow, 2).Value
        d3$ = UseBR$(.Cells(SourceRow, 3).Value)
        If oMode = 1 Then d4$ = "" Else d4$ = .Cells(SourceRow, 4).Value
        d5$ = Format(.Cells(SourceRow, 5).Value, defNumb$)
        If oMode = 1 Then d6$ = "" Else d6$ = Format(.Cells(SourceRow, 6).Value, defNumb$)
        d7$ = .Cells(SourceRow, 7).Value
    
    End With

tmp$ = "<tr><td class=" + II$ + "c1" + II$ + ">" + d2$ + "</td>"
tmp$ = tmp$ + "<td class=" + II$ + "c2" + II$ + ">" + d3$ + "</td>"
tmp$ = tmp$ + "<td class=" + II$ + "c1" + II$ + ">" + d4$ + "</td>"
tmp$ = tmp$ + "<td class=" + II$ + "c4" + II$ + ">" + d5$ + "</td>"
tmp$ = tmp$ + "<td class=" + II$ + "c4" + II$ + ">" + d6$ + "</td>"
tmp$ = tmp$ + "<td class=" + II$ + "c1" + II$ + ">" + d7$ + "</td></tr>"

    RowStd$ = tmp$ + CR$

End Function

Private Function RowEmpty$(Optional SourceRow)
    
    Dim tmp$
    
    tmp$ = "<tr height=" + II$ + "10" + II$ + "><td height=" + II$ + "10" + II$ + " colspan=" + II$ + "6" + II$ + "></td></tr>"
    RowEmpty$ = tmp$ + CR$

End Function

Private Function RowST$(SourceRow)

    Dim STtext$, STdata$, tmp$
    
    STtext$ = Worksheets("Form").Cells(SourceRow, 4).Value
    STdata$ = Worksheets("Form").Cells(SourceRow, 5).Value
    tmp$ = "<tr><td colspan=" + II$ + "3" + II$ + "></td><td class=" + II$ + "st" + II$ + ">" + STtext$
    tmp$ = tmp$ + "</td><td class=" + II$ + "st" + II$ + ">" + Format(STdata$, defNumb$)
    tmp$ = tmp$ + "</td><td></td></tr>"
    RowST$ = tmp$ + CR$

End Function

Private Function RowGRP$(SourceRow)
        
    Dim GrpName$, tmp$
    
    GrpName$ = Worksheets("Form").Cells(SourceRow, 3).Value
    tmp$ = "<tr><td class=" + II$ + "grp" + II$ + " width=" + II$ + "25" + II$ + "></td><td class=" + II$ + "grp" + II$ + " colspan=" + II$ + "5" + II$ + ">"
    tmp$ = tmp$ + GrpName$ + "</td></tr>"
    RowGRP$ = tmp$ + CR$
    
End Function

Private Function RowGT$(SourceRow)

    Dim GTtext$, GTdata$, tmp$
        
    GTtext$ = Worksheets("Form").Cells(SourceRow, 4).Value
    GTdata$ = Format(Worksheets("Form").Cells(SourceRow, 6).Value, defNumb$)

tmp$ = "<tr><td colspan=" + II$ + "4" + II$ + " class=" + II$ + "gt" + II$ + ">" + GTtext$
tmp$ = tmp$ + "</td><td class=" + II$ + "gt" + II$ + ">" + GTdata$
tmp$ = tmp$ + "</td><td></td></tr>"

    RowGT$ = tmp$ + CR$

    If GetVATmode() <> 1 Then Exit Function

    GTtext$ = Worksheets("Form").Cells(SourceRow + 1, 4).Value
    GTdata$ = Format(Worksheets("Form").Cells(SourceRow + 1, 6).Value, defNumb$)
tmp$ = tmp$ + "<tr><td colspan=" + II$ + "4" + II$ + " class=" + II$ + "gt" + II$ + ">" + GTtext$
tmp$ = tmp$ + "</td><td class=" + II$ + "gt" + II$ + ">" + GTdata$
tmp$ = tmp$ + "</td><td></td></tr>"
    GTtext$ = Worksheets("Form").Cells(SourceRow + 2, 4).Value
    GTdata$ = Format(Worksheets("Form").Cells(SourceRow + 2, 6).Value, defNumb$)
tmp$ = tmp$ + "<tr><td colspan=" + II$ + "4" + II$ + " class=" + II$ + "gt" + II$ + ">" + GTtext$
tmp$ = tmp$ + "</td><td class=" + II$ + "gt" + II$ + ">" + GTdata$
tmp$ = tmp$ + "</td><td></td></tr>"

    RowGT$ = tmp$ + CR$

End Function

Private Function RowGTW$(SourceRow)

    Dim GTWtext$, tmp$

    GTWtext$ = Worksheets("Form").Cells(SourceRow, 6).Value

tmp$ = "<tr><td colspan=" + II$ + "5" + II$ + " class=" + II$ + "gtw" + II$ + ">" + GTWtext$
tmp$ = tmp$ + "</td><td></td></tr>"

    RowGTW$ = tmp$ + CR$

End Function

Private Function RowDesc$(sourceShape$)

    Dim DescText$, EmailTo$, tmp$
    Dim LangMode As Integer

On Error Resume Next

    Worksheets("form").Shapes(sourceShape$).Select
    DescText$ = UseBR$(Selection.Characters.Text)

On Error GoTo 0
    
    EmailTo$ = GetQFemail$(DocumentOwner(), "")
    LangMode = CurrentLang()

tmp$ = "<tr><td class=" + II$ + "terms" + II$ + "><br>" + DescText$
tmp$ = tmp$ + "<BR><BR><div align=" + II$ + "left" + II$ + "><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + LastWord$(LangMode)
tmp$ = tmp$ + "<br><br><br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + SubmitterName$(LangMode)
tmp$ = tmp$ + "</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=" + II$ + "mailto:" + EmailTo$ + II$ + ">" + EmailTo$
tmp$ = tmp$ + "<br><br></a></p></div></td></tr>"
   
    RowDesc$ = tmp$ + CR$
   
End Function

Private Function HTMLrow$(SourceRow As Integer)

    Dim RowType As Integer

    HTMLrow$ = ""
        
    Worksheets("Form").Activate
    RowType = qRowType(SourceRow)

    Select Case RowType
        Case 0
            HTMLrow$ = RowEmpty$(SourceRow)
        Case 1
            HTMLrow$ = RowStd$(SourceRow)
        Case 2
            HTMLrow$ = RowGRP$(SourceRow)
        Case 3
            HTMLrow$ = RowST$(SourceRow)
    End Select

End Function

