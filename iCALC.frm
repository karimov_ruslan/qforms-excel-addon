VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} iCALC 
   Caption         =   "QF Calculator"
   ClientHeight    =   2160
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5070
   OleObjectBlob   =   "iCALC.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "iCALC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False





Private Sub cancelCalcs_Click()
    CalcCancel = True
    iCALC.Hide
    Unload iCALC
End Sub

Private Sub crMode1_Click()
    ChangeExample
End Sub

Private Sub crMode2_Click()
    ChangeExample
End Sub

Private Sub crMode3_Click()
    ChangeExample
End Sub

Private Sub doCalcs_Click()
    
    CalcValue$ = inValue.Text
    CalcNoComments = NoComments.Value
    
    If crMode1.Value = True Then CalcRoundMode = 1
    If crMode2.Value = True Then CalcRoundMode = 2
    If crMode3.Value = True Then CalcRoundMode = 3
    
    CalcCancel = False
    iCALC.Hide
    Unload iCALC

End Sub

Private Sub UserForm_Activate()

   CalcCancel = True
   NoComments.Value = False
    
   Dim opTP$(5)
   Dim valM$(5)
   Dim CurrMode As Integer, ccTag$, RevCCTag$
   
   CurrMode = CurrentCurrency()
   ccTag$ = CurrTagName$
   RevCCTag$ = RevCurrTagName$

   If Val(Application.Version) < 9 Then
      crMode1.Enabled = False
      crMode2.Enabled = False
      crMode3.Enabled = False
   End If

   If CalcAzmEuro = True And CalcOpMode <= 2 Then
      opTP$(2) = " Convert AZM > EURO"
      valM$(2) = "1EURO=XXX AZM"
      CalcCmt$(2) = "AZM>EURO @"
      GoTo nx1
   End If
      
   If CalcEuro = True And CalcOpMode <= 2 Then
      opTP$(1) = " Convert USD > EURO"
      opTP$(2) = " Convert EURO > USD"
      valM$(1) = "1USD=XXX EURO"
      valM$(2) = "1USD=XXX EURO"
      CalcCmt$(1) = "USD>EURO @"
      CalcCmt$(2) = "EURO>USD @"
      GoTo nx1
   End If
      
   opTP$(1) = " Convert " + ccTag$ + " > AZM"
   opTP$(2) = " Convert AZM > USD"
   opTP$(3) = " Add % to Amount (+%)"
   opTP$(4) = " Deduct % from Amount (-%)"
   opTP$(5) = " Add PB% to Amount"
   valM$(1) = "1" + ccTag$ + "=XXX AZM"
   valM$(2) = "1USD=XXX AZM"
   valM$(3) = "%"
   valM$(4) = "%"
   valM$(5) = "%PB"
   CalcCmt$(1) = ccTag$ + ">AZM @"
   CalcCmt$(2) = "AZM>USD @"

nx1:
   If CurrMode = 1 Then crMode3.Value = True Else crMode2.Value = True
   opType.Caption = opTP$(CalcOpMode)
   valueMSG.Caption = valM$(CalcOpMode)
        
   ChangeExample

End Sub

Private Sub ChangeExample()

    If crMode1.Value = True Then exMSG.Caption = "51750.7584"
    If crMode2.Value = True Then exMSG.Caption = "51751."
    If crMode3.Value = True Then exMSG.Caption = "51750.76"

End Sub
