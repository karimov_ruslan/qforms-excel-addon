VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} TIN 
   Caption         =   "QF> TIN (VOIN) Smart Lookup..."
   ClientHeight    =   4140
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5850
   OleObjectBlob   =   "TIN.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "TIN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Option Explicit
Option Compare Text

Private Sub cmdAdd_Click()
   cTIN$ = strEsc$
   Me.Hide
   Unload Me
   Load TINadd
   TINadd.Show
End Sub

Private Sub cmdCancel_Click()
   cTIN$ = strEsc$
   TIN.Hide
   Unload TIN
End Sub

Private Sub cmdSelect_Click()
   cTIN$ = Format$(tinNumb(UniRowIndex(Custs.ListIndex + 1)), TINfmt$)
   TIN.Hide
   Unload TIN
End Sub

Private Sub Custs_Change()
   UpdateInfo
End Sub

Private Sub UserForm_Activate()
   
   Dim i As Integer
   
   cTIN$ = strEsc$
   Custs.Clear
   For i = 1 To TotalUni
      Custs.AddItem tinClient$(i)
   Next
   Custs.Selected(0) = True
   sQuery.Caption = cCustomer$
   UpdateInfo
   
End Sub

Private Sub UpdateInfo()

   Dim idx As Integer
   idx = Custs.ListIndex + 1
   sMatch.Caption = UniScore(idx) & " %"
   sCust.Caption = tinClient$(idx)
   sTIN.Caption = Format$(tinNumb(UniRowIndex(idx)), TINfmt$)

End Sub
