Attribute VB_Name = "ConMod"
Option Explicit

Public Sub DoCell(xRow, xCol, shName$)

    Dim LangMode As Integer, i As Integer, zResult As Integer
    Dim notCompleted As Integer
    Dim DraftData$, ReplaceWith$, tag$
        
    LangMode = CurrentLang()
    DraftData$ = Workbooks(QFCmodName$).Worksheets(ConTemplate$(LangMode)).Cells(xRow, xCol)

    If Trim$(DraftData$) = "" Then
        Worksheets(shName$).Cells(xRow, xCol) = ""
        Exit Sub
    End If

DoAgain:

    For i = 1 To TotalParLines
        tag$ = ParLines$(i, 3) 'get tag for searching
        zResult = InStr(DraftData$, tag$)
        If zResult > 0 Then
            ReplaceWith$ = ParLines$(i, 4)
            DraftData$ = ReplaceTag$(DraftData$, ReplaceWith$, zResult, zResult + Len(tag$))
            notCompleted = 1
            Exit For
        End If
        notCompleted = 0
    Next i

    If notCompleted = 1 Then GoTo DoAgain
    Worksheets(shName$).Cells(xRow, xCol) = DraftData$

End Sub

Public Sub InitParLines()
    
   Dim LangMode As Integer, CurrMode As Integer
   Dim i As Integer, SkipTo As Integer
   Dim xCol As Integer, xRow As Integer
   Dim xRange$, errCount As Integer
       
   LangMode = CurrentLang()
   CurrMode = CurrentCurrency()
   
   i = 2
   On Error GoTo ErrHand98
R: While Workbooks(QFCmodName$).Worksheets("con_set").Cells(i, 1) <> "$$end$$"
   On Error GoTo 0
   
       If Trim$(Workbooks(QFCmodName$).Worksheets("con_set").Cells(i, 4 + LangMode)) = "SKIP" Then
           SkipTo = SkipTo - 1
           GoTo wnd
       End If

      With Workbooks(QFCmodName$).Worksheets("con_set")
        ParLines$(i - 1 + SkipTo, 1) = Trim$(.Cells(i, 1).Value) 'MessageEN
        ParLines$(i - 1 + SkipTo, 2) = Trim$(.Cells(i, 2).Value) 'MessageRU
        ParLines$(i - 1 + SkipTo, 3) = Trim$(LCase$(.Cells(i, 3).Value)) 'TAG
        ParLines$(i - 1 + SkipTo, 5) = Trim$(.Cells(i, 4).Value) 'Type
        ParLines$(i - 1 + SkipTo, 8) = Trim$(.Cells(i, 8).Value) 'SKIP MODE
        
        If ParLines$(i - 1 + SkipTo, 3) = "<<no>>" Then
            ParLines$(i - 1 + SkipTo, 4) = TrackGetNo(ConCountFile$) + 1
            GoTo NextCycle
        End If
        
        If ParLines$(i - 1 + SkipTo, 5) = "DYN" Then
            xRange$ = Trim$(.Cells(i, 4 + LangMode))
            xRow = RowAddr(xRange$)
            xCol = ColAddr(xRange$)
            ParLines$(i - 1 + SkipTo, 4) = Worksheets("Form").Cells(xRow, xCol).Value
            GoTo NextCycle
        End If
                
        If ParLines$(i - 1 + SkipTo, 5) = "STAT" Then
            If ParLines$(i - 1 + SkipTo, 4) <> "" Then GoTo NextCycle
            ParLines$(i - 1 + SkipTo, 4) = Trim$(.Cells(i, 4 + LangMode))
            CollectConCase i, SkipTo
            GoTo NextCycle
        End If
     End With
        
        If ParLines$(i - 1 + SkipTo, 5) = "ABS" Then
         With Workbooks(QFCmodName$).Worksheets("con_set").Cells(i, 6)
            If .Value = "Total Amount" Then GoSub GetTotal: GoTo NextCycle
            If .Value = "DigiWords" Then GoSub GetDigiwords: GoTo NextCycle
            If .Value = "Currency Type" Then GoSub GetCurrency: GoTo NextCycle
         End With
        End If
        
NextCycle:

        If Trim$(ParLines$(i - 1 + SkipTo, 4)) = "" Then ParLines$(i - 1, 4) = String$(50, "_")
wnd:
        i = i + 1
    Wend
        
        WizardSteps = i - 2
        TotalParLines = WizardSteps + SkipTo
Exit Sub

'------ GoSubs --------

    Dim SUMxROW As Integer, SUMxCOL As Integer
    Dim xDWRow As Integer, xDWCol As Integer

GetTotal:

    If GetVATmode() = 1 Then
        SUMxROW = FormulaRow(qsVAT$) + 1
    Else
        SUMxROW = FormulaRow(qsGT$)
    End If
    
    SUMxCOL = FormulaCol
    If SUMxROW = 0 Or SUMxCOL = 0 Then ParLines$(i - 1, 4) = "": Return
    ParLines$(i - 1, 4) = Worksheets("Form").Cells(SUMxROW, SUMxCOL).Value
Return

GetDigiwords:
    xDWRow = FormulaRow(qsDIGI$)
    xDWCol = FormulaCol
    If xDWCol = 0 Or xDWRow = 0 Then ParLines$(i - 1, 4) = "": Return
    ParLines$(i - 1, 4) = Trim$(Worksheets("Form").Cells(xDWRow, xDWCol).Value)
Return

'---------------------

GetCurrency:
    ParLines$(i - 1, 4) = CurrWords$(LangMode, CurrMode)
Return

ErrHand98:
  On Error GoTo 0
  errCount = errCount + 1
  If errCount > 3 Then GoTo BadDay
  MsgBox "Bug in MS-Excel just has been discovered," + Chr$(10) + "rolling back... It may help, who knows that Bill!", vbExclamation, "QF:BrainEngine:AutoFix"
  RequestCTemplate
  GoTo R

BadDay:

  MsgBox "Brain Engine couldn't avoid/fix Excel Bug." + Chr$(10) + "Try to install all Adins'es from 'C:\QFORMS\QMods' Manually!" + Chr$(10) + "Operation Cancelled !", "QF:BrainEngine:BadDay"
  End

End Sub

Public Sub RunWizard()

    qReConnect
    If preVerify(True) = False Then Exit Sub
    RequestCTemplate
    InitParLines
    CurrentWizardStep = 1
    WizardUpdate
    Application.ScreenUpdating = True
    Load iWIZARD
    iWIZARD.Show
    
End Sub

Public Sub WizardNext()
    
    Dim NewData$
    ParLines$(CurrentWizardStep, 4) = Trim$(iWIZARD.MyAnswer.Text)
    
   If CurrentWizardStep = WizardSteps Then ' wizard finished
     iWIZARD.Hide
     Unload iWIZARD
     doCon
     With Worksheets("Form")
      NewData$ = GetTagData("<<no>>")
      If NewData$ <> "" Then .Cells(2, 3).Value = NewData$
      NewData$ = GetTagData("<<date>>")
      If NewData$ <> "" Then .Cells(6, 3).Value = NewData$
      NewData$ = GetTagData("<<purchaser>>")
      If NewData$ <> "" Then .Cells(4, 3).Value = NewData$
      .Range("C4:C7").NumberFormat = "@"
      .Range("F7").NumberFormat = "@"
     End With
     Exit Sub
   End If
    
CheckSkip:
    CurrentWizardStep = CurrentWizardStep + 1
    If ParLines$(CurrentWizardStep, 8) = "SKIP" Then GoTo CheckSkip
    WizardUpdate

End Sub

Public Sub WizardPrev()

    If CurrentWizardStep <= 1 Then WizardUpdate: CurrentWizardStep = 1: Exit Sub
    CurrentWizardStep = CurrentWizardStep - 1
    WizardUpdate

End Sub

Public Sub WizardCancel()
    CurrentWizardStep = 1
    iWIZARD.Hide
    Unload iWIZARD
End Sub

Public Sub WizardUpdate()
    
    Dim LangMode As Integer, CurrMode As Integer

    LangMode = CurrentLang()
    CurrMode = CurrentCurrency()
    
  With iWIZARD
    
    .Caption = "Contract Wizard, Mode: " + ModeNames$(CurrentLang, CurrentCurrency)
    If CurrentWizardStep = WizardSteps Then .CB_Next.Caption = " ~Finish~ " Else .CB_Next.Caption = "Next "
    If CurrentWizardStep = 1 Then .CB_Prev.Enabled = False Else .CB_Prev.Enabled = True
    
    .L_EN.Caption = ParLines$(CurrentWizardStep, 1)
    .L_RU.Caption = ParLines$(CurrentWizardStep, 2)
    .MyAnswer.Text = ParLines$(CurrentWizardStep, 4)

    UpdateCases CurrentWizardStep

    InitLPV
    UpdateLPV
    .MyAnswer.SetFocus
  
  End With

End Sub

Public Sub DoStyle(xRow, xCol, shName$)
   
   Dim styleTag$, LangStyleTag$, LangMode As Integer
   LangMode = CurrentLang()
   styleTag$ = Trim$(LCase$(Workbooks(QFCmodName$).Worksheets(ConTemplate$(LangMode)).Cells(xRow, xCol + 1)))
   If styleTag$ = "" Then Exit Sub
   LangStyleTag$ = styleTag$ + Trim$(Str$(LangMode))
   Worksheets(shName$).Range(GetRange$(xRow, xCol)).Style = LangStyleTag$
    
End Sub


Public Sub WizardReset()
   Erase ParLines$
   Erase ConCase$
End Sub


Private Sub CollectConCase(ParStep, SkipTo)

   Const FisrtCaseCol As Integer = 8 '(-1 for add langNo count)
   Dim i As Integer
    
   If ParLines$(ParStep - 1 + SkipTo, 8) = "NOCASE" Then
       ConCase$(ParStep - 1 + SkipTo, 0) = ""
       Exit Sub
   End If

   With Workbooks(QFCmodName$).Worksheets("con_set")
    
   If Trim$(.Cells(ParStep, FisrtCaseCol + 1).Value) = "" Then
       ConCase$(ParStep - 1 + SkipTo, 0) = ""
       Exit Sub
   End If
   
   For i = 1 To 3
        ConCase$(ParStep - 1 + SkipTo, (3 * i + 1) - 3) = Trim$(.Cells(ParStep, (FisrtCaseCol + (3 * i + 1) - 3)).Value)
        ConCase$(ParStep - 1 + SkipTo, (3 * i + 2) - 3) = Trim$(.Cells(ParStep, (FisrtCaseCol + (3 * i + 2) - 3)).Value)
        ConCase$(ParStep - 1 + SkipTo, (3 * i + 3) - 3) = Trim$(.Cells(ParStep, (FisrtCaseCol + (3 * i + 3) - 3)).Value)
        If ConCase$(ParStep - 1 + SkipTo, (3 * i + 1) - 3) <> "" Then ConCase$(ParStep - 1 + SkipTo, 0) = Trim$(Str$(i))
   Next
   
   End With

End Sub

Public Sub UpdateCases(WizStep)

    Dim Opts$
    Opts$ = ConCase$(WizStep, 0)
    
 With iWIZARD
  .cOriginal.Value = True
  .cOriginal.Enabled = False
  .c1.Enabled = False
  .c2.Enabled = False
  .c3.Enabled = False
    
   Select Case Opts$
       Case "1"
         .cOriginal.Enabled = True
         .c1.Enabled = True
      Case "2"
         .cOriginal.Enabled = True
         .c1.Enabled = True
         .c2.Enabled = True
      Case "3"
         .cOriginal.Enabled = True
         .c1.Enabled = True
         .c2.Enabled = True
         .c3.Enabled = True
    End Select
  End With

End Sub

Public Sub CaseChanged()

    Dim LangMode As Integer
    
    LangMode = CurrentLang()
    ConCase$(CurrentWizardStep, 10) = ParLines$(CurrentWizardStep, 4)
       
   With iWIZARD
      If .cOriginal.Value = True Then .MyAnswer.Text = ConCase$(CurrentWizardStep, 10)
      If .c1.Value = True Then .MyAnswer.Text = ConCase$(CurrentWizardStep, LangMode)
      If .c2.Value = True Then .MyAnswer.Text = ConCase$(CurrentWizardStep, 3 + LangMode)
      If .c3.Value = True Then .MyAnswer.Text = ConCase$(CurrentWizardStep, 6 + LangMode)
      .MyAnswer.SetFocus
   End With

End Sub

Public Sub UpdateLPV()
    If iWIZARD.eLPV.Value = False Then Exit Sub
    iWIZARD.LPV.Caption = LPVstart$ + iWIZARD.MyAnswer.Text + LPVend$
End Sub

Public Sub InitLPV()
    
    Dim tag$, cellData$
    Dim CurrentLine As Integer, LangMode As Integer
    Dim TagLen As Integer, TagPos As Integer
    
    If iWIZARD.eLPV.Value = False Then Exit Sub
    
    LPVstart$ = ""
    LPVend$ = ""
    tag$ = ParLines$(CurrentWizardStep, 3)
    CurrentLine = 1
    LangMode = CurrentLang()
    
    While cellData$ <> "$$end$$"
        cellData$ = Workbooks(QFCmodName$).Worksheets(ConTemplate$(LangMode)).Cells(CurrentLine, 2)
        TagLen = Len(tag$)
        TagPos = InStr(cellData$, tag$)
        If TagPos = 0 Then GoTo wnd
        If Len(Trim$(tag$)) = Len(Trim$(cellData$)) Then
            LPVstart$ = ""
            LPVend$ = ""
            Exit Sub
        End If

        LPVstart$ = Left$(cellData$, TagPos - 1)
        LPVend$ = Mid$(cellData$, TagPos + TagLen)
        Exit Sub
        
wnd:    CurrentLine = CurrentLine + 1
    Wend

        LPVstart$ = "[ NOT FOUND ]"
        LPVend$ = "[ TAG: " + tag$ + " ]"

End Sub

Public Sub TunePages()
    
    Dim CoCycles As Integer, i As Integer, pbLoc As Integer
    Dim pbLocRng$, shName$
    Dim RowAbsolute, ColumnAbsolute
    Dim z As Integer
  
  shName$ = RealShName$("Contract")
    
  Worksheets(shName$).Activate
  ActiveWindow.View = xlPageBreakPreview
  CoCycles = 1

ReqRestart:
   
    If CoCycles > 10 Then GoTo GetOut
    For i = 1 To ActiveSheet.HPageBreaks.Count
        pbLocRng$ = ActiveSheet.HPageBreaks(i).Location.Address(RowAbsolute, ColumnAbsolute)
        pbLoc = Val(Mid$(pbLocRng$, 2))
        CoCycles = CoCycles + 1
        For z = 1 To 4
           If ActiveSheet.Cells(pbLoc - z, 2).Font.Size > 10 Then
                Set ActiveSheet.HPageBreaks(i).Location = Range("A" + Trim$(Str$(pbLoc - z)))
                GoTo nx1
            End If
        Next
nx1:
    Next

GetOut:
  ActiveWindow.View = xlNormalView
  
End Sub

Public Sub InsertContract()
   AddDocFromTemplate "Contract", ""
   Columns("A:A").ColumnWidth = 0.33
   Columns("B:B").ColumnWidth = 90
End Sub

Private Function GetTagData$(tagId$)

   Dim i As Integer
   For i = 1 To TotalParLines
       If Trim$(UCase$(ParLines$(i, 3))) = Trim$(UCase$(tagId$)) Then
           GetTagData$ = ParLines$(i, 4)
           Exit Function
       End If
   Next
   GetTagData$ = ""

End Function
