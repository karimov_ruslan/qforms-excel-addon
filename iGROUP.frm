VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} iGROUP 
   Caption         =   "Add Group ..."
   ClientHeight    =   1575
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5220
   OleObjectBlob   =   "iGROUP.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "iGROUP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False









Private Sub CommandButton1_Click()
   
    Dim gRows As Integer
    If sbt.Value = True Then GroupMode = 1 Else GroupMode = 0
    gRows = Val(Trim$(insROWS.Text))
    GroupName$ = insNAME.Text
    AddGroup AddGroupLoc, gRows, GroupName$, GroupMode
    iGROUP.Hide
    Unload iGROUP
    
        
End Sub

Private Sub CommandButton2_Click()
iGROUP.Hide
Unload iGROUP


End Sub

Private Sub UserForm_Activate()

    xMode = GetMode()
    If xMode = 3 Then iGROUP.Hide: Unload iGROUP
    
    If xMode = 2 Then
        sbt.Enabled = True
    End If
        
    If xMode = 1 Then
        sbt.Value = False
        sbt.Enabled = False
    End If
        
End Sub

