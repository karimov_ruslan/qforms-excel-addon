VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} intro 
   Caption         =   "Q!Forms"
   ClientHeight    =   3645
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7050
   OleObjectBlob   =   "intro.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "intro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub CommandButton1_Click()
   Me.Hide
End Sub

Private Sub UserForm_Initialize()

   DoEvents
   qcVer.Caption = QFversion
   qcBuild.Caption = QFbuild & " / " & Val(Application.Version) & "." & Application.Build
   qcCodeName.Caption = QFcodeName
   qcPers.Caption = GetIniPar$("user") & ",  ID: " & GetIniPar$("id", True) & ",  " & WkPlace$(Val(GetIniPar("workplace", True))) & " Dept."
On Error GoTo ErrHand
   qcStructure.Caption = Application.VBE.VBProjects("QFADDINS").VBComponents.Count & " modules and " & InsideCodeLines() & " Lines of s/w code."
cx:
On Error GoTo 0
   DoEvents



Exit Sub

ErrHand:
   qcStructure.Caption = "unkown (due to OfficeXP settings)"
   GoTo cx

End Sub
