Attribute VB_Name = "qMenuWorx"
Option Compare Text
Option Explicit

Private Const qmMode1 = 1
Private Const qmMode2 = 2
Private Const qmMode3 = 4
Private Const mItems As Integer = 100
Private mName$(mItems, 1 To 2) '1=Caption, 2=Action
Private mData(mItems, 3) As Long ' 1=Type, 2=Divider, 3=FaceID
Private mMode(mItems, 10) As Integer
Private mWkPl(mItems) As Integer
Private mCLG$(mItems, 3)
Private mCCR$(mItems, 2)
Private mCVT$(mItems, 2)
Private VisLocked(mItems) As Boolean

'mMode 1= Visibility in OfferModes
'mMode 2= Selected in LangModes
'mMode 3= Selected in CurrModes
'mMode 4= Visible in Protected Mode
'mMode 5= Selected in Offer Mode
'mMode 6= pushed when VAT is ON
'mMode 7= available only in dumb-mode (0-1 always, 2 dumb only)
'mMode 8= Excel version allowed.
'mMode 9= Allowed for Sheets (bit-logic)
'mMode 10= Leave Sub Menu Flag

Private Sub qMenuInit()

Dim cID As Integer, cCurr As Integer, ccTag As Integer

   If SheetBits() <= 0 Then
      cCurr = 1
      ccTag = 1
   Else
      cCurr = CurrentCurrency()
      ccTag = CurrTag()
   End If
   
   qMenuDeInit

cID = cID + 1
mName$(cID, 1) = "New QF Template":   mName$(cID, 2) = "CreateBlankTemplateSilent"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 2520
mMode(cID, 1) = 7
mMode(cID, 4) = True
mMode(cID, 7) = 2
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Verify and Tune-up":   mName$(cID, 2) = "VFY"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 2042
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "New Reference":   mName$(cID, 2) = "ReNewTemplate"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 387
mMode(cID, 1) = 3
mWkPl(cID) = 3

cID = cID + 1
mName$(cID, 1) = "Save":   mName$(cID, 2) = "SaveCurrent"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 3
mMode(cID, 1) = 7
mMode(cID, 4) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Convert Currency":   mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mData(cID, 2) = True
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
If cCurr = 2 Then mName$(cID, 1) = "AZM > USD":    mName$(cID, 2) = "ToggleCURR"
If ccTag = 1 And cCurr = 1 Then mName$(cID, 1) = "USD > AZM":   mName$(cID, 2) = "ToggleCURR"
If ccTag = 2 And cCurr = 1 Then mName$(cID, 1) = "EURO > AZM":   mName$(cID, 2) = "ToggleCURR"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
If cCurr = 2 Then mName$(cID, 1) = "AZM > EURO":   mName$(cID, 2) = "ToggleCURRae"
If ccTag = 1 And cCurr = 1 Then mName$(cID, 1) = "USD > EURO":   mName$(cID, 2) = "ToggleCURRue"
If ccTag = 2 And cCurr = 1 Then mName$(cID, 1) = "EURO > USD":   mName$(cID, 2) = "ToggleCURReu"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 7
mWkPl(cID) = 7
mMode(cID, 10) = True

cID = cID + 1
mName$(cID, 1) = "Add/Remove VAT":   mName$(cID, 2) = "ToggleVAT"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 938
mMode(cID, 1) = 6
mMode(cID, 6) = True
mCVT$(cID, 1) = "Remove VAT"
mCVT$(cID, 0) = "Add VAT"
mCVT$(cID, 2) = "Add VAT"
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Language / Currency":   mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mData(cID, 2) = True
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "English":   mName$(cID, 2) = "SwLangEN"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 7
mMode(cID, 2) = 1
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Russian":   mName$(cID, 2) = "SwLangRU"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 7
mMode(cID, 2) = 2
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Azeri":   mName$(cID, 2) = "SwLangAZ"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 7
mMode(cID, 2) = 3
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "USD":   mName$(cID, 2) = "SwCurrUSD"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mMode(cID, 1) = 7
mMode(cID, 3) = 1
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "AZM":   mName$(cID, 2) = "SwCurrAZM"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 7
mMode(cID, 3) = 2
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "EURO":   mName$(cID, 2) = "SwCurrEURO"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 7
mMode(cID, 3) = 3
mMode(cID, 10) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Offer Modes":   mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 3
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Pre-Offer":   mName$(cID, 2) = "Mode1"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 3
mMode(cID, 5) = 1
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Final Offer":   mName$(cID, 2) = "Mode2"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 3
mMode(cID, 5) = 2
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Contract Form":   mName$(cID, 2) = "DoContractForm"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 2
mMode(cID, 10) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Calculations":   mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mData(cID, 2) = True
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Add % to All":   mName$(cID, 2) = "DoPercentUP"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 462
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Deduct % from All":   mName$(cID, 2) = "DoPercentDOWN"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 464
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Add % of PB to All":   mName$(cID, 2) = "DoPaybackPlus"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mData(cID, 3) = 462
mMode(cID, 1) = 7
mWkPl(cID) = 5

cID = cID + 1
mName$(cID, 1) = "Deduct % of PB from All":   mName$(cID, 2) = "DoPaybackMinus"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 464
mMode(cID, 1) = 7
mWkPl(cID) = 5

cID = cID + 1
mName$(cID, 1) = "Round All to Integer":   mName$(cID, 2) = "DoRoundInt"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mData(cID, 3) = 732
mMode(cID, 1) = 7
mMode(cID, 8) = 9
mWkPl(cID) = 5

cID = cID + 1
mName$(cID, 1) = "Round All to .XX":   mName$(cID, 2) = "DoRound2"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 399
mMode(cID, 1) = 7
mMode(cID, 8) = 9
mMode(cID, 10) = True
mWkPl(cID) = 5

cID = cID + 1
mName$(cID, 1) = "Submit to Customer":   mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 3
mMode(cID, 4) = True
mWkPl(cID) = 3

cID = cID + 1
mName$(cID, 1) = "Send WebOffer by E-mail":   mName$(cID, 2) = "MakeEMLfile"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 3903
mMode(cID, 1) = 3
mMode(cID, 4) = True
mWkPl(cID) = 3

cID = cID + 1
mName$(cID, 1) = "Make WebOffer (.html) file":   mName$(cID, 2) = "MakeHTMLfile"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 3823
mMode(cID, 1) = 3
mMode(cID, 4) = True
mWkPl(cID) = 3

cID = cID + 1
mName$(cID, 1) = "Send E-Mail (.xls) file":   mName$(cID, 2) = "DoEmail"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 2188
mMode(cID, 1) = 3
mMode(cID, 4) = True
mMode(cID, 10) = True
mWkPl(cID) = 3

cID = cID + 1
mName$(cID, 1) = "Groups / Sub Totals":   mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 2
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Insert Sub Totals":   mName$(cID, 2) = "InsertSubTotals"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 2
mMode(cID, 4) = False
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Remove Sub Totals":   mName$(cID, 2) = "RemoveSubTotals"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 2
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Add Group":   mName$(cID, 2) = "DoAddGroup"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mData(cID, 3) = 107
mMode(cID, 1) = 3
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Remove All Groups":   mName$(cID, 2) = "RemoveGroupsInForm"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 1742
mMode(cID, 1) = 3
mMode(cID, 10) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Security":   mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 7
mMode(cID, 4) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Remove Comments":   mName$(cID, 2) = "RemoveComments"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 1592
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Secure / UnSecure":   mName$(cID, 2) = "DoProtection"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 225
mMode(cID, 1) = 7
mMode(cID, 4) = True
mMode(cID, 10) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Customer Forms":   mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Cash Memo":   mName$(cID, 2) = "InsertCashMemo"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 2520
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Delivery Note":   mName$(cID, 2) = "InsertDeliveryNote"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 2520
mMode(cID, 1) = 7
mMode(cID, 10) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Document Services":   mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Compact Descriptions":   mName$(cID, 2) = "CompDesc"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 486
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Expand Descriptions":   mName$(cID, 2) = "UnCompDesc"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 451
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Waste Spaces Off":   mName$(cID, 2) = "WasteSp"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 47
mMode(cID, 1) = 7
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Tune PageBreaks":   mName$(cID, 2) = "DoTunePB"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 364
mMode(cID, 1) = 3
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Callibrate Cells Height":   mName$(cID, 2) = "DoCalbCells"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 541
mMode(cID, 1) = 3
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Sort by Total Price":   mName$(cID, 2) = "DoSortTotal"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mMode(cID, 1) = 4
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Sort by Unit Price":   mName$(cID, 2) = "DoSortUnit"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 4
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Sort by Quantity":   mName$(cID, 2) = "DoSortQty"
mData(cID, 1) = msoControlButton
mMode(cID, 1) = 4
mWkPl(cID) = 7

mName$(cID, 1) = "Force to New Format":   mName$(cID, 2) = "migInit"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mMode(cID, 1) = 7
mMode(cID, 10) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Contracts":   mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 4
mWkPl(cID) = 4

cID = cID + 1
mName$(cID, 1) = "Prepare Everything":   mName$(cID, 2) = "PrepEverything"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 461
mMode(cID, 1) = 4
mWkPl(cID) = 4

cID = cID + 1
mName$(cID, 1) = "Print...":   mName$(cID, 2) = "ShowPrintDialog"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 4
mMode(cID, 1) = 4
mWkPl(cID) = 4

cID = cID + 1
mName$(cID, 1) = "Prepare Contract":   mName$(cID, 2) = "RunWizard"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mData(cID, 3) = 3844
mMode(cID, 1) = 4
mWkPl(cID) = 4

cID = cID + 1
mName$(cID, 1) = "Prepare Attachment":   mName$(cID, 2) = "DoAtt"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 3844
mMode(cID, 1) = 4
mWkPl(cID) = 4

cID = cID + 1
mName$(cID, 1) = "Prepare Invoice":   mName$(cID, 2) = "DoInv"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 3844
mMode(cID, 1) = 4
mMode(cID, 10) = True
mWkPl(cID) = 4


cID = cID + 1
mName$(cID, 1) = "Service Contracts":     mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 4
mWkPl(cID) = 2

cID = cID + 1
mName$(cID, 1) = "Prepare Invoice":   mName$(cID, 2) = "DoInv"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 3844
mMode(cID, 1) = 4
mWkPl(cID) = 2

cID = cID + 1
mName$(cID, 1) = "Prepare Act":   mName$(cID, 2) = "DoAct"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 3844
mMode(cID, 1) = 4
mWkPl(cID) = 2
mMode(cID, 10) = True

cID = cID + 1
mName$(cID, 1) = "Tax Documents":     mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 4
mWkPl(cID) = 6

cID = cID + 1
mName$(cID, 1) = "Make (Re-New) All":   mName$(cID, 2) = "DoRenewAll"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 461
mMode(cID, 1) = 4
mWkPl(cID) = 6

cID = cID + 1
mName$(cID, 1) = "Make (Re-New) TaxSheet":   mName$(cID, 2) = "DoTaxSheet"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mData(cID, 3) = 723
mMode(cID, 1) = 4
mWkPl(cID) = 6

cID = cID + 1
mName$(cID, 1) = "Make (Re-New) TaxInvoice":   mName$(cID, 2) = "DoTaxInvoice"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 723
mMode(cID, 1) = 4
mWkPl(cID) = 6

cID = cID + 1
mName$(cID, 1) = "Cross Check":   mName$(cID, 2) = "TaxCrossCheck"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mData(cID, 3) = 702
mMode(cID, 1) = 4
mMode(cID, 9) = fsTS + fsTI
mMode(cID, 10) = True
mWkPl(cID) = 6

cID = cID + 1
mName$(cID, 1) = "Q!Link (CAS)":     mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 4
mMode(cID, 4) = True
mWkPl(cID) = 6

cID = cID + 1
mName$(cID, 1) = "Recognize TIN":   mName$(cID, 2) = "RetriveTIN"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 3709
mMode(cID, 1) = 4
mWkPl(cID) = 6

cID = cID + 1
mName$(cID, 1) = "Submit to CAS":   mName$(cID, 2) = "SubmitTAXDB"
mData(cID, 1) = msoControlButton
mData(cID, 2) = True
mData(cID, 3) = 3159
mMode(cID, 1) = 4
mMode(cID, 9) = fsInv
mWkPl(cID) = 6

cID = cID + 1
mName$(cID, 1) = "Synchronize with CAS":   mName$(cID, 2) = "SyncTAXDB"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 3727
mMode(cID, 1) = 4
mMode(cID, 9) = fsInv
mWkPl(cID) = 6

cID = cID + 1
mName$(cID, 1) = "Remove from CAS":   mName$(cID, 2) = "RemoveTAXDB"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 1019 '3160
mMode(cID, 1) = 4
mMode(cID, 9) = fsInv
mWkPl(cID) = 6
mMode(cID, 10) = True

cID = cID + 1
mName$(cID, 1) = "Misc.":     mName$(cID, 2) = ""
mData(cID, 1) = msoControlPopup
mMode(cID, 1) = 7
mMode(cID, 7) = 3
mMode(cID, 4) = True
VisLocked(cID) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "About QF":   mName$(cID, 2) = "AboutQF"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 1954
mMode(cID, 1) = 7
mMode(cID, 4) = True
mMode(cID, 7) = 3
VisLocked(cID) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "HotKeys Information":   mName$(cID, 2) = "HotKeysInfo"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 984
mMode(cID, 1) = 7
mMode(cID, 4) = True
mMode(cID, 7) = 3
VisLocked(cID) = True
mWkPl(cID) = 7

cID = cID + 1
mName$(cID, 1) = "Settings...":   mName$(cID, 2) = "editSettings"
mData(cID, 1) = msoControlButton
mData(cID, 3) = 548
mMode(cID, 1) = 7
mMode(cID, 4) = True
mMode(cID, 7) = 3
mMode(cID, 10) = True
VisLocked(cID) = True
mWkPl(cID) = 7

End Sub

Public Sub qMenuInstall()
   
   qMenuRemove
   
   Dim cb
   Dim cbb, MenuID As Integer, i As Integer
   
   Set cb = Application.CommandBars(1).Controls.Add(msoControlPopup, 1)
   cb.Caption = "Q!Forms"
   cb.OnAction = "qMenuReBuild" + Trim$(Str$(Val(GetIniPar$("Workplace"))))

Dim MathMenu$(10, 2)
Dim MathDiv(10) As Boolean
Dim fID(10) As Integer
   
MathMenu$(1, 1) = "Roll Back"
MathMenu$(1, 2) = "RollBack"
MathDiv(1) = False
fID(1) = 128
   
MathMenu$(2, 1) = "% Up (+)"
MathMenu$(2, 2) = "SinglePercentUP"
MathDiv(2) = True

MathMenu$(3, 1) = "% Down (-)"
MathMenu$(3, 2) = "SinglePercentDOWN"
MathDiv(3) = False

MathMenu$(4, 1) = "% PB+"
MathMenu$(4, 2) = "SinglePBPlus"
MathDiv(4) = True

MathMenu$(5, 1) = "% PB-"
MathMenu$(5, 2) = "SinglePBMinus"
MathDiv(5) = False
    
   MenuID = DDmenuID(1)
If MenuID = 0 Then GoTo StepX1

With Application.CommandBars(MenuID).Controls
Set cb = .Add(msoControlPopup, 1)
    cb.Caption = "(QF) Calculator"
    cb.BeginGroup = True
    
    i = 1
While MathMenu$(i, 1) <> ""
   Set cbb = cb.Controls.Add(msoControlButton, 1)
   cbb.Caption = MathMenu$(i, 1)
   cbb.OnAction = MathMenu$(i, 2)
   cbb.BeginGroup = MathDiv(i)
   If fID(i) <> 0 Then cbb.FaceId = fID(i)
    i = i + 1
Wend

Set cb = .Add(msoControlButton, 1)
    cb.Caption = "(QF) Insert New Group"
    cb.OnAction = "DoAddGroup"
    cb.FaceId = 107
Set cb = .Add(msoControlButton, 1)
    cb.Caption = "(QF) Edit Box"
    cb.OnAction = "OpenEditBox"
    cb.FaceId = 1850
End With
   
    If GetIniPar$("optSimp") = "YES" Then SimplifyPopup MenuID

StepX1:
   
On Error Resume Next
Set cb = Application.CommandBars(DDmenuID(2)).Controls.Add(msoControlButton, 1)
    cb.Caption = "(QF) Export Sheet"
    cb.OnAction = "DoExportSheet"
    cb.FaceId = 1591
On Error GoTo 0

   SimplifyExcel

End Sub

Private Sub SimplifyPopup(MenuID As Integer)

   Dim mCB, i As Integer
   Const MaxSimps As Integer = 5
   Dim Simp$(MaxSimps)
   
   Simp$(1) = "Clear Co&ntents"
   Simp$(2) = "Pic&k From List..."
   Simp$(3) = "Pic&k From List"
   Simp$(4) = "&Hyperlink..."
   Simp$(5) = "Add &Watch"
   
   For Each mCB In Application.CommandBars(MenuID).Controls
      For i = 1 To MaxSimps
         If mCB.Caption = Simp$(i) Then mCB.Delete: GoTo n
      Next
n: Next
  
End Sub

Public Sub qMenuRemove()
   
   Dim cb
      
   For Each cb In Application.CommandBars(1).Controls
      If cb.Caption = "Q!Forms" Or cb.Caption = "Quick Forms" Then cb.Delete
   Next
   
On Error Resume Next
   Application.CommandBars(DDmenuID(1)).Reset
   Application.CommandBars(DDmenuID(2)).Reset
On Error GoTo 0
     
   Application.CommandBars(1).Reset
   For Each cb In Application.CommandBars(1).Controls
      cb.Reset
   Next
     
End Sub

Public Sub qMenuReBuild0()
qMenuReBuild 0
End Sub

Public Sub qMenuReBuild1()
qMenuReBuild 1
End Sub

Public Sub qMenuReBuild2()
qMenuReBuild 2
End Sub


Private Sub qMenuReBuild(wPlace As Integer)

   Dim i As Integer, cID As Integer
   Dim cP As CommandBarPopup
   Dim cb As CommandBarButton
   Dim rootMenu, rM, xM
   Dim cMode As Integer, cLang As Integer, cCurr As Integer, cVAT As Integer
   Dim cProt As Boolean, DumbMode As Boolean, mAllowed As Boolean, ccTag As Integer
   Dim mn, wbs, ws, newCapt$
   Dim HasSheets As Integer, tBit As Integer, mHidden As Boolean
   
   Application.StatusBar = ""
   Application.ScreenUpdating = True
   
   qReConnect

   Set rootMenu = Application.CommandBars(1).Controls("Q!Forms")
   Set xM = rootMenu.Controls

   For Each mn In xM
      mn.Delete
   Next

   DumbMode = False
   HasSheets = SheetBits()

   If HasSheets <= 0 Then DumbMode = True: GoTo d1
   If BitLog(HasSheets, fsForm) Then Worksheets("form").Activate
   
   cMode = GetMode()
   cLang = CurrentLang()
   cCurr = CurrentCurrency()
   cVAT = GetVATmode()
   cProt = CheckProtect(True)
   ccTag = CurrTag()
      
d1: qMenuInit
      
   Set rM = rootMenu
     
   cID = 1
   While mName$(cID, 1) <> ""
   
      If mData(cID, 1) = msoControlPopup Then 'add sub menu
         GoSub CheckPerm
         If mAllowed = False Then GoTo w
         Set rM = rootMenu.Controls.Add(msoControlPopup, 1)
         GoSub GetCapt
         rM.Caption = newCapt$
         rM.OnAction = mName$(cID, 2)
         rM.BeginGroup = mData(cID, 2)
         If mHidden = True Then rM.Enabled = False
      End If
     
      If mData(cID, 1) = msoControlButton Then 'add command
         GoSub CheckPerm
         If mAllowed = False Then GoTo w
         Set cb = rM.Controls.Add(msoControlButton, 1)
         GoSub GetCapt
         cb.Caption = newCapt$
         cb.OnAction = mName$(cID, 2)
         cb.BeginGroup = mData(cID, 2)
         If mHidden = True Then cb.Enabled = False
         If DumbMode = True Then GoTo x
         If cVAT = 1 And mMode(cID, 6) = True Then cb.State = msoButtonDown
         If cLang = mMode(cID, 2) Then cb.State = msoButtonMixed: GoTo Ns1
         If cCurr = 1 And mMode(cID, 3) = 3 And ccTag = 2 Then cb.State = msoButtonMixed: GoTo Ns1
         If cCurr = mMode(cID, 3) And ccTag = 1 Then cb.State = msoButtonMixed: GoTo Ns1
         If cMode = mMode(cID, 5) Then cb.State = msoButtonMixed: GoTo Ns1
         On Error Resume Next
x:          cb.FaceId = mData(cID, 3)
         On Error GoTo 0
Ns1:
      End If
w:
      If mMode(cID, 10) = True Then Set rM = rootMenu 'leave sub menu
      cID = cID + 1
   Wend
   
Exit Sub

CheckPerm:
   mAllowed = False
   mHidden = False
   
   If mMode(cID, 7) = 3 Then mAllowed = True: Return
   If DumbMode = True And mMode(cID, 7) >= 1 Then GoTo mYes
   If DumbMode = True And mMode(cID, 7) = 0 Then Return
   If DumbMode = False And mMode(cID, 7) = 2 Then Return
   If mMode(cID, 4) = False And cProt = True Then Return
   If ModBit(cMode, mMode(cID, 1)) = False Then mHidden = True: GoTo mYes
   If mMode(cID, 9) = 0 Then GoTo mYes
   tBit = mMode(cID, 9) And HasSheets
   If tBit <= 0 Then mHidden = True: GoTo mYes
mYes:
   tBit = mWkPl(cID) And (2 ^ wPlace)
   If tBit <= 0 Then mHidden = True: Return
   If SystemLocked = True And VisLocked(cID) = False Then Return
   mAllowed = True
   Return

GetCapt:
   newCapt$ = ""
   If mCCR$(cID, cCurr) <> "" Then newCapt$ = mCCR$(cID, cCurr)
   If mCLG$(cID, cLang) <> "" Then newCapt$ = mCLG$(cID, cLang)
   If mCVT$(cID, cVAT) <> "" Then newCapt$ = mCVT$(cID, cVAT)
   If newCapt$ = "" Then newCapt$ = mName$(cID, 1)
   Return

End Sub

Private Function ModBit(cMode As Integer, avBitMode As Integer) As Boolean

   Dim TmpVal As Integer
   Dim BitVal As Integer
   BitVal = 2 ^ (cMode - 1)
   TmpVal = BitVal And avBitMode
   If TmpVal = BitVal Then ModBit = True Else ModBit = False
      
End Function

Private Function qMenuDeInit()
   Erase mName$
   Erase mCCR$
   Erase mCVT$
   Erase mCLG$
   Erase mData
   Erase mMode
End Function

Private Function DDmenuID(sID As Integer) As Integer

   If sID = 2 Then
      If Val(Application.Version) > 9 Then DDmenuID = 34 Else DDmenuID = 31
      Exit Function
   End If

   If sID = 1 Then DDmenuID = FindCtlID(1)

End Function

Public Function FindCtlID(xType As Integer) As Integer

   Dim cBar, cBut, cBars
   Dim fCount As Integer, i As Integer
   Const MaxTargets = 9
   Const Eno = 3
   Dim Target$(1, MaxTargets)
   
   Target$(1, 1) = "Clear Co&ntents"
   Target$(1, 2) = "Pic&k From List"
   Target$(1, 3) = "Paste &Special..."
   Target$(1, 4) = "&Hyperlink..."
   Target$(1, 5) = "Insert Co&mment"
   Target$(1, 6) = "&Edit Comment"
   Target$(1, 7) = "Add &Watch"
   Target$(1, 8) = "&Format Cells..."
   Target$(1, 9) = "Pic&k From List..."
      
   Set cBars = Application.CommandBars
   
   For Each cBar In cBars
      fCount = 0
      For Each cBut In cBar.Controls
         For i = 1 To MaxTargets
            If Target$(xType, i) = cBut.Caption Then fCount = fCount + 1
         Next
      Next
      If fCount >= Eno Then
         FindCtlID = cBar.Index
         Exit Function
      End If
   Next

End Function


Private Sub SimpToolbar(cLevel As Byte)

   Dim dmName$(20), inPar$
   Dim dmLevel(20) As Byte
   Dim idx As Integer, i As Integer
   Dim mn
   
   Open SysRoot$ + "\set_menu.ini" For Input As 1
      While Not EOF(1)
         Input #1, inPar$
         If Trim$(inPar$) = "" Then GoTo w
         idx = idx + 1
         dmLevel(idx) = Val(ParseDivider$(inPar$, ":", 1))
         dmName$(idx) = Trim$(ParseDivider$(inPar$, ":", 2))
w:    Wend
   Close #1

   For Each mn In Application.CommandBars(1).Controls
      For i = 1 To idx
         If dmLevel(i) > cLevel Then GoTo n1
         If dmName$(i) = mn.Caption Then mn.Delete: GoTo n
n1:   Next
n: Next

End Sub

Private Sub SimpCommands(cLevel As Byte)

   Dim dmName$(200), inPar$
   Dim dmLevel(200) As Byte
   Dim idx As Integer, i As Integer
   Dim mn, mnc
  
   Open SysRoot$ + "\set_cmd.ini" For Input As 1
      While Not EOF(1)
         Input #1, inPar$
         If Trim$(inPar$) = "" Then GoTo w
         idx = idx + 1
         dmLevel(idx) = Val(ParseDivider$(inPar$, ":", 1))
         dmName$(idx) = Trim$(ParseDivider$(inPar$, ":", 2))
w:    Wend
   Close #1

   For Each mn In Application.CommandBars(1).Controls
     For Each mnc In mn.Controls
      For i = 1 To idx
         If dmLevel(i) > cLevel Then GoTo n1
         If dmName$(i) = mnc.Caption Then mnc.Delete: GoTo n
n1:   Next
n:  Next
   Next

End Sub

Private Sub SimplifyExcel()
   
   Dim sLevel As Byte
   
   sLevel = Val(GetIniPar$("SimplifyLevel"))
   If sLevel = 0 Then Exit Sub
   
   SimpToolbar sLevel
   SimpCommands sLevel

End Sub

