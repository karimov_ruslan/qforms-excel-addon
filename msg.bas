Attribute VB_Name = "msg"
Option Explicit
Option Compare Text

Public Function crSign$(curr As Integer, lang As Integer)
   
   Dim tmp As Integer
   Dim crSignInt$(1 To 2, 1 To 3, 1 To 2) ' Curr,Lang, currTag
   
   crSignInt$(1, 1, 1) = "USD"
   crSignInt$(1, 1, 2) = "EURO"
   crSignInt$(2, 1, 1) = "AZM"
   crSignInt$(2, 1, 2) = "AZM"
   crSignInt$(1, 2, 1) = "USD"
   crSignInt$(1, 2, 2) = "EURO"
   crSignInt$(2, 2, 1) = "AZM"
   crSignInt$(2, 2, 2) = "AZM"
   crSignInt$(1, 3, 1) = "AB� Dollari"
   crSignInt$(1, 3, 2) = "EURO"
   crSignInt$(2, 3, 1) = "AZM"
   crSignInt$(2, 3, 2) = "AZM"
   
   If curr = 2 Then tmp = 1 Else tmp = CurrTag()
   crSign$ = crSignInt$(curr, lang, tmp)

End Function

Public Function ModeNames$(curr As Integer, lang As Integer)
   
   Dim tmp As Integer
   Dim ModeNamesInt$(1 To 3, 1 To 2, 1 To 2) ' language , currency, currTag
   
   ModeNamesInt$(1, 1, 1) = "English / USD"
   ModeNamesInt$(1, 1, 2) = "English / EURO"
   ModeNamesInt$(1, 2, 1) = "English / AZM"
   ModeNamesInt$(1, 2, 2) = "English / AZM"
   ModeNamesInt$(2, 1, 1) = "Russian / USD"
   ModeNamesInt$(2, 1, 2) = "Russian / EURO"
   ModeNamesInt$(2, 2, 1) = "Russian / AZM"
   ModeNamesInt$(2, 2, 2) = "Russian / AZM"
   ModeNamesInt$(3, 1, 1) = "Azeri / USD"
   ModeNamesInt$(3, 1, 2) = "Azeri / EURO"
   ModeNamesInt$(3, 2, 1) = "Azeri / AZM"
   ModeNamesInt$(3, 2, 2) = "Azeri / AZM"

   If curr = 2 Then tmp = 1 Else tmp = CurrTag()
   ModeNames$ = ModeNamesInt$(curr, lang, tmp)

End Function

Public Function CurrWords$(curr As Integer, lang As Integer)
   
   Dim tmp As Integer
   Dim CurrWordsInt$(1 To 3, 1 To 2, 1 To 2) ' language , currency, currTag
   
   CurrWordsInt$(1, 1, 1) = "US Dollars"
   CurrWordsInt$(1, 1, 2) = "EURO"
   CurrWordsInt$(1, 2, 1) = "Azerbaijan's Manats"
   CurrWordsInt$(1, 2, 2) = "Azerbaijan's Manats"
   CurrWordsInt$(2, 1, 1) = "�������� ���"
   CurrWordsInt$(2, 1, 2) = "����"
   CurrWordsInt$(2, 2, 1) = "� ������� ��������������� ����������"
   CurrWordsInt$(2, 2, 2) = "� ������� ��������������� ����������"
   CurrWordsInt$(3, 1, 1) = "AB� Dollari"
   CurrWordsInt$(3, 1, 2) = "EURO"
   CurrWordsInt$(3, 2, 1) = "���������� ��������������� ���������"
   CurrWordsInt$(3, 2, 2) = "���������� ��������������� ���������"

   If curr = 2 Then tmp = 1 Else tmp = CurrTag()
   CurrWords$ = CurrWordsInt$(curr, lang, tmp)

End Function

Public Function BaiCO$(lang As Integer, acType As Integer)

   Dim BaiCOint$(1 To 3, 1 To 2, 0 To 2)
      
   'USD Account
   BaiCOint$(1, 1, 2) = "R.I.S.K. Company" + Chr$(10) + "USD Account 3813.840.00.00596," + Chr$(10) + "Corresponding A/C No. 204818 USD 3302-01" + Chr$(10) + "in International Bank of NBA."
   BaiCOint$(2, 1, 2) = "R.I.S.K. Company" + Chr$(10) + "USD Account 3813.840.00.00596," + Chr$(10) + "Corresponding A/C No. 204818 USD 3302-01" + Chr$(10) + "in International Bank of NBA."
   BaiCOint$(3, 1, 2) = "R.I.S.K. Company" + Chr$(10) + "USD Account 3813.840.00.00596," + Chr$(10) + "Corresponding A/C No. 204818 USD 3302-01" + Chr$(10) + "in International Bank of NBA."
   'AZM Account
   BaiCOint$(1, 2, 2) = "R.I.S.K. Company, TIN 150009631" + Chr$(10) + "JSC Bank 'RESPUBLIKA', TIN 150003500," + Chr$(10) + "AZM Account: 3803.031.00.00596" + Chr$(10) + "Corresponding No.: 0137010014031, Code: 505668" + Chr$(10) + "SWIFT: BRES AZ 22"
   BaiCOint$(2, 2, 2) = "��� 'R.I.S.K.' ��� 150009631" + Chr$(10) + "���� N: 3803.031.00.00596 � ��� '����������'" + Chr$(10) + "��� ��� ������������� �����, ��� 150003500," + Chr$(10) + "�/c: 0137010014031, ��� 505668" + "  SWIFT:  BRES AZ 22"
   BaiCOint$(3, 2, 2) = "'R.I.S.K.' E�F, ���� 150009631" + Chr$(10) + "��� N: 3803.031.00.00596 S�� �RESPUBLIKA�" + Chr$(10) + "����� ������ �������� ���� 150003500" + Chr$(10) + "�/� 0137010014031, ��� 505668, SWIFT:  BRES AZ 22"

   'USD Account
   BaiCOint$(1, 1, 1) = "account not set"
   BaiCOint$(2, 1, 1) = "account not set"
   BaiCOint$(3, 1, 1) = "account not set"
   'AZM Account
   BaiCOint$(1, 2, 1) = "account not set"
   BaiCOint$(2, 2, 1) = "account not set"
   BaiCOint$(3, 2, 1) = "'R.I.S.K.' E�F, ���� 150009631" + Chr$(10) + "��� N: CCS 330571AZM 380301, �BB" + Chr$(10) + "����� ������ �������� ���� 990000213" + Chr$(10) + "�/� 0137010002031, ��� 805250, SWIFT: IBAZ AZ 2X"

   BaiCO$ = BaiCOint$(lang, acType, Val(GetIniPar$("Workplace")))

End Function


