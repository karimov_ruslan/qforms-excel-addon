Attribute VB_Name = "qDocModes"
Option Compare Text
Option Explicit

Public Sub PropsUpdate(Optional xMode As Integer, Optional xLang As Integer, Optional xCurr As Integer, Optional xVat As Integer)
On Error Resume Next
a: If xMode <> 0 Then ActiveWorkbook.CustomDocumentProperties("qMode").Value = xMode
   If xLang <> 0 Then ActiveWorkbook.CustomDocumentProperties("qLang").Value = xLang
   If xCurr <> 0 Then ActiveWorkbook.CustomDocumentProperties("qCurr").Value = xCurr
   If xVat <> 0 Then ActiveWorkbook.CustomDocumentProperties("qVAT").Value = xVat
On Error GoTo 0
End Sub

Public Function GetMode() As Integer
   GetMode = ActiveWorkbook.CustomDocumentProperties("qMode").Value
End Function

Public Function CurrentLang() As Integer
   CurrentLang = ActiveWorkbook.CustomDocumentProperties("qLang").Value
End Function

Public Function CurrentCurrency() As Integer
   CurrentCurrency = ActiveWorkbook.CustomDocumentProperties("qCurr").Value
End Function

Public Function CurrTag() As Integer
   Dim tmp As Integer
  On Error Resume Next
   tmp = ActiveWorkbook.CustomDocumentProperties("qccTag").Value
  On Error GoTo 0
   
   If tmp = 0 Then
      CurrTag = 1
      On Error Resume Next
      With ActiveWorkbook.CustomDocumentProperties
         .Add Name:="qccTag", _
            LinkToContent:=False, _
            Type:=msoPropertyTypeNumber, _
            Value:=1
      End With
      On Error GoTo 0
   Else
      CurrTag = tmp
   End If
  
End Function

Public Function CurrTagName$(Optional tagId As Integer)

   Dim TagName$(2)
   TagName$(1) = "USD"
   TagName$(2) = "EURO"
   If tagId = 0 Then
      CurrTagName$ = TagName$(CurrTag())
   Else
      CurrTagName$ = TagName$(tagId)
   End If

End Function

Public Function RevCurrTagName$()
   Dim TagName$(2)
   TagName$(1) = "USD"
   TagName$(2) = "EURO"
   If CurrTag() = 1 Then RevCurrTagName$ = TagName$(2)
   If CurrTag() = 2 Then RevCurrTagName$ = TagName$(1)
End Function

Public Function GetVATmode() As Integer
   GetVATmode = ActiveWorkbook.CustomDocumentProperties("qVAT").Value
End Function


Public Function GetVATsubMode() As Integer

'=0 nothing
'=1 VAT 18%
'=2 VAT =0
   
   Dim fRow As Integer, msgText$
   
   GetVATsubMode = 0
   If GetVATmode() <> 1 Then Exit Function
   fRow = FormulaRow("=AVERAGE(F", "Form", qsAREA$) - 1
   If fRow <= 0 Then
      msgText$ = "BrainEngine has detected Template Malfunctions." + Chr$(10)
      msgText$ = msgText$ + "Conflict removal process will be activated..." + Chr$(10) + Chr$(10)
      msgText$ = msgText$ + "PROCESS TERMINATED - PLEASE CHECK TEMPLATE [VAT] AGAIN !"
      MsgBox msgText$, vbCritical, "(QF) Template Malfunctions Detected."
      RemoveVAT True
      End
   End If
      
   If Worksheets("form").Cells(fRow, 6).Formula = 0 Then
      GetVATsubMode = 2
   Else
      GetVATsubMode = 1
   End If
   
End Function


Private Sub ChangeMode(xMode As Integer)

   Dim PrevMode As Integer
   If CheckProtect(False) = True Then Exit Sub
       
   NoScreen
   RemoveSubTotals
   PrevMode = GetMode()
   PropsUpdate xMode

With Worksheets("form")
   If xMode = 1 Then
      .Columns("G:G").EntireColumn.Hidden = False
      .Columns("F:F").EntireColumn.Hidden = True
      .Columns("D:D").EntireColumn.Hidden = True
      .Columns("E:E").ColumnWidth = 19.86
      .Columns("G:G").ColumnWidth = 16.01
      Exit Sub
   End If
   If xMode = 2 Then
      .Columns("G:G").EntireColumn.Hidden = False
      .Columns("F:F").EntireColumn.Hidden = False
      .Columns("D:D").EntireColumn.Hidden = False
      .Columns("G:G").ColumnWidth = 5.86
      .Columns("E:E").ColumnWidth = 11
      .Columns("F:F").ColumnWidth = 13.14
      If PrevMode = 1 Then
          SwLang CurrentLang(), True
          SwCurrency CurrentCurrency(), True
      End If
      Exit Sub
   End If
   If xMode = 3 Then
      .Columns("G:G").EntireColumn.Hidden = True
      .Columns("F:F").EntireColumn.Hidden = False
      .Columns("D:D").EntireColumn.Hidden = False
      .Columns("E:E").ColumnWidth = 13.14
      .Columns("F:F").ColumnWidth = 17.43
       Exit Sub
    End If
End With

End Sub

Public Sub Mode1()
    ChangeMode 1
End Sub

Public Sub Mode2()
   ChangeMode 2
End Sub

Public Sub Mode3()
   ChangeMode 3
End Sub

Public Sub SwLang(xMode As Integer, Optional ForceFlag As Boolean)

   If IsMissing(ForceFlag) Then ForceFlag = False
   If CurrentLang() = xMode And ForceFlag = False Then Exit Sub
   Dim cCurr As Integer
   cCurr = CurrentCurrency()

   SaveLocation True
    WizardReset
    PropsUpdate , xMode
    ChTable xMode, cCurr
    ChLayout xMode
    ChTotal xMode, cCurr
   RestLocation True

End Sub

Public Sub SwCurrency(xMode As Integer, Optional ForceFlag As Boolean)

    If AllowCurrencyChange() = False Then Exit Sub
    If CurrentCurrency() = xMode And ForceFlag = False Then Exit Sub

   SaveLocation True
    PropsUpdate , , xMode
    WizardReset
    MaintDigits
    ChTotal CurrentLang(), xMode
    ChTable CurrentLang(), xMode
   RestLocation True

End Sub

Public Sub ToggleVAT()
    Dim z As Boolean
    If CheckProtect(False) = True Then Exit Sub
    If AllowCurrencyChange() = False Then Exit Sub
    SaveLocation True
    If GetVATmode() = 1 Then RemoveVAT Else AddVAT
    z = preVerify()
    RestLocation True
End Sub

Public Sub SwLangEN()
   SwLang 1, True
End Sub

Public Sub SwLangRU()
   SwLang 2, True
End Sub

Public Sub SwLangAZ()
   SwLang 3, True
End Sub

Public Sub SwCurrUSD()
   ActiveWorkbook.CustomDocumentProperties("qccTag").Value = 1
   SwCurrency 1, True
End Sub

Public Sub SwCurrEURO()
   ActiveWorkbook.CustomDocumentProperties("qccTag").Value = 2
   SwCurrency 1, True
End Sub

Public Sub SwCurrAZM()
   ActiveWorkbook.CustomDocumentProperties("qccTag").Value = 1
   SwCurrency 2, True
End Sub

Public Sub DoContractForm()

   Dim msgText$, x As Integer, xFN$
    
   If CheckProtect(False) = True Then Exit Sub
   SaveLocation True
   If qVerify(, , True) <> 0 Then MsgBox "Document Contain Errors and Can NOT be Converted !!!!" + Chr$(10) + Chr$(10), 16, "Action Canceled !": Exit Sub
   If Val(GetIniPar$("Workplace")) = 2 Then GoTo GoWithoutSave
   
   msgText$ = "You're about to Create Contract Form Now." + Chr$(10)
   msgText$ = msgText$ + "Note! This Action can not be undone !!!" + Chr$(10) + Chr$(10)
   msgText$ = msgText$ + "Would you like to save original document first ?"
    
   x = MsgBox(msgText$, vbYesNoCancel + vbQuestion + vbDefaultButton1, "Save Original?")
   
   If x = 2 Then Exit Sub
   If x = 7 Then GoTo GoWithoutSave
        
   Dim dlgAnswer As Boolean
   xFN$ = WhatFile(0)
   dlgAnswer = Application.Dialogs(xlDialogSaveAs).Show(xFN$)
   AfterSave dlgAnswer
   
GoWithoutSave:

   RemoveGroups "Form", True
   Mode3
   ReconstractContractForm
   RestLocation True
    
End Sub

Public Sub ReconstractContractForm()
   DecoCell 2, 2, "CTR #", False, True, xlRight, , True, 11, True, , 8, False
   DecoCell 3, 2, "VOIN #", False, True, xlRight, , True, 3, True, , 8, False
   DecoCell 2, 3, "0", True, False, xlLeft, False, True, 11, True, , 10
   DecoCell 3, 3, "0", True, False, xlLeft, False, True, 3, True, , 10
   DecoCell 1, 3, "CONTRACT FORM", True, False, xlCenter, False, True, 46, True, , 14
   uLine "A3:C3", 1
End Sub

Private Sub ChTotal(LangMode As Integer, CurrMode As Integer)

   Dim GTrow As Integer, GTcol As Integer

    If GetMode() <> 1 Then
       InitVariables
       TranslateSubTotals LangMode
        GTrow = FormulaRow(qsGT$)
        GTcol = FormulaCol
        If GTrow = 0 Then
            MakeVerification
            GTrow = FormulaRow(qsGT$)
        End If
        If GTrow = 0 Then Exit Sub
        FixDigiwords
        Cells(GTrow, 5).Value = ""
        DecoCell GTrow, 4, tot$(LangMode) + crSign$(CurrMode, LangMode) + "):", True, , xlRight
        If GetVATmode() = 1 Then AddVAT
    End If

End Sub

Private Sub ChTable(LangMode As Integer, CurrMode As Integer)
   
   Dim xFsz As Integer

    InitVariables
    If LangMode = 2 Then xFsz = 9 Else xFsz = 10
    With Worksheets("Form")
        .Range("C9:G9").Font.Name = xFNT$(LangMode)
        .Cells(9, 3) = aHEAD$(1, LangMode)
        .Cells(9, 3).Font.Size = xFsz
        .Cells(9, 4) = aHEAD$(2, LangMode)
        .Cells(9, 4).Font.Size = xFsz - 2
        .Cells(9, 5) = aHEAD$(3, LangMode) + Chr$(10) + crSign$(CurrMode, LangMode)
        .Cells(9, 5).Font.Size = xFsz - 2
        .Cells(9, 6) = aHEAD$(4, LangMode) + Chr$(10) + crSign$(CurrMode, LangMode)
        .Cells(9, 6).Font.Size = xFsz - 2
        .Cells(9, 7) = aHEAD$(5, LangMode)
        .Cells(9, 7).Font.Size = xFsz - 3
        .Range("B4:C6").Font.Name = xFNT$(LangMode)
        .Range("B4:C6").Font.Size = xFsz
        .Cells(4, 2) = wHEAD$(1, LangMode)
        .Cells(4, 2).Font.Size = xFsz - 2
        .Cells(5, 2) = wHEAD$(2, LangMode)
        .Cells(5, 2).Font.Size = xFsz - 2
        .Cells(6, 2) = wHEAD$(3, LangMode)
        .Cells(6, 2).Font.Size = xFsz - 2
    End With
    
    PutDate 6, 3, 0

End Sub

Public Sub MakeVerification(Optional DontSaveLoc As Boolean)

   Dim msgText$, x As Integer

   If IsRowsHidden() = True Then
      msgText$ = "Warning! Some of the rows are Hidden!" + Chr$(10)
      msgText$ = msgText$ + "It may cause your and verification Inattentions !" + Chr$(10) + Chr$(10)
      msgText$ = msgText$ + "Would you like to UnHide those rows ?" + Chr$(10)
      x = MsgBox(msgText$, vbYesNo + vbExclamation, "BrainEngine(R): Warning !")
      If x = 6 Then UnHideRows
   End If
 
VRestart:
    x = qVerify(, , DontSaveLoc)
    If x >= 5000 Then GoTo VRestart

End Sub

Public Sub AddGroup(xRow As Integer, xLines As Integer, GroupName$, GroupMode) ' GroupMode =0 plain, =1 with subtotal

   Dim xRange$, deadRange$, xFormula$
   Dim i As Integer
   Dim xDefDelivery   As Integer, xDefQty As Integer
   
   If xLines < 3 Then Exit Sub
   
   SaveLocation True
   
   xLines = xLines - 1
   xRange$ = RowsAddr$(xRow, xRow + xLines + 2)
   Rows(xRange$).Select
   Selection.Insert Shift:=xlDown
   xRange$ = "C" + Trim$(Str$(xRow + 1)) + ":G" + Trim$(Str$(xRow + 1))
   deadRange$ = "B" + Trim$(Str$(xRow)) + ":G" + Trim$(Str$(xRow + 1))
    
   DecoInter deadRange$, xlAutomatic, xlAutomatic, False
   DecoInter xRange$, 36, , True
   Range(xRange$).WrapText = False
   
   DecoCell xRow + 1, 2, "'>", True, , xlCenter, , True, 3
   Cells(xRow + 1, 3) = "  " + GroupName$
   OutlineCells CellAddr$(xRow + 2, 2, 5, xLines), 1
   DecoInter "B" + Trim$(Str$(xRow + 2)) + ":G" + Trim$(Str$(xRow + 2 + xLines)), xlAutomatic, xlAutomatic, False

   For i = xRow + 2 To xRow + 2 + xLines
      DecoCell i, 4, "1", , , xlCenter, , True, , True
      DecoCell i, 6, "=D" + Trim$(Str$(i)) + "*E" + Trim$(Str$(i)), , , xlRight, True, True
      DecoCell i, 7, "0", , , xlCenter, , True
   Next
    
   If GroupMode = 1 Then InsertSTrow xRow + 2, xLines, 0
   DoRenumber
   RestLocation True
      
End Sub

Public Sub DoRenumber()
   
    Dim i As Integer, SUMxROW As Integer, xNUM As Integer
    Dim xRNG$, SaveRange$, ignAFit As Boolean
    Dim usePO As Boolean
    
    SUMxROW = FormulaRow(qsGT$)
    If SUMxROW = 0 Then SUMxROW = 100
    xNUM = 1
    
    If GetIniPar("AutoFit") <> "YES" Then ignAFit = True
    If GetMode() = 0 Then usePO = True Else usePO = False
    
    For i = FirstDataRow To SUMxROW - 1
        If qRowType(i, , usePO, True) = 1 Then
            Cells(i, 2).Value = xNUM
            xNUM = xNUM + 1
            If ignAFit <> True Then Rows(RowsAddr$(i, i)).EntireRow.AutoFit
            xRNG$ = "B" + Trim$(Str$(i)) + ":G" + Trim$(Str$(i))
            With Range(xRNG$)
                .VerticalAlignment = xlTop
                .WrapText = True
            End With
            Cells(i, 10).Formula = "=ROUND(F" + Trim$(Str$(i)) + "/100*18,0)"
        End If
    Next

End Sub

Public Sub DoAddGroup()
   
    If UCase$(ActiveSheet.Name) <> "FORM" Then Exit Sub
    If CheckProtect(False) = True Then Exit Sub

    Dim xLoc As Integer, SUMxROW As Integer
    Dim gRows As Integer, x As Integer
    Dim RowAbsolute, ColumnAbsolute
        
    xLoc = Val(Mid$(ActiveCell.Address(RowAbsolute, ColumnAbsolute), 2))
    SUMxROW = FormulaRow(qsGT$)
    If qRowType(xLoc) <> 0 Or xLoc <= 10 Then Exit Sub
    AddGroupLoc = xLoc
    Load iGROUP
    iGROUP.Show

End Sub

Public Sub RemoveGroupsInForm()
   If MsgBox("All Groups will be Removed, Are You Sure ?", vbQuestion + vbYesNo, "(QF) Remove Groups") = 6 Then
      RemoveSubTotals
      RemoveGroups "Form"
   End If
End Sub



Public Sub RemoveGroups(sheetName$, Optional DontSaveLoc As Boolean)
  
   Dim i As Integer, xFlag As Integer, SUMxROW As Integer, xRange$
   Dim rType As Integer, LeaveLast As Integer, msgText$
   
   If DontSaveLoc = False Then SaveLocation True
   ActiveWorkbook.Worksheets(sheetName$).Activate
   qVerify , True, True

   If Val(GetIniPar$("Workplace")) = 1 Then LeaveLast = True: GoTo DoRestart
   If TotalGroups >= 2 Then
      msgText$ = "Would you like to Leave Last Group ?" + Chr$(10) + "(If so, last group will be assumed as SERVICES)"
      If MsgBox(msgText$, vbYesNo + vbQuestion + vbDefaultButton2, "(QF) Last Group...") = vbYes Then LeaveLast = True
   End If

DoRestart:
   xFlag = 0
   SUMxROW = FormulaRow(qsGT$)
   If SUMxROW = 0 Then GoTo ex

   For i = FirstDataRow To SUMxROW - 2
      rType = qRowType(i)
      If rType <> 1 Then
         If rType = 2 And LeaveLast = True Then
            If TotalGroups = 1 Then
               MakeSimpleTR i
               i = i + 1
               GoTo e1
            End If
            TotalGroups = TotalGroups - 1
         End If
         xRange$ = RowsAddr$(i, i)
         Rows(xRange$).EntireRow.Select
         Selection.Delete Shift:=xlUp
         xFlag = 1
         Exit For
      End If
   Next
e1:
   If xFlag = 1 Then GoTo DoRestart
ex:
   If DontSaveLoc = False Then RestLocation True
   
End Sub

Public Sub AddVAT(Optional fromOnly As Boolean)
    
    Dim OverOld As Boolean
    Dim LangMode As Integer, CurrMode As Integer
    Dim subMode As Integer, GTrow As Integer
    Dim fsz As Integer, xRound$
    
    OverOld = False
    Worksheets("form").Activate
    
    subMode = GetVATsubMode()
    If subMode <> 0 Then OverOld = True
    GTrow = FormulaRow(qsGT$)
    If GTrow = 0 Then Exit Sub

    InitVariables
    NoScreen
    CurrMode = CurrentCurrency()
    LangMode = CurrentLang()
    
    If OverOld = False Then
        Rows(RowsAddr$(GTrow + 1, GTrow + 2)).Select
        Selection.Insert Shift:=xlDown
        Range("a1").Select
    End If
    
   PropsUpdate 0, 0, 0, 1
   Cells(GTrow, 4).HorizontalAlignment = xlRight
    
   DecoCell GTrow + 1, 4, InvSuffix$(LangMode, 1) + " (" + crSign$(CurrMode, LangMode) + "):", True, , xlRight
   If CurrMode = 1 Then xRound$ = "2" Else xRound$ = "0"
   If subMode <> 2 Then DecoCell GTrow + 1, 6, "=ROUND(F" + Trim$(Str$(GTrow)) + "/100*18" + "," + xRound$ + ")", True, , xlLeft, True, True
   DecoCell GTrow + 2, 4, InvSuffix$(LangMode, 2) + " (" + crSign$(CurrMode, LangMode) + "):", True, , xlRight
   DecoCell GTrow + 2, 6, "=AVERAGE(F" + Trim$(Str$(GTrow)) + "+F" + Trim$(Str$(GTrow + 1)) + ")", True, , xlLeft, True, True
   uLine CellAddr$(GTrow + 2, 3, 3), 3

   FixDigiwords

End Sub

Public Sub RemoveVAT(Optional ForceFlag As Boolean)
   
   Dim VATrow As Integer
   If GetVATmode() <> 1 And ForceFlag = False Then Exit Sub
   
   PropsUpdate 0, 0, 0, 2
   VATrow = FormulaRow(qsVAT$)
   If VATrow = 0 Then VATrow = FormulaRow(qsSUMVAT$) - 1
   If VATrow < 0 Then Exit Sub

   Worksheets("form").Activate
   NoScreen
   Rows(RowsAddr$(VATrow, VATrow + 1)).Select
   Selection.Delete Shift:=xlUp
   FixDigiwords

End Sub

Private Sub ChLayout(LangMode As Integer)

   Dim xFsz As Integer, TextLen As Integer

    InitVariables
    UpdateDocHead LangMode
    If LangMode = 2 Then xFsz = 8 Else xFsz = 9

On Error GoTo DoOut
    Worksheets("form").Shapes("Text Box 66").Select
    TextLen = Len(Selection.Characters.Text)
    With Selection.Characters(Start:=1, Length:=TextLen).Font
        .Name = xFNT$(LangMode)
        .FontStyle = "Italic"
        .Size = xFsz
    End With

DoOut:
On Error GoTo 0
End Sub

Public Function SheetBits(Optional wbName$) As Integer

   Const MaxBits As Integer = 8
   Dim MySh$(MaxBits), i As Integer, tmp As Integer
   Dim sh, shName$
   
   MySh$(1) = "Form"         '1
   MySh$(2) = "Attachment"   '2
   MySh$(3) = "Invoice"      '4
   MySh$(4) = "TaxInv"       '8
   MySh$(5) = "TaxSheet"     '16
   MySh$(6) = "CashMemo"     '32
   MySh$(7) = "DeliveryNote" '64
   MySh$(8) = "Contract"     '128
     
   If Workbooks.Count = 0 Then SheetBits = 0: Exit Function
On Error GoTo ex
   If wbName$ = "" Then wbName$ = ActiveWorkbook.Name
   For Each sh In Workbooks(wbName$).Sheets
      shName$ = sh.Name
      For i = 1 To MaxBits
         If InStr(1, shName$, MySh$(i)) > 0 Then tmp = tmp + 2 ^ (i - 1): Exit For
      Next
   Next
   SheetBits = tmp
On Error GoTo 0
Exit Function

ex:
   On Error GoTo 0
   SheetBits = -1024

End Function

