VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} iBOX 
   Caption         =   "(QF) Text Edit Box..."
   ClientHeight    =   2925
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8010
   OleObjectBlob   =   "iBOX.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "iBOX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False








Private Sub CommandButton1_Click()

   NoMore13$ = Remove13$(iBOX.editBOX.Text)
   PutDataAccordingFormat EditBoxLocX, EditBoxLocY, NoMore13$, iBOX.emphFL.Value, iBOX.aWrap.Value, iBOX.eBold.Value, True
   CommandButton2_Click
    
End Sub

Private Sub CommandButton2_Click()

    OutRange$ = Chr$(64 + EditBoxLocY) + Trim$(Str$(EditBoxLocX))
    Range(OutRange$).Select
       
    iBOX.Hide
    Unload iBOX

End Sub

Private Sub UserForm_Activate()
    
    editBOX.WordWrap = WrapInside.Value
    editCELL.Caption = Chr$(64 + EditBoxLocY) + Trim$(Str$(EditBoxLocX))
    editBOX.Text = ActiveSheet.Cells(EditBoxLocX, EditBoxLocY).Value
   
End Sub

Private Sub WrapInside_Change()
    editBOX.WordWrap = WrapInside.Value
End Sub


