Attribute VB_Name = "InvAtt"
Option Explicit

Public Sub InsertInvoice()
   AddDocFromTemplate "Invoice", "Inv"
End Sub

Public Sub InsertAttach()
    AddDocFromTemplate "Attachment", "Att"
End Sub

Public Sub LinkInvoice()
    
   Dim LangMode As Integer, CurrMode As Integer, AddrMode As Integer, dtLEN As Integer
   Dim rBegin, rEnd, fFlag, fsz, xFsz, xFsz2, Ln
   Dim SUMxCOL, SUMxROW, xDP, Answ As Integer
   Dim totFORMULA$, msgText$, answTxt$, invName$
   Dim useConv As Boolean, newNo As Integer, wpIdx As Integer
    
   LangMode = CurrentLang()
   CurrMode = CurrentCurrency()
   invName$ = RealShName$("Invoice")
   wpIdx = Val(GetIniPar$("Workplace"))
    
   Worksheets("form").Activate
       rBegin = FirstDataRow
       rEnd = GetLastRow()
   Worksheets(invName$).Activate
    
  If CurrMode = 1 Then
     msgText$ = "Dear " + GetIniPar$("user") + "," + Chr$(10) + Chr$(10)
     msgText$ = msgText$ + "Your offer is made with USD Currency." + Chr$(10) + Chr$(10)
     msgText$ = msgText$ + "Would you like to use AZM Account ?" + Chr$(10)
     msgText$ = msgText$ + "If No, then USD Account will be used."
     Answ = MsgBox(msgText$, vbQuestion + vbYesNo, "Convert to AZM for Invoice ?")
  End If
    
  If Answ = 6 Then AddrMode = 2: useConv = True Else AddrMode = 1
  If Answ = 0 Then AddrMode = CurrMode: useConv = False
    
  LinkSourceDest rBegin - 1, 15, rEnd - rBegin, invName$, 0, AddrMode, useConv
    
  Worksheets(invName$).Activate
  Cells(6, 3).NumberFormat = "General"
  Cells(6, 3).Formula = "=Form!C4"
    
  DecoCell 25 + rEnd - rBegin, 2, iPOS$(1, LangMode, wpIdx), , True, xlLeft
  DecoCell 25 + rEnd - rBegin, 4, iSIGN$(1, LangMode, wpIdx), , True, xlRight
  DecoCell 29 + rEnd - rBegin, 2, iPOS$(2, LangMode, wpIdx), , True, xlLeft
  DecoCell 29 + rEnd - rBegin, 4, iSIGN$(2, LangMode, wpIdx), , True, xlRight
  uLine CellAddr$(25 + rEnd - rBegin, 2, 2), 1
  uLine CellAddr$(29 + rEnd - rBegin, 2, 2), 1

  If LangMode = 2 Then xFsz = 9 Else xFsz = 10
  If LangMode = 2 Then xFsz2 = 8 Else xFsz2 = 10
     
  Range("B14:F14").Font.Name = xFNT$(LangMode)
  Range("A6:B12").Font.Name = xFNT$(LangMode)
  Range("A1:C1").Font.Name = xFNT$(LangMode)
  Cells(14, 3) = aHEAD$(1, LangMode)
  Cells(14, 3).Font.Size = xFsz
  Cells(14, 4) = aHEAD$(2, LangMode)
  Cells(14, 4).Font.Size = xFsz2 - 1
  Cells(14, 5) = aHEAD$(3, LangMode) + Chr$(10) + crSign$(CurrMode, LangMode)
  Cells(14, 5).Font.Size = xFsz - 1
  Cells(14, 6) = aHEAD$(4, LangMode) + Chr$(10) + crSign$(CurrMode, LangMode)
  Cells(14, 6).Font.Size = xFsz - 1
  Cells(6, 2) = wiHEAD$(1, LangMode)
  Cells(6, 2).Font.Size = xFsz
  Cells(8, 2) = wiHEAD$(2, LangMode)
  Cells(8, 2).Font.Size = xFsz
  Cells(10, 2) = wiHEAD$(3, LangMode)
  Cells(10, 2).Font.Size = xFsz
  Cells(12, 2) = wiHEAD$(4, LangMode)
  Cells(12, 2).Font.Size = xFsz
  Cells(1, 3) = aiCO$(LangMode)
  Cells(3, 5) = wHEAD$(3, LangMode)
  Cells(3, 5).Font.Name = xFNT$(LangMode)
  Cells(3, 6).NumberFormat = "General"
  Cells(3, 6).Value = "'" + CurrentDateW$(LangMode, 2)
  Cells(3, 6).Font.Name = xFNT$(LangMode)
    
'AddrMode REQUEST ---------------------
    
  Worksheets(invName$).Shapes("Text Box 1").Select
  Selection.Characters.Text = BaiCO$(LangMode, AddrMode)
  Ln = InStr(BaiCO$(LangMode, AddrMode), Chr$(10))
  dtLEN = Len(BaiCO$(LangMode, AddrMode)) - Ln + 1
       
    With Selection.Characters(Start:=1, Length:=Ln).Font
        .Name = xFNT$(LangMode)
        .FontStyle = "Bold"
        .Size = 14
    End With
        
    With Selection.Characters(Start:=Ln + 1, Length:=dtLEN).Font
        .Name = xFNT$(LangMode)
        .FontStyle = "Regular"
        .Size = xFsz
    End With

    newNo = TrackGetNo(InvCountFile$) + 1
    Cells(1, 5).Value = newNo
    ActiveSheet.Name = "Invoice-" & newNo
    Range("A1").Select

End Sub

Public Sub LinkAttachment()

    Dim LangMode As Integer, CurrMode As Integer
    Dim rBegin, rEnd, fFlag, fsz, xFsz, xFsz2
    Dim SUMxCOL As Integer, SUMxROW As Integer, xDP As Integer
    Dim totFORMULA$, wpIdx As Integer
    
    LangMode = CurrentLang()
    CurrMode = CurrentCurrency()
    wpIdx = Val(GetIniPar$("Workplace"))

'GetRange(s)
          
    Worksheets("form").Select
        rBegin = FirstDataRow
        rEnd = GetLastRow()
    Worksheets("Attachment").Select
    
    LinkSourceDest rBegin - 1, FirstDataRow, rEnd - rBegin, "Attachment"
    
'Sign Placeholders

    If LangMode = 2 Then fFlag = 1: fsz = 9 Else fsz = 10: fFlag = 0

   Worksheets("Attachment").Activate

   DecoCell 20 + rEnd - rBegin, 2, coSign$(LangMode), , True, xlLeft
   DecoCell 20 + rEnd - rBegin, 4, iSIGN$(1, LangMode, wpIdx), , True, xlRight
   DecoCell 27 + rEnd - rBegin, 2, "=Form!C4", , True, xlLeft, True
   DecoCell 27 + rEnd - rBegin, 4, "=Form!C5", , True, xlRight, True
   uLine CellAddr$(20 + rEnd - rBegin, 2, 2), 1
   uLine CellAddr$(27 + rEnd - rBegin, 2, 2), 1

' Change Layout

   If LangMode = 2 Then xFsz = 9 Else xFsz = 10
   If LangMode = 2 Then xFsz2 = 8 Else xFsz2 = 10
    
   Range("B3:F6").Font.Name = xFNT$(LangMode)
   Range("A9:F9").Font.Name = xFNT$(LangMode)
   Range("A9:F9").Font.Size = xFsz2
   Cells(9, 3) = aHEAD$(1, LangMode)
   Cells(9, 3).Font.Size = xFsz
   Cells(9, 4) = aHEAD$(2, LangMode)
   Cells(9, 4).Font.Size = xFsz2 - 1
   Cells(9, 5) = aHEAD$(3, LangMode) + Chr$(10) + crSign$(CurrMode, LangMode)
   Cells(9, 5).Font.Size = xFsz - 1
   Cells(9, 6) = aHEAD$(4, LangMode) + Chr$(10) + crSign$(CurrMode, LangMode)
   Cells(9, 6).Font.Size = xFsz - 1
   If LangMode = 3 Then
      Cells(3, 4) = AttTITLE$(LangMode)
      Cells(3, 3) = Trim$(Str$(Worksheets("Form").Cells(7, 6).Value)) + AttSubTITLE$(LangMode)
   Else
      Cells(3, 3) = AttTITLE$(LangMode)
      Cells(3, 4) = AttSubTITLE$(LangMode) + Trim$(Str$(Worksheets("Form").Cells(7, 6).Value))
   End If
   Cells(5, 5) = wHEAD$(3, LangMode)
   Cells(5, 6).NumberFormat = "General"
   Cells(5, 6).Value = CurrentDateW$(LangMode, 2)
   Cells(5, 6).Font.Name = xFNT$(LangMode)
           
   Worksheets("Attachment").Range("A1").Select
  
End Sub

Public Sub LinkSourceDest(SourceRow, destRow, RowsQty, destSheet$, Optional ConvRate, Optional AddrMode As Integer, Optional useConv As Boolean)

   Dim LangMode As Integer, CurrMode As Integer
   Dim SyncDATA$, SyncFORMULA$, SyncFORMULA2$, vatShift As Integer
   Dim SUMxROW As Integer, SUMxCOL As Integer, xDWRow As Integer, xDWCol As Integer
   Dim dtLEN, i, xCol, xRow
   Dim rBegin As Integer, rEnd As Integer, rLines As Integer, xDP
   Dim totFORMULA$, selRNG$
   Dim decFont$, II$, IssuedBy$
   Dim vatFormula$, vatRANGE$, dgwFORMULA$
   Dim SumFrmPos As Integer, rType As Integer
   Dim sumFRM$, totFRM$, digiwordsFRM$, NewGTformula$, NewGTrange$

   rBegin = destRow
   rEnd = rBegin + RowsQty

   selRNG$ = "B" + Trim$(Str$(rBegin)) + ":F" + Trim$(Str$(rEnd))
        
   Worksheets(destSheet$).Activate
   Worksheets(destSheet$).Range(selRNG$).Select
   Selection.WrapText = True
   AddGrid xlHairline

   With Worksheets(destSheet$)
   For i = 1 To rEnd - rBegin + 1
      rType = qRowType(SourceRow + i, "Form", , True)
      If rType = 10 Then MakeSimpleTR rBegin - 1 + i
      SyncFORMULA$ = "=Form!C" + Trim$(Str$(SourceRow + i))
      .Cells(rBegin - 1 + i, 3).Formula = SyncFORMULA$
      If rType = 10 Then GoTo n1
      SyncFORMULA$ = "=Form!B" + Trim$(Str$(SourceRow + i))
      .Cells(rBegin - 1 + i, 2).Formula = SyncFORMULA$
      SyncFORMULA$ = "=Form!D" + Trim$(Str$(SourceRow + i))
      .Cells(rBegin - 1 + i, 4).Formula = SyncFORMULA$
      If IsMissing(ConvRate) Then ConvRate = 0
      If ConvRate = 0 Or ConvRate = 1 Then
         SyncFORMULA$ = "=Form!E" + Trim$(Str$(SourceRow + i))
         SyncFORMULA2$ = "=Form!F" + Trim$(Str$(SourceRow + i))
      Else
         SyncFORMULA$ = "=Form!E" + Trim$(Str$(SourceRow + i)) + "*" + Trim$(Str$(ConvRate))
         SyncFORMULA2$ = "=E" & SourceRow + i & "*D" & SourceRow + i
      End If
      .Cells(rBegin - 1 + i, 5).Formula = SyncFORMULA$
      .Cells(rBegin - 1 + i, 6).Formula = SyncFORMULA2$
n1: Next
    End With

'sync totals
    LangMode = CurrentLang()
    CurrMode = CurrentCurrency()
   
    If ConvRate > 0 Then CurrMode = 2
   
    Worksheets("Form").Activate
        
    xDWRow = FormulaRow(qsDIGI$)
    xDWCol = FormulaCol
    SUMxROW = FormulaRow(qsGT$)
    SUMxCOL = FormulaCol
    xRow = SUMxROW
    xCol = SUMxCOL - 2
    II$ = Chr$(34)
    SumFrmPos = destRow + RowsQty + 2
    sumFRM$ = "=SUM(F" + Trim$(Str$(rBegin)) + ":F" + Trim$(Str$(rEnd)) + ")"
    totFRM$ = tot$(LangMode) + crSign$(CurrMode, LangMode) + "):"
    
   Worksheets(destSheet$).Activate
   DecoCell SumFrmPos, 5, totFRM$, True, False, xlRight
   DecoCell SumFrmPos, 6, sumFRM$, True, False, xlRight, True, True
       
If GetVATmode() = 1 Then
   vatShift = 2
   vatRANGE$ = GetRange$(SumFrmPos, 6)
   vatFormula$ = "=FORM!F" + Trim$(Str$(SUMxROW + 1))
   NewGTrange$ = GetRange$(SumFrmPos, 6) + "+" + GetRange$(SumFrmPos + 1, 6)
   NewGTformula$ = "=AVERAGE(" + NewGTrange$ + ")"
   DecoCell SumFrmPos + 1, 5, InvSuffix$(LangMode, 1) + " (" + crSign$(CurrMode, LangMode) + "):", True, , xlRight
   DecoCell SumFrmPos + 1, 6, vatFormula$, True, , xlRight, True, True
   DecoCell SumFrmPos + 2, 5, InvSuffix$(LangMode, 2) + " (" + crSign$(CurrMode, LangMode) + "):", True, , xlRight
   DecoCell SumFrmPos + 2, 6, NewGTformula$, True, , xlRight, True, True
End If

   If AddrMode = 2 And useConv = True Then
      DecoCell SumFrmPos + vatShift + 3, 3, ConvPay$(LangMode), False, False, xlLeft
   End If

   digiwordsFRM$ = "=HowMuchIs(F" + Trim$(Str$(SumFrmPos + vatShift)) + "," + II$ + crSign$(CurrMode, LangMode) + II$ + "," + II$ + dwSign$(LangMode) + II$ + ")"
   DecoCell destRow + RowsQty + 3 + vatShift, 6, digiwordsFRM$, False, True, xlRight, True

   Range(selRNG$).WrapText = True
   Rows(RowsAddr$(rBegin, rEnd)).Select
   Rows(RowsAddr$(rBegin, rEnd)).EntireRow.AutoFit
   Columns("D:F").NumberFormat = "General"

End Sub

