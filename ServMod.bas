Attribute VB_Name = "ServMod"
Option Explicit
Option Compare Text
Private Cl As New Excel.Application
Private Clone As Excel.Application

Public Sub PutDate(CellX As Integer, CellY As Integer, ForceAz As Integer)
   Dim LangMode As Integer
   InitVariables
   If ForceAz = 1 Then LangMode = 3 Else LangMode = CurrentLang()
   DecoCell CellX, CellY, "'" + CurrentDateW$(LangMode, 1), True, , , , , , , LangMode
End Sub

Public Sub UnHideRows()
   Rows(RowsAddr$(FirstDataRow, MaximumROWS)).EntireRow.Hidden = False
End Sub

Public Sub AddDocHist(docName$, Optional MoreInfo$)

   Dim cTxt$

On Error Resume Next
   cTxt$ = "This " + docName$ + " was created on" + Chr$(10)
   cTxt$ = cTxt$ + Date$ + " " + Time$ + " by " + GetIniPar("user") + Chr$(10)
   cTxt$ = cTxt$ + MoreInfo$
   With Worksheets(docName$).Range("B1")
       .AddComment
       .Comment.Visible = False
       .Comment.Shape.Height = 35
       .Comment.Shape.Width = 200
       .Comment.Text Text:=cTxt$
   End With
On Error GoTo 0

End Sub

Public Sub ReScheduleAS()

   Dim tmInt$, tmVal$

    If LastSched <> 0 Then
        On Error Resume Next
        Application.OnTime LastSched, "ASTasks", Schedule:=False
        On Error GoTo 0
    End If
    
    If GetIniPar("AutoSave") = "YES" Then
        tmInt$ = GetIniPar$("ASTime")
        If Val(tmInt$) <= 0 Then Exit Sub
        tmVal$ = "00:" + Format$(tmInt$, "0#") + ":00"
        Application.OnTime Now + TimeValue(tmVal$), "ASTasks"
        LastSched = Now + TimeValue(tmVal$)
        Exit Sub
    End If

    LastSched = 0

End Sub

Public Sub ASTasks()
    AutoSave
    qReConnect
    ReScheduleAS
End Sub

Public Sub AutoSave()

   If Application.Workbooks.Count = 0 Then Exit Sub
   If GetIniPar("AutoSave") <> "YES" Then Exit Sub
           
   Dim ASavePath$, fName$, xERR$, wbIndex As Integer
   Dim Wb, sh
   Dim DocHasForm As Boolean
           
    ASavePath$ = SysRoot$ + ASaveRoot$

    For Each Wb In Application.Workbooks
        wbIndex = wbIndex + 1
        For Each sh In Wb.Worksheets
            If UCase$(sh.Name) = "FORM" Then
                On Error GoTo ErrHandler
                    Application.DisplayAlerts = False
                    fName$ = "QFR" + Trim$(Str$(wbIndex)) + "~" + Trim$(Str$(Format(Hour(Time), "00"))) + ".xls"
                    Wb.SaveCopyAs ASavePath$ + "\" + fName$
                    Application.DisplayAlerts = True
                On Error GoTo 0
           End If
        Next
    Next

    If GetIniPar("ASAll") <> "YES" Then Exit Sub

    wbIndex = 0
            
    For Each Wb In Application.Workbooks
        wbIndex = wbIndex + 1
        DocHasForm = False
        For Each sh In Wb.Worksheets
            If UCase$(sh.Name) = "FORM" Then DocHasForm = True
        Next
        If DocHasForm = False Then
            On Error GoTo ErrHandler
                Application.DisplayAlerts = False
                fName$ = "ODR" + Trim$(Str$(wbIndex)) + "~" + Trim$(Str$(Format(Hour(Time), "0#"))) + ".xls"
                Wb.SaveCopyAs ASavePath$ + "\" + fName$
                Application.DisplayAlerts = True
            On Error GoTo 0
        End If
    Next
Exit Sub

ErrHandler: '@@@@@@@@@@@@ ERROR HANDLER @@@@@@@@@@@@@@@@
    xERR$ = "[AutoSave] ERROR:" & Err.Number & " : " & Err.Description
    MsgBox xERR$, vbCritical, "QuickForms ERROR!"
    Resume Next

End Sub

Public Sub SetHotKeys()
    Dim i As Integer
   On Error Resume Next
    For i = 1 To MaxHotKeys
        If HotKey$(i, 1) = "" Then GoTo n1
        Application.OnKey HotKey$(i, 1), HotKey$(i, 2)
n1: Next
   On Error GoTo 0
End Sub

Public Sub ReleaseHotKeys()
   Dim i As Integer
   On Error Resume Next
    For i = 1 To MaxHotKeys
        If HotKey$(i, 1) = "" Then GoTo n1
        Application.OnKey HotKey$(i, 1), ""
   On Error GoTo 0
n1: Next
End Sub

Public Function GetLastRow() As Integer

    Dim i As Integer, rType As Integer
    Dim EndFlag As Boolean

    EndFlag = False
    i = FirstDataRow
    While EndFlag <> True
        rType = qRowType(i)
        If rType = 1 Then GetLastRow = i
        If rType = 4 Then Exit Function
        If i > MaximumROWS Then
            GetLastRow = FirstDataRow + 1
            Exit Function
        End If
        i = i + 1
    Wend

End Function

Public Sub CompDesc()
    PerformComp 1
End Sub
Public Sub UnCompDesc()
    PerformComp 2
End Sub
Public Sub WasteSp()
    PerformComp 3
End Sub
Public Sub DoCalbCells()
    PerformComp 4
End Sub

Private Sub PerformComp(compMode As Integer) '1=compact, 2=expand, 3=waste '4=calibrate
    
   Dim i As Integer, xMode As Integer, k As Integer
   Dim inText$, TotalCRS As Integer, AddCRs As Integer
            
   xMode = compMode
   SaveLocation True
        
   For i = FirstDataRow To MaximumROWS
      Select Case qRowType(i)
         Case 1
            inText$ = Cells(i, 3)
            If xMode = 1 Then DecoCell i, 3, CompText$(inText$), False, False, , , True, xlAutomatic, , , 10, True
            If xMode = 2 Then DecoCell i, 3, WasteSpaces$(UnCompText$(inText$)), , , , , True, xlAutomatic, , , 10, True
            If xMode = 3 Then DecoCell i, 3, WasteSpaces$(inText$), , , , , True, xlAutomatic, , , 10, True
            If xMode = 4 Then
               TotalCRS = 0
               For k = 1 To Len(inText$)
                  If Mid$(inText$, k, 1) = Chr$(10) Then TotalCRS = TotalCRS + 1
               Next
               If TotalCRS = 0 Then GoTo n
               AddCRs = Int(TotalCRS / CalbTool)
               If AddCRs < 1 Then GoTo n
               PutDataAccordingFormat i, 3, inText$ + String$(AddCRs, Chr$(10)), , True
             End If

          Case 4, 5, 7, 8
               Exit For
      End Select
n:      Next
   RestLocation True
End Sub

Public Function CopyFileFSO(sourceName$, destName$, statusPrefix$)

    CopyFileFSO = True

On Error GoTo ErrHandler
    Dim x, fs
    Set fs = CreateObject("Scripting.FileSystemObject")
    x = fs.CopyFile(sourceName$, destName$, True)
On Error GoTo 0
Exit Function

ErrHandler:
    CopyFileFSO = False
    Resume Next
End Function

Public Sub DoProtection()

   If Worksheets("Form").ProtectContents = True Then
      UnProtectDocument
      MsgBox "Protection Removed - Document UnSecured", vbInformation, "(QF) Message."
   Else
      If MsgBox("Would you like to Remove Comments before securing document ?", vbQuestion + vbYesNo, "(QF) Confirm ?") = vbYes Then
         RemoveComments
      End If
      ProtectDocument
      MsgBox "Protection Set - Document Secured", vbInformation, "(QF) Message."
    End If

End Sub

Public Sub ProtectDocument()

   With Worksheets("form").Range("A1:G100")
    .Locked = True
    .FormulaHidden = True
   End With
   Worksheets("form").Protect Password:="riskprotect", DrawingObjects:=True, Contents:=True, Scenarios:=True

End Sub

Public Sub UnProtectDocument()
   Worksheets("form").unProtect Password:="riskprotect"
   migInit
End Sub

Public Sub CreateBlankTemplateSilent()
On Error GoTo ErrHand
   Workbooks.Open QFMainPath$ + qftem$, 0
   migInit
   ReNewTemplate
   DecoCell 56, 3, GetIniPar$("user"), True, True, , , True
   DecoCell 57, 3, GetIniPar$("email"), True, True, , , True
Exit Sub
ErrHand:
   On Error GoTo 0
   MsgBox "Failed to Open QF Template", vbExclamation, "(QF) Warning !"
End Sub

Public Sub ReNewTemplate()
   If CheckProtect(False) = True Then Exit Sub
   With Worksheets("Form")
      .Cells(4, 3) = ""
      .Cells(5, 3) = ""
      .Cells(7, 3) = NewReference$()
   End With
   PutDate 6, 3, 0
End Sub

Public Sub AddDocFromTemplate(docName$, tempName$)
    
   Dim ws, prevName$, mt$
    
   Application.DisplayAlerts = False
   prevName$ = docName$
    For Each ws In ActiveWorkbook.Worksheets
        If InStr(1, ws.Name, docName$) > 0 Then prevName$ = ws.Name: ws.Delete
    Next
   Application.DisplayAlerts = True

On Error GoTo ErrHand
   If tempName$ <> "" Then
    With Workbooks(qfais$)
      .IsAddin = False
      .Worksheets(tempName$).Activate
      Cells.Select
      Selection.Copy
      .IsAddin = True
    End With
   End If
    
   ActiveWorkbook.Worksheets("Form").Activate
   Sheets.Add After:=Worksheets(Worksheets.Count)
   ActiveSheet.Name = prevName$
   If tempName$ <> "" Then ActiveSheet.Paste
   ActiveWindow.DisplayGridlines = False
   ActiveSheet.Range("A1").Select
   Worksheets(prevName$).Activate
   If InStr(1, prevName$, "TaxInv") <> 0 Then
      ChDocName prevName$, docName$
   Else
      Columns("J:O").EntireColumn.Hidden = True
   End If

On Error GoTo 0
   Exit Sub

ErrHand:
   On Error GoTo 0
   mt$ = "QF has deteced internal problems in Excel!" + Chr$(10)
   mt$ = mt$ + "Please Restart Excel, then repeat your operation again."
   MsgBox mt$, vbCritical, "(QF) AI-Diagnostic..."
   End

End Sub

Public Sub RemoveComments()
    Dim c
    If CheckProtect(False) = True Then Exit Sub
    For Each c In Worksheets("Form").comments
        c.Delete
    Next
End Sub

Public Sub AddComments(comments$)
   Dim prev$
 On Error Resume Next
    With Worksheets("form").Range("E9")
        .AddComment
        .Comment.Visible = False
        .Comment.Shape.Height = 100
        .Comment.Shape.Width = 160
         prev$ = .Comment.Text
        .Comment.Text Text:=prev$ & "[" & GetIniPar("id") & "] " & comments$ & Chr(10)
    End With
 On Error GoTo 0
End Sub

Public Sub DoEmail()
   
    Dim dlgAnswer As Boolean
    Dim errCount As Integer, x As Integer, xFN$
    Dim xRow As Integer, xCol As Integer
    Dim DigiWordsValue$, msgText$
    
    If GetMode() = 1 Then GoTo DoSend
    If qVerify() > 0 Then
      MsgBox "FOUND MULTIPLE ERRORS" + Chr$(10) + "Please Correct The Error(s), Save Aborted !" + Chr$(10) + "DOCUMENT WILL NOT BE SEND!", 16, "Send Aborted !"
      Exit Sub
    End If

DoSend:
    x = MsgBox("You're about to make PROTECTED eMail Form" + Chr$(10) + "Note! This Action Can NOT be Undone" + Chr$(10) + Chr$(10) + "Are You Sure ?", 308, "ATTENTION !")
    If x = 7 Then Exit Sub

AG1:
    RemoveComments

LockDigiwords:
    If GetMode() = 1 Then GoTo Protection
    xRow = FormulaRow(qsDIGI$)
    xCol = FormulaCol

    If xRow = 0 Or xCol = 0 Then
        MsgBox "The '=HowMuchIs()' Formula could not be found, Aborted" + Chr$(10), 64, "ERROR OCCURED (DoEmail)"
        Exit Sub
    End If

    With Cells(xRow, xCol)
        DigiWordsValue$ = .Value
        .Formula = ""
        .Value = DigiWordsValue$
    End With

Protection:
    ProtectDocument
    On Error GoTo ErrMail
        Application.Dialogs(xlDialogSendMail).Show "", "R.I.S.K. Commercial Offer, REF:" + Worksheets("form").Cells(7, 3)
    On Error GoTo 0
    Exit Sub

ErrMail:
    On Error GoTo 0
    msgText$ = "Sorry, but your eMail client & Excel configuration" + Chr$(10)
    msgText$ = msgText$ + "does not support direct send operation !!!" + Chr$(10) + Chr$(10)
    msgText$ = msgText$ + "Please do it Manually..." + Chr$(10)
    MsgBox msgText$
   
End Sub

Public Sub SaveCurrent()
    
    Dim errCount As Integer, x As Integer, xFN$
    
    If GetMode = 1 Then GoTo DoSave
    errCount = qVerify()
    If errCount <= 0 Then GoTo DoSave
    
    MsgBox "Warning !!!" + Chr$(10) + "Document Has Errors." + Chr$(10), vbExclamation, "WARNING !"

DoSave:
    Dim dlgAnswer As Boolean
    xFN$ = WhatFile(0)
    dlgAnswer = Application.Dialogs(xlDialogSaveAs).Show(xFN$)
    AfterSave dlgAnswer

End Sub

Private Sub CreateClone()
    Set Clone = Cl
    ActiveWorkbook.SaveCopyAs ActiveWorkbook.Path + "\~" + ActiveWorkbook.Name
    Clone.Workbooks.Open ActiveWorkbook.Path + "\~" + ActiveWorkbook.Name, 0, True
    Clone.DisplayAlerts = False
End Sub

Private Sub KillClone()
    Clone.Quit
    Set Cl = Nothing
    Set Clone = Nothing
    Kill ActiveWorkbook.Path + "\~" + ActiveWorkbook.Name
End Sub

Private Sub ExportSheet(sheetName$)

    Dim ShFound As Boolean, sh, name1$, name2$, name3$, fName$, FinalName
    ShFound = False
    
rest:
    If Clone.Worksheets.Count <= 0 Then Exit Sub
    For Each sh In Clone.Worksheets
        If UCase$(sh.Name) = UCase$(sheetName) Then
            ShFound = True
        Else
           sh.Delete
           GoTo rest
        End If
    Next

    If ShFound = False Then Exit Sub
    Clone.Worksheets(1).Name = "_" + sheetName$
    
    name1$ = Clone.Workbooks(1).Path + "\"
    name2$ = Mid$(ParseDivider$(Clone.Workbooks(1).Name, ".", 1), 2)
    name3$ = "~[" + sheetName$ + "]" '.xls"
    fName$ = name1$ + name2$ + name3$
    
    FinalName = Application.GetSaveAsFilename(fName$, "Excel Sheet, *.xls", , "Save Sheet As...")
    If FinalName = False Then Exit Sub
    Clone.Workbooks(1).SaveAs FinalName

End Sub

Public Sub DoExportSheet()
    CreateClone
    ExportSheet ActiveSheet.Name
    KillClone
End Sub


Public Sub MaintDigits()
    Dim CellMode$(2)
    Dim CCCur As Integer
    CCCur = CurrentCurrency()
    CellMode$(1) = "### ### ##0.##"
    CellMode$(2) = defNumb$
    Range("E9:F200").NumberFormat = CellMode$(CCCur)
End Sub

Public Sub FixDigiwords()

   Dim xRow As Integer, xCol As Integer
   Dim xFormula$, LocHOW As Integer

   xRow = FormulaRow(qsDIGI$)
   xCol = FormulaCol

   InitVariables

   If xRow = 0 Or xCol = 0 Then FixDigiwordsPointer: Exit Sub
   
   xFormula$ = Trim$(Worksheets("form").Cells(xRow, xCol).Formula)
   LocHOW = InStr(xFormula$, "HowMuchIs")
      
   Cells(xRow, xCol).Formula = "=" + Mid$(xFormula$, LocHOW)
   Cells(xRow, xCol).WrapText = False
   FixDigiwordsPointer

End Sub

Public Sub FixDigiwordsPointer()
   
   Dim vatShift As Integer
   Dim SUMxROW As Integer, SUMxCOL As Integer
   Dim DWDxRow As Integer, DWDxCol As Integer
   Dim SumPointer$, LangSign1$, NewFormula$, CurrSign1$
   Dim LangMode As Integer, CurrMode As Integer, II$
   
   II$ = Chr$(34)
   InitVariables
    
   If GetVATmode() = 1 Then vatShift = 2 Else vatShift = 0
   
   SUMxROW = FormulaRow(qsGT$)
   SUMxCOL = FormulaCol
    
   If SUMxROW = 0 Or SUMxCOL = 0 Then Exit Sub
    
   DWDxRow = FormulaRow(qsDIGI$)
   DWDxCol = FormulaCol
    
   If DWDxRow = 0 Or DWDxCol = 0 Then
       DWDxCol = SUMxCOL
       DWDxRow = SUMxROW + 1 + vatShift
   End If
    
   SumPointer$ = Chr$(64 + SUMxCOL) + Trim$(Str$(SUMxROW + vatShift))
   LangMode = CurrentLang()
   CurrMode = CurrentCurrency()
   NewFormula$ = "=HowMuchIs(" + SumPointer$ + "," + II$ + crSign$(CurrMode, LangMode) + II$ + "," + II$ + dwSign$(LangMode) + II$ + ")"
   DecoCell DWDxRow, 6, NewFormula$, , , xlRight, True
   Cells(DWDxRow + 1, 6).Value = ""
     
End Sub

Public Sub RestoreGrandTotal(LastSeenX)

   Dim WasteRange$, NewGTformula$, NewGTWords$
   Dim LangMode As Integer, CurrMode As Integer

   InitVariables
    
   WasteRange$ = "C" + Trim$(Str$(LastSeenX + 2)) + ":G" + Trim$(Str$(LastSeenX + 6))
   Range(WasteRange$).ClearContents
   OutlineCells WasteRange$, 0
    
   LangMode = CurrentLang()
   CurrMode = CurrentCurrency()
      
   Dim fsz(3)
   fsz(1) = 10: fsz(2) = 9: fsz(3) = 10

   NewGTformula$ = "=SUM(F" + Trim$(Str$(FirstDataRow)) + ":F" + Trim$(Str$(LastSeenX + 1)) + ")"
   NewGTWords$ = tot$(LangMode) + crSign$(CurrMode, LangMode) + "):"
   
   DecoCell LastSeenX + 3, 6, NewGTformula$, True, , xlLeft, True, True
   DecoCell LastSeenX + 3, 4, NewGTWords$, True, False, xlLeft
   uLine CellAddr$(LastSeenX + 3, 4, 2), 2

   If GetVATmode() = 1 Then AddVAT
   FixDigiwords
    
End Sub

Public Sub RemoveTopMerges()
  With Range("C4:C7")
   .HorizontalAlignment = xlGeneral
   .VerticalAlignment = xlCenter
   .WrapText = False
   End With
End Sub


Public Sub AlignFormulas(destSheet$)
   
   Dim i As Integer, pRow$, newFrm$
   
    With Worksheets(destSheet$)
        .Activate
        For i = FirstDataRow To MaximumROWS
            If qRowType(i) = 1 Then
                pRow$ = Trim$(Str$(i))
                newFrm$ = "=D" + pRow$ + "*E" + pRow$
                .Cells(i, 6).Formula = newFrm$
            End If
        Next
    End With
    
End Sub

Public Sub SaveLocation(Optional sUpdating As Boolean)
    If sUpdating = True Then Application.ScreenUpdating = False
    Dim RowAbsolute, ColumnAbsolute
    'Application.ScreenUpdating = False
    SRstack = SRstack + 1
    On Error Resume Next
        SavedRange$(SRstack) = Selection.Address(RowAbsolute, ColumnAbsolute)
    On Error GoTo 0
End Sub

Public Sub RestLocation(Optional sUpdating As Boolean)
   'If sUpdating = True Then Application.ScreenUpdating = True
   If SRstack <= 0 Then Range("A1").Select: SRstack = 0: Exit Sub
   On Error Resume Next
    Range(SavedRange$(SRstack)).Select
    SRstack = SRstack - 1
   On Error GoTo 0
End Sub

Public Sub DoTunePB()
   SaveLocation True
   
   Dim i As Integer, pbLoc As Integer, rType As Integer
   Dim RowAbsolute, ColumnAbsolute
   Dim pbLocRng$
      
   Worksheets("form").Activate
   ActiveSheet.ResetAllPageBreaks
   ActiveWindow.View = xlPageBreakPreview
R:
   For i = 1 To ActiveSheet.HPageBreaks.Count
      pbLocRng$ = ActiveSheet.HPageBreaks(i).Location.Address(RowAbsolute, ColumnAbsolute)
      pbLoc = Val(Mid$(pbLocRng$, 2))
      rType = qRowType(pbLoc - 1)
      If rType = 2 Then Set ActiveSheet.HPageBreaks(i).Location = Range("A" + Trim$(Str$(pbLoc - 2)))
   Next
   
   ActiveWindow.View = xlNormalView
   RestLocation True
End Sub

Public Sub PutDataAccordingFormat(CellX As Integer, CellY As Integer, NewData$, Optional forceEmph As Boolean, Optional xWrap As Boolean, Optional xBold As Boolean, Optional fromEditBox As Boolean)
    
   Dim FirstCR As Integer
   Dim cIndex
       
   If fromEditBox = True Then GoTo d
   If forceEmph = False Then
      cIndex = Cells(CellX, CellY).Characters(Start:=1, Length:=1).Font.ColorIndex
      If cIndex <> 1 And cIndex <> xlAutomatic Then forceEmph = True
   End If
d:
   With Cells(CellX, CellY)
       .Value = NewData$
       .WrapText = xWrap
       .Font.Bold = xBold
       .Font.ColorIndex = 1
   End With

   If forceEmph = False Then Exit Sub

   For FirstCR = 1 To Len(NewData$)
       If Mid$(NewData$, FirstCR, 1) = Chr$(10) Then Exit For
   Next

   With Cells(CellX, CellY).Characters(Start:=1, Length:=FirstCR).Font
       .Bold = True
       .ColorIndex = 49
   End With

End Sub

Public Function GetInvNo() As Integer
  On Error Resume Next
   GetInvNo = Val(Worksheets(RealShName$("Invoice")).Cells(1, 5).Value)
  On Error GoTo 0
End Function

Public Function GetConNo() As Integer
  On Error Resume Next
   GetConNo = Val(Worksheets(RealShName$("Form")).Cells(7, 6).Value)
  On Error GoTo 0
End Function

Public Function RealShName$(ShortShName$)
   Dim sh
   For Each sh In ActiveWorkbook.Sheets
      If InStr(1, sh.Name, ShortShName$) Then RealShName$ = sh.Name: Exit Function
   Next
   RealShName$ = ShortShName$
End Function

Public Function RealShSerial(ShortShName$) As Integer
   Dim sh, tmp$, pLock As Integer
   For Each sh In ActiveWorkbook.Sheets
      If InStr(1, sh.Name, ShortShName$) Then
         tmp$ = sh.Name
         pLock = InStr(1, tmp$, "-")
         If pLock = 0 Then RealShSerial = 0: Exit Function
         RealShSerial = Val(Mid$(tmp$, pLock + 1))
         Exit Function
      End If
   Next


End Function
Public Sub ChDocName(RealName$, ShortName$)
   Dim mt$, xNo$
   If RealName = ShortName$ Then
      mt$ = "Your " & ShortName$ & " needs to have serial number."
      mt$ = mt$ & "Please enter number according to Expected Blank #"
      xNo$ = InputBox(mt$, "(QF) Document Serial #")
      If xNo$ <> "" Then ActiveSheet.Name = ShortName$ & "-" & xNo$
   End If
End Sub

Public Function ScanDivider(shName$, Optional CostGoods As Double, Optional CostServices As Double, Optional cgFirstRow As Integer, Optional cgLastRow As Integer, Optional csFirstRow As Integer, Optional csLastRow As Integer) As Boolean

   'Returns------
   '
   CostGoods = 0
   CostServices = 0
   cgFirstRow = 0
   cgLastRow = 0
   csFirstRow = 0
   csLastRow = 0

   Const fRow As Integer = 15
   Const sCol As Integer = 6
   Dim lRow As Integer
   Dim sh$, i As Integer, rType As Integer
   Dim gotDiv As Boolean
      
   sh$ = RealShName$(shName$)
   lRow = FormulaRow(qsGT$, sh$)
   If lRow < 0 Then ScanDivider = False: Exit Function
   
   cgFirstRow = fRow
   
   For i = fRow To lRow - 2
      rType = qRowType(i, sh$, , True)
      If rType = 1 And gotDiv = False Then
         CostGoods = CostGoods + Worksheets(sh$).Cells(i, sCol).Value
         GoTo n
      End If
      If rType = 0 And gotDiv = False Then
         gotDiv = True
         cgLastRow = i - 1
         csFirstRow = i + 1
         GoTo n
      End If
      If rType = 1 And gotDiv = True Then
         CostServices = CostServices + Worksheets(sh$).Cells(i, sCol).Value
      End If
   
n: Next

      If gotDiv = True Then csLastRow = lRow - 2
      ScanDivider = True

End Function
