Attribute VB_Name = "qTextWorx"
Option Explicit
Option Compare Text

Public Function GetFileName$(pref$, id$, no, rev$, year, company$)
    GetFileName$ = pref$ + "~" + id$ + "~" + Trim$(Str$(Format(no, "###000"))) + rev$ + "~" + Trim$(Str$(year)) + " ~ " + company$
End Function

Public Function GetID$(Ref$)
    GetID$ = ParseDivider$(Ref$, "/", 1)
End Function

Public Function GetYR$(Ref$)
    GetYR$ = Val(ParseDivider$(Ref$, "/", 3))
End Function

Public Function GetNO(Ref$) As Integer
    GetNO = Val(ParseDivider$(Ref$, "/", 2))
End Function

Public Function GetREV$(Ref$)
    
    Dim tmpID$, i As Integer
    
    tmpID$ = ParseDivider$(Ref$, "/", 2)
    For i = 1 To Len(tmpID$)
        If Asc(Mid$(tmpID$, i, 1)) >= 65 Then
            GetREV$ = Mid$(tmpID$, i)
            Exit Function
        End If
    Next

End Function

Public Function ParseDivider$(inputSTR$, divSIGN$, divNO)

   Dim cPointer As Integer, pPointer As Integer, xMatch As Integer
   Dim i As Integer

    ParseDivider$ = ""
    cPointer = 1
    
    For i = 1 To divNO
        xMatch = InStr(cPointer, inputSTR$, divSIGN$)
        pPointer = cPointer
        cPointer = xMatch + 1
        If xMatch = 0 Then Exit For
    Next
    
    If xMatch = 0 And i < divNO Then Exit Function
    If pPointer = 1 And cPointer = 1 Then Exit Function
    If pPointer <> 1 And cPointer = 1 Then
        cPointer = Len(inputSTR$) + 2
    End If
    ParseDivider$ = Mid$(inputSTR$, pPointer, cPointer - pPointer - 1)

End Function

Public Function CurrentDateW$(LangMode As Integer, SizeMode As Integer)
   Dim Divider$(2)
   Divider$(1) = ", "
   Divider$(2) = " "
   CurrentDateW$ = Trim$(Str$(Day(Date))) + " " + Months$(Month(Date), LangMode, SizeMode) + Divider$(SizeMode) + Trim$(Str$(year(Date)))
End Function

Public Function Remove13$(inpText$)
    Remove13$ = ReplaceChar$(inpText$, Chr$(13), "")
End Function

Public Function ReplaceChar$(inpText$, SearchChar$, ReplaceText$)

   Dim rFlag As Boolean
   Dim outText$
   Dim TextLen As Integer, RestFrom As Integer, i As Integer
    
    outText$ = inpText$
    RestFrom = 1

Restart1:
    
    rFlag = False
    TextLen = Len(outText$)

    For i = RestFrom To TextLen
        If Mid$(outText$, i, 1) = SearchChar$ Then
            rFlag = True
            outText$ = Left$(outText$, i - 1) + ReplaceText$ + Mid$(outText$, i + 1)
            Exit For
        End If
    Next

    If rFlag = True Then
        RestFrom = i + Len(ReplaceText$)
        GoTo Restart1
    End If
    ReplaceChar$ = outText$

End Function

Public Function CompText$(inText$)
    
    Dim p2$, inText2$
    Dim fPoint As Integer
    
    fPoint = InStr(1, inText$, Chr$(10))
    If fPoint = 0 Then
        CompText$ = inText$
        Exit Function
    End If
           
    inText2$ = Mid$(inText$, fPoint + 1)
    p2$ = ReplaceChar$(inText2$, Chr$(10), "; ")
    
    CompText$ = Left$(inText$, fPoint) + p2$
    
End Function

Public Function UnCompText$(inText$)
    UnCompText$ = ReplaceChar$(inText$, ";", Chr$(10))
End Function

Public Function WasteSpaces$(inText$)

    Const MaxWaste As Integer = 4
    Dim waste$(MaxWaste)
    Dim rwst$(MaxWaste)
    Dim i As Integer, fPoint As Integer
    Dim p1$, p2$, tmpText$
    
    waste$(1) = Chr$(10) + " ":      rwst$(1) = Chr$(10)
    waste$(2) = " " + Chr$(10):      rwst$(2) = Chr$(10)
    waste$(3) = "  ":                rwst$(3) = " "
    waste$(4) = Chr$(10) + Chr$(10): rwst$(4) = Chr$(10)
    
        tmpText$ = inText$
    
r1: For i = 1 To MaxWaste
        fPoint = InStr(1, tmpText$, waste$(i))
        If fPoint = 0 Then GoTo n1
        p1$ = Left$(tmpText$, fPoint - 1)
        p2$ = Mid$(tmpText$, fPoint + Len(waste$(i)))
        tmpText$ = p1$ + rwst$(i) + p2$
        GoTo r1
n1: Next
        WasteSpaces$ = tmpText$

End Function

Public Function WhatFile$(xEML)

    'xEML = 1 E-Mail Mode
    'xEML = 0 Standard Modes
   
   Dim cMode As Integer, pref$, tmp$, comp$
   
    cMode = GetMode()
    
    tmp$ = Worksheets("form").Cells(7, 3)
    comp$ = Worksheets("form").Cells(4, 3)
    If xEML = 1 Then pref$ = "eMail~" + pref
    
    Select Case cMode
        Case 1
            pref$ = "PO"
        Case 2
            pref$ = "FO"
        Case 3
            GoTo cfSave
    End Select
        
    WhatFile$ = GetFileName$(pref$, GetID$(tmp$), GetNO(tmp$), GetREV$(tmp$), GetYR(tmp$), comp$)
Exit Function

cfSave:
   
   Dim HasSheets As Integer, tBit As Integer, newName$
   HasSheets = SheetBits()
   tBit = HasSheets And 4
   If tBit > 0 Then newName$ = Format$(GetInvNo(), "####0000") + "inv "
   tBit = HasSheets And 128
   If tBit > 0 Then newName$ = newName$ & Format$(GetConNo(), "####0000") + "con "
   newName$ = newName$ + "("
   tBit = HasSheets And 8
   If tBit > 0 Then newName$ = newName$ + "Ti"
   tBit = HasSheets And 16
   If tBit > 0 Then newName$ = newName$ + "Ts"
   tBit = HasSheets And 32
   If tBit > 0 Then newName$ = newName$ + "Cm"
   tBit = HasSheets And 64
   If tBit > 0 Then newName$ = newName$ + "Dn"
   If Len(comp$) > 30 Then comp$ = Left$(comp$, 29)
   WhatFile$ = newName$ + ")~" + comp$
   
End Function

Public Function ReplaceTag$(Data$, ReplaceWith$, tStart, tEnd)
    Dim temp$
    temp$ = Left$(Data$, tStart - 1)
    temp$ = temp$ + ReplaceWith$
    temp$ = temp$ + Mid$(Data$, tEnd)
    ReplaceTag$ = temp$
End Function

Public Function RowAddr(xRange$) As Integer
    RowAddr = Val(Mid$(xRange$, 2))
End Function

Public Function ColAddr(xRange$) As Integer
    ColAddr = Asc(UCase$(Left$(xRange$, 1))) - 64
End Function

Public Function GetRange$(xRow, xCol)
    GetRange$ = Trim$(Chr$(xCol + 64)) + Trim$(Str$(xRow))
End Function

Public Function CellAddr$(CellX As Integer, CellY As Integer, Optional incRight As Integer, Optional incDown As Integer)

   Dim baseAddr$, extAddr$

   baseAddr$ = Chr$(64 + CellY) + Trim$(Str$(CellX))
   If incRight <> 0 Then
      extAddr$ = Chr$(64 + CellY + incRight) + Trim$(Str$(CellX))
   End If
   If incDown <> 0 Then
      extAddr$ = Chr$(64 + CellY) + Trim$(Str$(CellX + incDown))
   End If
   If incDown <> 0 And incRight <> 0 Then
      extAddr$ = Chr$(64 + CellY + incRight) + Trim$(Str$(CellX + incDown))
   End If
   
   If extAddr$ = "" Then
      CellAddr$ = baseAddr$
   Else
      CellAddr$ = baseAddr$ + ":" + extAddr$
   End If

End Function

Public Function RowsAddr$(Row1 As Integer, Row2 As Integer)
   RowsAddr$ = Trim$(Str$(Row1)) + ":" + Trim$(Str$(Row2))
End Function

Public Function dateFormat(inVal$) As Date

   Dim i As Integer, k As Integer, t As Integer, dtVal$
   Dim pLock As Integer, xDay As Integer, xYear As Integer
   If Left$(inVal$, 1) = "'" Then inVal$ = Mid$(inVal$, 2)
   InitVariables
   
   For i = 1 To 12
      For k = 1 To 3
         For t = 1 To 2
            pLock = InStr(1, inVal$, Months$(i, k, t))
            If pLock > 0 Then
               xDay = Val(Mid$(inVal$, 1, pLock - 1))
               xYear = Val(Mid$(inVal$, pLock + Len(Months$(i, k, t))))
               dateFormat = DateSerial(xYear, i, xDay)
            End If
         Next t
      Next k
   Next i
   

End Function
