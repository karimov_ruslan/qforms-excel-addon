Attribute VB_Name = "ifControls"
Option Explicit
Option Compare Text
Global trInvNo          As Integer
Global trTIN            As Long
Global trCName$
Global trTotal          As Double
Global trTax            As Double
Global trCostGoods      As Double
Global trCostServices   As Double
Global trGT             As Double
Global trCID            As Integer
Global trMode           As Integer
Global trAnswer         As Boolean
Global trDate           As Date

Public Function PriviewInvPublish(xMode As Integer, invNo As Integer, Optional TIN As Long, Optional cName$, Optional aTotal As Double, Optional aTax As Double, Optional aGT As Double, Optional aCostGoods As Double, Optional aCostServices As Double, Optional actualDate As Date, Optional cID As Integer) As Boolean

   'xMode = 1 ' Priview Before Publish
   'xMode = 2 ' Previous Priview
   
   If xMode = 2 Then GoTo PrevPreview
      
   trInvNo = invNo
   trTIN = TIN
   trCName$ = cName$
   trTotal = aTotal
   trTax = aTax
   trCostGoods = aCostGoods
   trCostServices = aCostServices
   trGT = aGT
   trCID = cID
   trDate = actualDate
   trMode = xMode
   
      
   Load InvPre
   InvPre.Show
         
   PriviewInvPublish = trAnswer
   Exit Function
         
PrevPreview: '--------------------------------------------------
   
   
End Function

