VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} iSET 
   Caption         =   "QF Configuration..."
   ClientHeight    =   3375
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7635
   OleObjectBlob   =   "iSET.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "iSET"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Option Explicit
Option Compare Text

Private Sub ASena_Click()
   ASvalue.Enabled = ASena.Value
   AStime.Enabled = ASena.Value
   ASoptions.Enabled = ASena.Value
   ASall.Enabled = ASena.Value
   ASrotation.Enabled = ASena.Value
   
   If Val(Application.Version) >= 10 And ASena.Value = True Then
      asWarn.Visible = True
   Else
      asWarn.Visible = False
   End If

End Sub

Private Sub AStime_Change()
ASvalue.Value = AStime.Value
End Sub

Private Sub cmdRenew_Click()
   MaintTemplate
End Sub

Private Sub cmdUpdateQFS_Click()
   MaintLoader
End Sub

Private Sub CommandButton1_Click()
    writeDefINI
    qMenuInstall
    ini_Cancel_Click
End Sub

Private Sub ini_Cancel_Click()
    iSET.Hide
    Unload iSET
End Sub

Private Sub ini_Save_Click()
    CreateINI
    qMenuInstall
    CheckUpdateUID
    iSET.Hide
    Unload iSET
End Sub

Private Sub optQlink_Change()
   mPages("qlpage").Visible = optQlink.Value
End Sub

Private Sub UserForm_Activate()
   InitVariables
   Dim i As Integer
   For i = 0 To 2
      wpl.AddItem WkPlace$(i)
   Next
   FillINIdata
   ASena_Click
   optQlink_Change
   mPages.Value = 3
End Sub

Private Sub UserForm_Deactivate()
    ReadINI
End Sub

Private Sub wpl_Change()
   wpUpdate
End Sub

Private Sub wpOverride_Change()
   
   Dim tm As Boolean
   If wpOverride.Value = True Then
      tm = False
      wpUpdate
   Else
      tm = True
   End If
   
   optOwnership.Enabled = tm
   optAutoFit.Enabled = tm
   optCounters.Enabled = tm
   optQlink.Enabled = tm

End Sub

Private Sub wpUpdate()
   If wpOverride.Value <> True Then Exit Sub
   Dim wpMode As Integer
   
   wpMode = wpl.ListIndex
   optAutoFit.Value = False
   
   Select Case wpMode
      Case 0
         optOwnership.Value = True
         optCounters.Value = False
         optQlink.Value = False
      Case 1
         optOwnership.Value = False
         optCounters.Value = False
         optQlink.Value = True
      Case 2
         optOwnership.Value = False
         optCounters.Value = True
         optQlink.Value = True
   End Select
   
End Sub
