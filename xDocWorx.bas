Attribute VB_Name = "xDocWorx"
Option Explicit
Option Compare Text


Public Function WbSheet$(shName$)
   
   Dim sh, Wb
   For Each Wb In Application.Workbooks
      For Each sh In Wb.Worksheets
         If sh.Name = shName$ Then WbSheet$ = Wb.Name
      Next
   Next

End Function

Public Function TaxFN$()
   TaxFN$ = year(Date) & "-" & Format(Month(Date), "##00")
End Function

