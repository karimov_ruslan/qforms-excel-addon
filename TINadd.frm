VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} TINadd 
   Caption         =   "QF> Add New Customer & TIN"
   ClientHeight    =   1635
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5535
   OleObjectBlob   =   "TINadd.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "TINadd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Option Explicit
Option Compare Text

Private Sub cmdAdd_Click()
   DoAddTIN
End Sub

Private Sub cmdCancel_Click()
   cTIN$ = strEsc$
   Me.Hide
   Unload Me
End Sub

Private Sub newTIN_Change()
   cmdAdd.Enabled = IsNumeric(newTIN.Text)
   warn.Visible = cmdAdd.Enabled Xor True
End Sub

Private Sub UserForm_Activate()
   newCust.Text = cCustomer$
   newTIN.SetFocus
End Sub

