VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} iPRINT 
   Caption         =   "Print..."
   ClientHeight    =   1695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4770
   OleObjectBlob   =   "iPRINT.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "iPRINT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False









Private Sub prALL_Click()

    qtyCON = Val(cpCON.Text)
    qtyATT = Val(cpATT.Text)
    qtyINV = Val(cpINV.Text)

    PrintAll qtyCON, qtyATT, qtyINV

End Sub

Private Sub prATT_Click()

    cps = Val(cpATT.Text)
    PrintATT cps

End Sub

Private Sub prCANCEL_Click()

    iPRINT.Hide
    Unload iPRINT
    
End Sub

Private Sub prCON_Click()

    cps = Val(cpCON.Text)
    PrintCON cps
    
End Sub

Private Sub prINV_Click()
    
    cps = Val(cpINV.Text)
    PrintINV cps

End Sub

Private Sub UserForm_Activate()

    prINV.Enabled = False
    prCON.Enabled = False
    prATT.Enabled = False

'-----

    Set wbs = ActiveWorkbook.Worksheets
    For Each ws In wbs
        If ws.Name = "Invoice" Then prINV.Enabled = True
        If ws.Name = "Contract" Then prCON.Enabled = True
        If ws.Name = "Attachment" Then prATT.Enabled = True
    Next



End Sub

