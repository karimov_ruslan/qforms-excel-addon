Attribute VB_Name = "MathMod"
Option Compare Text
Option Explicit

Public Sub MakeMath(inValue As Double, opMode As Integer, CellX As Integer, CellY As Integer, Optional forCurrency As Integer)

   Dim i As Integer
   SaveLocation True
   
   If CellX > 0 And CellY > 0 Then
       MathSingleCell inValue, opMode, CellX, CellY, forCurrency
       GoTo ex
   End If

   For i = FirstDataRow To MaximumROWS
      If qRowType(i) = 1 Then MathSingleCell inValue, opMode, i, 5, forCurrency
   Next

ex:
   i = qVerify(True, , True)
   RestLocation True

End Sub

Private Sub MathSingleCell(inValue As Double, opMode As Integer, CellX As Integer, CellY As Integer, Optional forCurrency As Integer)

'opMode
  '=1 *
  '=2 /
  '=3 %+
  '=4 %-
  '=5 PB%+
  '=6 Round to Integer
  '=7 Round to .XX
   
  Dim SourceValue As Double, NewValue As Double, FinalValue As Double
  Dim msg$, PrevValue As Double
      
  If ApplyMath(CellX, CellY) = False Then Exit Sub
  If forCurrency = 0 Then forCurrency = CurrentCurrency()
   
  SourceValue = Cells(CellX, CellY).Value
    
  If opMode = 1 Then NewValue = SourceValue * inValue
  If opMode = 2 Then NewValue = SourceValue / inValue
  If opMode = 3 Then NewValue = SourceValue + (SourceValue / 100 * inValue)
  If opMode = 4 Then NewValue = SourceValue - (SourceValue / 100 * inValue)
  If opMode = 5 Then NewValue = SourceValue / (100 - inValue) * 100
On Error Resume Next
  If opMode = 6 Then CalcRoundMode = 0: NewValue = Round(SourceValue, 0)
  If opMode = 7 Then CalcRoundMode = 0: NewValue = Round(SourceValue, 2)
On Error GoTo 0

  If forCurrency = 2 Then NewValue = Int(NewValue)
  If CalcRoundMode = 1 Or CalcRoundMode = 0 Then
     FinalValue = NewValue
     PrevValue = SourceValue
  Else
     FinalValue = RoundFilter(NewValue)
     PrevValue = NewValue
  End If
    
' Zero Value Warning
       
   If FinalValue = 0 And SourceValue <> 0 Then
      msg$ = "WARNING! Rounding Integer causing a Zero Value!" + Chr$(10) + Chr$(10)
      msg$ = msg$ + "Would you like to leave value Un-Rounded? Otherwise Zero will appear!" + Chr$(10)
      msg$ = msg$ & "( Un-Rounded=" & PrevValue & ", Rounded=0 )"
      If MsgBox(msg$, vbYesNo + vbExclamation, "Q!Forms Calc...") = vbYes Then
         FinalValue = Round(PrevValue, 2)
      End If
   End If
    
  Cells(CellX, CellY).Value = FinalValue
  AddMathCommentsLocal opMode, CellX, CellY, SourceValue
    
End Sub

Public Sub ToggleCURR()
   CalcEuro = False
   CalcAzmEuro = False
   ToggleCURRgeneral
End Sub

Public Sub ToggleCURRae()
   CalcEuro = False
   CalcAzmEuro = True
   ToggleCURRgeneral
End Sub

Public Sub ToggleCURRue()
   CalcEuro = True
   CalcAzmEuro = False
   ToggleCURRgeneral
End Sub

Public Sub ToggleCURReu()
   CalcEuro = True
   CalcAzmEuro = False
   ToggleCURRgeneral
End Sub


Private Sub ToggleCURRgeneral()

   Dim i As Boolean
   If AllowCurrencyChange() = False Then Exit Sub
   If CheckProtect(False) = True Then Exit Sub
   
   If CalcEuro = False Then
      If CurrentCurrency() = 1 Then
         DoMath 1, 2
         If CalcCancel = True Then Exit Sub
         SwCurrAZM
      Else
         DoMath 2, 1
         If CalcCancel = True Then Exit Sub
         If CalcAzmEuro = True Then SwCurrEURO Else SwCurrUSD
      End If
   Else
      If CurrTag() = 1 Then
         DoMath 1, 1
         If CalcCancel = True Then Exit Sub
         SwCurrEURO
      Else
         DoMath 2, 1
         If CalcCancel = True Then Exit Sub
         SwCurrUSD
      End If
   End If
   
   i = qVerify(True)

End Sub

Public Sub DoRoundInt()
   DoMath 6
End Sub

Public Sub DoRound2()
   DoMath 7
End Sub

Public Sub DoPercentUP()
   DoMath 3
End Sub

Public Sub DoPercentDOWN()
    DoMath 4
End Sub

Public Sub DoPaybackMinus()
    DoMath 4
End Sub

Public Sub DoPaybackPlus()
    DoMath 5
End Sub

Public Sub SinglePercentUP()
    DoMathSingle 3
End Sub

Public Sub SinglePercentDOWN()
    DoMathSingle 4
End Sub

Public Sub SinglePBPlus()
    DoMathSingle 5
End Sub

Public Sub SinglePBMinus()
    DoMathSingle 4
End Sub

Private Function ApplyMath(CellX As Integer, CellY As Integer) As Boolean

   Dim msgText$

On Error GoTo ErrHand
    ApplyMath = True
    If IsNumeric(Cells(CellX, CellY)) = False Then GoTo BadArg
    If IsError(Cells(CellX, CellY)) = True Then GoTo BadArg
    If IsEmpty(Cells(CellX, CellY)) = True Then ApplyMath = False: Exit Function
    If Left$(Cells(CellX, CellY).Formula, 1) = "=" Then
        OutlineCells CellAddr$(CellX, CellY), 4
        msgText$ = "The Cell: " & Chr(64 + CellY) & CellX & " contains formula:" & Chr$(10) & Chr$(10)
        msgText$ = msgText$ & ActiveSheet.Cells(CellX, CellY).Formula & Chr$(10)
        msgText$ = msgText$ & "which is equal to: " & ActiveSheet.Cells(CellX, CellY).Value & Chr$(10) & Chr$(10)
        msgText$ = msgText$ & "Note, System is replacing fromula with its value and do calculation." & Chr$(10)
        msgText$ = msgText$ & "All Replacements are marking with ORANGE Outline !"
        MsgBox msgText$, vbExclamation, "[QF] Formula Replace..."
        ApplyMath = True
    End If
    On Error GoTo 0
    Exit Function

BadArg:
   ApplyMath = False
   OutlineCells CellAddr$(CellX, CellY), 2
   AddLocalComments CellX, CellY, "Calculation NOT Made !!!"
   msgText$ = "The Cell: " & Chr(64 + CellY) & CellX & " is not Numeric either contains Error !" & Chr$(10)
   msgText$ = msgText$ & "!!! Calculation Will NOT be Made !!!" & Chr$(10)
   msgText$ = msgText$ & "This cell is marked with RED Outline & Commented !"
   MsgBox msgText$, vbCritical, "[QF] Calculation NOT Done!"
   Exit Function

ErrHand:
    On Error GoTo 0
    msgText$ = "The Cell: " & Chr(64 + CellY) & CellX & " contains something wrong:" & Chr$(10) & Chr$(10)
    msgText$ = msgText$ & ActiveSheet.Cells(CellX, CellY).Formula & Chr$(10) & Chr$(10)
    msgText$ = msgText$ & "!!! Calculation Will NOT be Made !!!"
    MsgBox msgText$, vbCritical, "[QF] Cell Contents ERROR!"
    GoTo BadArg

End Function

Public Sub AddLocalComments(CellX, CellY, CommentText$)

  Dim CRange$, prev$
  
  CRange$ = Chr$(CellY + 64) + Trim$(Str$(CellX))
    
  On Error Resume Next
    With ActiveSheet.Range(CRange$)
        .AddComment
        .Comment.Visible = False
        .Comment.Shape.Height = 70
        .Comment.Shape.Width = 200
         prev$ = .Comment.Text
        .Comment.Text Text:=prev$ & "[" & GetIniPar("id") & "] " & CommentText$ & Chr(10)
    End With
 On Error GoTo 0

End Sub

Public Sub LoadCalc(opMode)
    CalcOpMode = opMode
    Load iCALC
    iCALC.Show
End Sub

Private Function RoundFilter(inpValue As Double) As Double
    RoundFilter = inpValue

On Error Resume Next
    Select Case CalcRoundMode
        Case 2
            RoundFilter = Round(inpValue, 0)
        Case 3
            RoundFilter = Round(inpValue, 2)
    End Select
On Error GoTo 0

End Function


Public Sub DoMath(opMode As Integer, Optional forCurrency As Integer)

   Dim inValue As Double

   If forCurrency = 0 Then forCurrency = CurrentCurrency()
   If CheckProtect(False) = True Then Exit Sub
   If opMode >= 6 Then GoTo s2
   LoadCalc opMode
   If CalcCancel = True Then Exit Sub
   inValue = Val(CalcValue$)
   If CalcValue$ = "" Or inValue = 0 Then Exit Sub

s2: MakeMath inValue, opMode, 0, 0, forCurrency

End Sub

Public Sub DoMathSingle(opMode As Integer)
    
    Dim CellX As Integer, CellY As Integer
    Dim inValue As Double
    Dim DataRNG$, RngSel, RngX, RowAbsolute, ColumnAbsolute
    
    If CheckProtect(False) = True Then Exit Sub
    LoadCalc opMode
    If CalcCancel = True Then Exit Sub
    inValue = Val(CalcValue$)
    If CalcValue$ = "" Or inValue = 0 Then Exit Sub
    Set RngSel = Selection
    For Each RngX In RngSel
        DataRNG$ = RngX.Address(RowAbsolute, ColumnAbsolute)
        CellX = Val(Mid$(DataRNG$, 2))
        CellY = Asc(UCase$(Left$(DataRNG$, 1))) - 64
        If ApplyMath(CellX, CellY) = False Then GoTo nx
        MathSingleCell inValue, opMode, CellX, CellY
nx: Next
   inValue = qVerify(True)

End Sub

Private Sub AddMathComments(opMode As Integer)

   If CalcNoComments = True Then Exit Sub
    
   Dim opCmt$(7)
   Dim opRound$(3)
   
   opCmt$(1) = "ALL# USD>AZM:" + CalcValue$
   opCmt$(2) = "ALL# AZM>USD:" + CalcValue$
   opCmt$(3) = "ALL# %UP: +" + CalcValue$ + "%"
   opCmt$(4) = "ALL# %DOWN: -" + CalcValue$ + "%"
   opCmt$(5) = "ALL# PB+" + CalcValue$ + "% added"
   opCmt$(6) = "ALL# Rounded to Integer"
   opCmt$(7) = "ALL# Rounded to .XX"
    
   opRound$(0) = ""
   opRound$(1) = ""
   opRound$(2) = "(int)"
   opRound$(3) = "(.xx)"
    
   AddComments opCmt$(opMode) + " " + opRound$(CalcRoundMode)

End Sub

Sub AddMathCommentsLocal(opMode As Integer, CellX As Integer, CellY As Integer, theValue As Double)

   If CalcNoComments = True Then Exit Sub
   Dim cmt$
   Dim opCmt$(5)
   Dim opRound$(3)
   
   opCmt$(1) = CalcCmt$(1) + CalcValue$
   opCmt$(2) = CalcCmt$(2) + CalcValue$
   opCmt$(3) = "%+:" + CalcValue$ + "%"
   opCmt$(4) = "%-:" + CalcValue$ + "%"
   opCmt$(5) = "PB:" + CalcValue$ + "%"
    
   opRound$(0) = ""
   opRound$(1) = ""
   opRound$(2) = "R:Fix"
   opRound$(3) = "R:.00"
    
   cmt$ = opCmt$(opMode) + " " + opRound$(CalcRoundMode)
   If opMode >= 3 Then
      cmt$ = cmt$ + " (PV:" & theValue & ")"
   Else
      cmt$ = cmt$ + " (PCV:" & theValue & ")"
   End If
        
   AddLocalComments CellX, CellY, cmt$

End Sub

Public Sub RollBack()

   Dim CellX As Integer, CellY As Integer
   Dim RowAbsolute, ColumnAbsolute, RngSel, RngX, DataRNG$, inValue As Double
   
   If CheckProtect(False) = True Then Exit Sub
      
   SaveLocation True
   Set RngSel = Selection
   For Each RngX In RngSel
      DataRNG$ = RngX.Address(RowAbsolute, ColumnAbsolute)
      CellX = Val(Mid$(DataRNG$, 2))
      CellY = Asc(UCase$(Left$(DataRNG$, 1))) - 64
      RestCellVal CellX, CellY
nx: Next
   inValue = qVerify(True, , True)
   RestLocation True

End Sub

Private Sub RestCellVal(CellX As Integer, CellY As Integer)

   Dim CRange$, prev$, NewCmt$, NewVal As Double

  CRange$ = Chr$(CellY + 64) + Trim$(Str$(CellX))
    
  On Error Resume Next
    With ActiveSheet.Range(CRange$)
         prev$ = .Comment.Text
  On Error GoTo 0
         If prev$ = "" Then Exit Sub
         NewVal = GetCommentPV(prev$)
         If NewVal = 0 Then Exit Sub
         Cells(CellX, CellY).Value = NewVal
         NewCmt$ = CutComment$(prev$)
         If NewCmt$ = "" Then
            .Comment.Delete
         Else
            .Comment.Text Text:=NewCmt$
         End If
    End With
 
End Sub

Private Function GetCommentPV(inTxt$) As Double

   Dim lr$, StartLoc As Integer, EndLoc As Integer
   Const PVL As Integer = 4
   
   lr$ = ReadLastRow$(inTxt$)
   StartLoc = InStr(1, lr$, "(PV:")
   If StartLoc = 0 Then GetCommentPV = 0: Exit Function
   EndLoc = InStr(StartLoc + PVL, lr$, ")")
   If EndLoc = 0 Then GetCommentPV = 0: Exit Function
   GetCommentPV = Val(Mid$(lr$, StartLoc + PVL, EndLoc - StartLoc - PVL))

End Function

Private Function CutComment$(inTxt$)
   
   Dim i As Integer, TotalCRS As Integer, cCR As Integer
   
   For i = Len(inTxt) To 1 Step -1
      If Mid$(inTxt$, i, 1) = Chr$(10) Then
         cCR = cCR + 1
         If cCR = 2 Then
            CutComment$ = Left$(inTxt$, i)
            Exit Function
         End If
      End If
      If cCR = 1 And i = 1 Then CutComment$ = "": Exit Function
   Next

End Function

Public Function ReadLastRow$(inTxt$)

   Dim i As Integer, TotalCRS As Integer, cCR As Integer
   
   For i = Len(inTxt) To 1 Step -1
      If Mid$(inTxt$, i, 1) = Chr$(10) Then
         cCR = cCR + 1
         If cCR = 2 Then
            ReadLastRow$ = Mid$(inTxt$, i + 1)
            Exit Function
         End If
      End If
      If cCR = 1 And i = 1 Then ReadLastRow$ = inTxt$: Exit Function
   Next

End Function

