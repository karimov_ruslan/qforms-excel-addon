Attribute VB_Name = "StartUP"
Dim EClassX As New EClass
Option Compare Text
Option Explicit

Public Sub QFINIT()

   If Application.Visible <> True Then Exit Sub
   If Val(Application.Version) < 9 Then MsgBox "Excel Version must be 2000 or higher !" + Chr$(10) + "Otherwise system may not work correctly." + Chr$(10) + "Please install fresh Excel.", vbExclamation, "(QF) Version Conflict !"

   If GetIniPar$("noSplash") <> "YES" Then intro.Show 0
   qSetPaths
   qConnect
   UpdateEnviroment
   ChangeToolbarCapts
   NotifyQFStart
   NotifyAddinsInstallation
   CheckUpdateUID
   If GetIniPar$("noSplash") <> "YES" Then Wait 2
   If NotifyQFVersion() = True Then intro.Show 0: Wait 3

   intro.Hide
   
End Sub

Public Sub qPreloadFiles()

   Dim CurrServer$

   On Error GoTo TryAlt
      Application.StatusBar = "Please Wait, Connecting to Primary Server (it may take few minutes)..."
      Open MainServer$ + "qfhost.ini" For Input As #1
      Close #1
      Application.StatusBar = ""
      CurrServer$ = MainServer$
   On Error GoTo 0
   
   GoTo qPreload
      
TryAlt:
   On Error GoTo 0
   On Error GoTo FailOut
      Application.StatusBar = "Please Wait, Connecting to Alternative Server (it may take few minutes)..."
      Open AltServer$ + "qfhost.ini" For Input As #1
      Close #1
      Application.StatusBar = ""
      CurrServer$ = AltServer$
   On Error GoTo 0
      
      
qPreload:
   Const MaxUpFiles As Integer = 20
   Dim upFile$(MaxUpFiles)
   Dim errCount As Integer, F1$, F2$, i As Integer, x As Boolean

   upFile$(1) = "addins.ini"
   upFile$(2) = "qfusers.ini"
   upFile$(3) = qftem$
   upFile$(4) = "qmods\QFweb.xla"
   upFile$(5) = "qmods\QFcon.xla"
   upFile$(6) = "qmods\QFcon_AIS.xla"
   upFile$(7) = "qmods\QFconAC.xla"
   upFile$(8) = "qmods\qfcmods.ini"
   upFile$(9) = "qmods\QFcon1.xla"
   upFile$(10) = "qmods\QFcon2.xla"
   upFile$(11) = "qmods\QFcon3.xla"
   upFile$(12) = "qmods\QFcon4.xla"
   upFile$(13) = "qmods\QFcon5.xla"
   upFile$(14) = "qmods\QFcon6.xla"
   upFile$(15) = "qmods\QFcon7.xla"
   upFile$(16) = "set_menu.ini"
   upFile$(17) = "set_cmd.ini"
   upFile$(18) = "prodwiz\chassis.ini"
   upFile$(19) = "prodwiz\parts.ini"
   upFile$(20) = "prodwiz\pricetag.ini"


   For i = 1 To MaxUpFiles
      F1$ = CurrServer$ + upFile$(i)
      F2$ = QFMainPath$ + upFile$(i)
      If RunCopy(F1$, F2$) = True Then
         x = CopyFileFSO(F1$, F2$, "")
         If x = False Then errCount = errCount + 1
      End If
   Next
   FixLastOnline
Exit Sub

FailOut:
   On Error GoTo 0
   Application.StatusBar = ""
   CheckLastOnline

End Sub

Public Sub qReConnect()
   InitVariables
   ConnectClasses
   SetHotKeys
   'Application.OnTime Now + TimeValue("00:03:00"), "qReConnect"
End Sub

Public Sub qConnect()
   qReConnect
   qPreloadFiles
   qMenuInstall
End Sub

Public Sub qSetPaths()
   On Error Resume Next
    MkDir SysRoot$
    MkDir SysRoot$ + ASaveRoot$
    MkDir SysRoot$ + CacheRoot$
    MkDir QFMainPath$
    MkDir QFTemplatesPath$
    MkDir QFHTMLpath$
    MkDir QFTempPath$
   On Error GoTo 0
End Sub

Public Sub ConnectClasses()
    Set EClassX.App = Application
End Sub

Public Function RunCopy(ServerFile$, LocalFile$) As Boolean

   Dim d1 As Date, d2 As Date

   On Error GoTo FailOut
      d1 = FileDateTime(ServerFile$)
   On Error GoTo 0

   On Error Resume Next
      d2 = FileDateTime(LocalFile$)
   On Error GoTo 0

   If d2 = 0 Or d2 = Empty Then RunCopy = True: Exit Function
   If DateValue(d1) > DateValue(d2) Then RunCopy = True Else RunCopy = False
   If DateValue(d1) = DateValue(d2) Then
      If TimeValue(d1) > TimeValue(d2) Then RunCopy = True
   End If
   
Exit Function
FailOut:
   On Error GoTo 0
   RunCopy = False
End Function

Private Sub FixLastOnline()
   Open Application.StartupPath + stickFile$ For Output As #1
      Print #1, Now
   Close #1
End Sub

Private Sub CheckLastOnline()
    
  Dim LastTime$, msgText$, RunTime As Integer
  LastTime = ""
  
  On Error Resume Next
   Open Application.StartupPath + stickFile$ For Input As #1
      Input #1, LastTime$
   Close #1
  On Error GoTo 0

   If LastTime = "" Then GoTo Violation
   RunTime = Day(Now) - Day(LastTime)
   If RunTime > AliveTime Or RunTime < 0 Then GoTo Violation
   Exit Sub
    
Violation:
   msgText$ = "Dear User, Your OffLine credit has Expired !" + Chr$(10) + Chr$(10)
   msgText$ = msgText$ + "QF System is Locked, Connect to Network and restart Excel" + Chr$(10)
   MsgBox msgText$, vbCritical, "QF System is Locked !"
   
   qMenuRemove
   ReleaseHotKeys
   Application.DisplayAlerts = False
   Workbooks("qf.xla").Close
   Application.DisplayAlerts = True
   End

End Sub

Public Sub PreloadTemplate(tempName$)

   Dim vbp, xERR$
   
On Error Resume Next
   For Each vbp In Application.VBE.VBProjects
      If InStr(1, vbp.filename, tempName$) > 0 Then Exit Sub
   Next
On Error GoTo ErrHand
   Workbooks.Open QFTemplatesPath$ + tempName$
On Error GoTo 0
   Exit Sub

ErrHand:
    On Error GoTo 0
    xERR$ = "[PreLoadQFW] ERROR:" & Err.Number & " : " & Err.Description
    MsgBox xERR$, vbCritical, "QuickForms ERROR!"

End Sub


Public Sub OffLoadTemplates()

   Dim vbp
On Error Resume Next
 Application.DisplayAlerts = False
   For Each vbp In Application.VBE.VBProjects
      If Left$(vbp.Name, 2) = "qf" Then
         If vbp.Name = "qfaddins" Then GoTo n
         Workbooks(vbp.Name + ".xla").Close
      End If
n: Next
 Application.DisplayAlerts = True
On Error GoTo 0

End Sub

Public Sub VFY(Optional DontSaveLoc As Boolean)
    
   If CheckProtect(True) = True Then Exit Sub
   If DontSaveLoc = False Then SaveLocation True
 
'step1
   
   Application.StatusBar = "(QF) Verify: Re-Numbering Items & Estimating VAT Mismatches..."
   CheckRefValid
   DoRenumber
   
   If GetMode() <> 1 Then
      Application.StatusBar = "(QF) Verify: Checking Formulas/Links & Calibrating VAT Mismatch..."
      MakeVerification True
   End If
   
'step 2
   Application.StatusBar = "(QF) Verify: Ownership / Signatures Check..."
   VerifySettings
   If GetIniPar$("noSignCheck") = "NO" Then SignatureMismatchCheck

'step 3
   Application.StatusBar = "(QF) Verify: Mainteining Document..."
   RemoveTopMerges
   MaintDigits
   FixDigiwords

   If DontSaveLoc = False Then RestLocation True
   Application.StatusBar = ""
    
End Sub

Public Sub doInv()
   Dim invName$
   qReConnect
   If preVerify(True) = False Then Exit Sub
   Application.ScreenUpdating = False
   Application.StatusBar = "(QF) Invoice Creation: Loading Template..."
   PreloadTemplate qfais$
   Application.StatusBar = "(QF) Invoice Creation: Modifying Template..."
   InsertInvoice
   Application.StatusBar = "(QF) Invoice Creation: Page Setup..."
   DoPageSetup
   Application.StatusBar = "(QF) Invoice Creation: Linking Content..."
   LinkInvoice
   invName$ = RealShName$("Invoice")
   AddDocHist invName$
   Application.StatusBar = "(QF) Invoice Creation: Aligning Formulas..."
   AlignFormulas invName$
   Range("A1").Select
   Application.StatusBar = ""
   Application.ScreenUpdating = True
End Sub

Public Sub doAtt()
   
   qReConnect
   If preVerify(True) = False Then Exit Sub
   Application.ScreenUpdating = False
   Application.StatusBar = "(QF) Attachment Creation: Loading Template..."
   PreloadTemplate qfais$
   Application.StatusBar = "(QF) Attachment Creation: Modifying Template..."
   InsertAttach
   Application.StatusBar = "(QF) Attachment Creation: Page Setup..."
   DoPageSetup
   Application.StatusBar = "(QF) Attachment Creation: Linking Content..."
   LinkAttachment
   AddDocHist "Attachment"
   Range("A1").Select
   Application.StatusBar = ""
   Application.ScreenUpdating = True
   
End Sub

Public Sub doAct()

   MsgBox "Sorry, this feature is not available at this version."

End Sub


Public Sub doCon()

   Dim LangMode As Integer, CurrentLine As Integer, shName$, newNo As Integer
   
   If preVerify(True) = False Then Exit Sub
   
   Application.ScreenUpdating = False
'Step 1
   Application.StatusBar = "(QF) Contract Creation: Creating Template..."
   InsertContract
   shName$ = RealShName$("Contract")
'Step 2
   Application.StatusBar = "(QF) Contract Creation: Page Setup..."
   Worksheets(shName$).Activate
   DoPageSetup
   SetConStyles
'Step 3
   Worksheets(shName$).Activate
   Worksheets(shName$).Range("A1:C100").Select
   Selection.Cells = ""
   Selection.Style = "Normal"

   LangMode = CurrentLang()
   CurrentLine = 1
       
   While Workbooks(QFCmodName$).Worksheets(ConTemplate$(LangMode)).Cells(CurrentLine, 2) <> "$$end$$"
       Application.StatusBar = "(QF) Contract Creation: Writing Paragraph..." + Str$(CurrentLine)
       DoCell CurrentLine, 2, shName$
       DoStyle CurrentLine, 2, shName$
       CurrentLine = CurrentLine + 1
   Wend
   Worksheets(shName$).Activate
   Rows("1:200").EntireRow.AutoFit
   
'Step 4
   Application.StatusBar = "(QF) Contract Creation: Tunning Page Breaks..."
   TunePages
   Worksheets(shName$).Activate
   AddDocHist shName$
   newNo = TrackGetNo(ConCountFile$) + 1
   ActiveSheet.Name = "Contract-" & newNo
   Range("B1").Select
   Application.ScreenUpdating = True
   Application.StatusBar = ""
    
End Sub

Public Sub PrepEverything()
    RunWizard
    doAtt
    doInv
End Sub

Public Sub DoTaxInvoice()
    
   Dim ConvRate
   Dim TaxMode As Integer, Ans As Integer
   Dim msg$, ans1$, MoreInfo$
    
   If preVerify(True) = False Then Exit Sub
    
   If CurrentCurrency() = 1 Then
       msg$ = "The Present Offer has made in USD!" + Chr$(10)
       msg$ = msg$ + "However TaxInvoice can be done only in AZM." + Chr$(10) + Chr$(10)
       msg$ = msg$ + "Please enter 1$/AZM Rate:"
       ans1$ = InputBox(msg$, "Convertion Required !", "")
       If ans1$ = "" Or Val(ans1$) <= 0 Then Exit Sub
       ConvRate = Val(ans1$)
       MoreInfo$ = "USD>AZM:" & ConvRate & " / "
   Else
       ConvRate = 1
   End If
    
   If GetVATsubMode() = 1 Then
       TaxMode = 1
       MoreInfo$ = MoreInfo$ + "VAT: Standard 18%"
   Else
       msg$ = "The Present Offer has no VAT." + Chr$(10)
       msg$ = msg$ + "Does customer has a Zero VAT Cerificate ?" + Chr$(10) + Chr$(10)
       msg$ = msg$ + "(if you're not sure, cancel operation)" + Chr$(10) + Chr$(10)
       Ans = MsgBox(msg$, vbYesNoCancel + vbQuestion, "VAT ?")
       Select Case Ans
          Case 6
             TaxMode = 3
             MoreInfo$ = MoreInfo$ + "VAT: Zero Certificate"
          Case 7
             TaxMode = 2
             MoreInfo$ = MoreInfo$ + "VAT: Not Applied"
          Case 2
             Exit Sub
       End Select
   End If
    
   Application.ScreenUpdating = False
   qReConnect
   Application.StatusBar = "(QF) TaxInvoice Creation: Loading Template..."
   PreloadTemplate qfais$
   Application.StatusBar = "(QF) TaxInvoice Creation: Modifying Template..."
   InsertTaxInvoice
   Application.StatusBar = "(QF) TaxInvoice Creation: Page Setup..."
   DoPageSetup
   Application.StatusBar = "(QF) TaxInvoice Creation: Linking Content..."
   LinkTaxInvoice ConvRate, TaxMode
   AddDocHist "TaxInv", MoreInfo$
   Application.ScreenUpdating = True
   Application.StatusBar = ""
   
End Sub

Public Sub DoTaxSheet()
   
   Dim cnvRate
   If preVerify(True) = False Then Exit Sub
   Application.DisplayAlerts = False
   Application.StatusBar = "(QF) TaxSheet Creation: Generating Template..."
   cnvRate = InsertTaxSheet()
   If cnvRate < 0 Or cnvRate = Empty Then GoTo ex
   Application.StatusBar = "(QF) TaxSheet Creation: Linking Content..."
   LinkTaxSheet cnvRate
ex:
   Application.DisplayAlerts = True
   Application.StatusBar = ""
   
End Sub

Private Sub NotifyQFStart()

   Dim msg$
      
   If RunCopy(MainServer$ + "qfSTART.xla", WhereIsAddins$("QFSTART")) = True Then
      msg$ = "QF-Start module has new Version, Please Update." + Chr$(10) + Chr$(10)
      msg$ = msg$ + "Instructions:" + Chr$(10)
      msg$ = msg$ + "Follow to: Q!Forms > Misc. > Settings > Maintenance" + Chr$(10)
      msg$ = msg$ + "and press: [Update QF-Start Module] Button."
      MsgBox msg$, vbInformation, "(QF) Notification..."
   End If

End Sub

Private Function NotifyQFVersion() As Boolean
   
   Dim TmpBuild As Integer, PrevBuild$, msg$
   TmpBuild = Val(GetIniPar$("PrevBuild"))
   If TmpBuild = 0 Then PrevBuild$ = "Unknown" Else PrevBuild$ = Trim$(Str$(TmpBuild))

   If QFbuild > TmpBuild Then
      NotifyQFVersion = True
      PutIniPar "PrevBuild", Trim$(Str$(QFbuild))
      msg$ = "Q!Forms Version has changed (after self-update)" + Chr$(10) + Chr$(10)
      msg$ = msg$ + "Previous Build was " + PrevBuild$ + Chr$(10)
      msg$ = msg$ + "New Version is " & QFversion & " / Build: " & QFbuild
      MsgBox msg$, vbInformation, sdTitle$
   End If
      
End Function

Private Sub NotifyAddinsInstallation()

   Dim msg$
   On Error GoTo chk2
   If Application.Run("QFSTART.XLS!InstState") = True Then Exit Sub
   
chk2:
   On Error GoTo 0
   If WhereIsAddins$("QFSTART.XLA") = "" Then
n1:   msg$ = "Q!Forms AddIns Incorrecty Installed!" + Chr$(10)
      msg$ = msg$ + "QFSTART.XLA is NOT Enabled!" + Chr$(10) + Chr$(10)
      msg$ = msg$ + "Follow to Menu > Tools > Addins, and make sure that QFSTART is enabled." + Chr$(10)
      msg$ = msg$ + "and QF.XLA is disabled."
      MsgBox msg$, vbExclamation, sdTitle$
      GoTo n2
   Else
      If Application.AddIns("QFSTART").Installed = False Then GoTo n1
   End If
n2:
   If WhereIsAddins$("QFADDINS") <> "" Then
      If Application.AddIns("QF").Installed = False Then Exit Sub
      msg$ = "QF!Forms addins incorrecty installed!" + Chr$(10)
      msg$ = msg$ + "QF.XLA is in use isted of QFSTART.XLA" + Chr$(10) + Chr$(10)
      msg$ = msg$ + "Follow to Menu > Tools > Addins, and make sure that QFSTART is enabled." + Chr$(10)
      msg$ = msg$ + "and QF.XLA is disabled."
      MsgBox msg$, vbExclamation, sdTitle$
   End If

End Sub

Public Sub CheckUpdateUID()

   Dim tmpID$, i As Integer, msg$
   
   ReadUsersINI
   tmpID$ = GetIniPar$("ID")
   
   For i = 1 To CurQFusers
      If qfusers(i, 1) = tmpID$ Then SystemLocked = False: Exit Sub
   Next
   
   For i = 1 To CurQFusers
      If qfusers(i, 2) = tmpID$ Then
         PutIniPar "ID", qfusers(i, 1)
         msg$ = "Dear " + GetIniPar$("user") + ", " + Chr$(10)
         msg$ = msg$ + "Your Personal ID number has been automatically upgraded" + Chr$(10)
         msg$ = msg$ + "to the new Standard. (old ID:" + tmpID$ + " / NEW ID:" + qfusers(i, 1) + ")"
         MsgBox msg$, vbInformation, "(QF) Notice !"
         SystemLocked = False
         Exit Sub
      End If
   Next
   
UnregisteredUser:

   msg$ = "Your Current ID is incorrect or not registered in database !" + Chr$(10)
   msg$ = msg$ + "Please Correct ID or contact Administration !" + Chr$(10) + Chr$(10)
   msg$ = msg$ + "QF SYSTEM LOCKED !"
   MsgBox msg$, vbCritical, "(QF) Warning !!!"
   SystemLocked = True
      
End Sub
