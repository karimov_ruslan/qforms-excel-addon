Attribute VB_Name = "STmod"
Public Sub InsertSubTotals()

   If CheckProtect(False) = True Then Exit Sub
   If STallowed() = False Then Exit Sub
    
   Dim i As Integer
   Dim RestartRequired As Boolean
   
   SaveLocation True

Restart1:
    GrpBegin = 0
    GrpEnd = 0
    RestartRequired = False

    ZoneEnd = FormulaRow(qsGT$) - 1
    If ZoneEnd <= 0 Then GoTo ex
    
    For i = FirstDataRow To ZoneEnd
        If qRowType(i) = 2 Then ' Beginning of the group
            GrpBegin = i + 1
            GoTo nx
        End If

        If qRowType(i) = 3 Then ' Already has subtotal !
            GrpBegin = 0
            GrpEnd = 0
            GoTo nx
        End If
            
        If qRowType(i) = 0 Then 'Possibly it's the end of the group
            If GrpBegin <= 0 Then GoTo nx 'No beginning was previuosly found
            GrpEnd = i
            InsertSTrow GrpBegin, GrpEnd - GrpBegin - 1, 0
            RestartRequired = True
            GrpBegin = 0
            GrpEnd = 0
        End If

nx: If RestartRequired = True Then GoTo Restart1
    Next
ex:
    RestLocation True

End Sub

Public Sub RemoveSubTotals()

    If CheckProtect(False) = True Then Exit Sub
    If HasSubTotal() = False Then Exit Sub
    If STallowed() = False Then Exit Sub

    SaveLocation True
    
    Dim i As Integer
    Dim RestartRequired As Boolean

Restart1:
    
    RestartRequired = False

    ZoneEnd = FormulaRow(qsGT$) - 1
    If ZoneEnd <= 0 Then
        Exit Sub 'verification and repairment is needed
        RestLocation
    End If
    
    For i = FirstDataRow To ZoneEnd
        If qRowType(i) = 3 Then ' It has subtotal !
            RestartRequired = True
            RemoveSTrow i
            GrpBegin = 0
        End If
         
nx:     If RestartRequired = True Then GoTo Restart1
    Next
        RestLocation True
End Sub

Public Sub TranslateSubTotals(LangMode)

Dim LocRow As Integer, LocCol As Integer

xWhat$ = "=SUM(F"

With ActiveSheet.Range("E11:E500")
    Set c = .Find(What:=xWhat$, LookIn:=xlFormulas, LookAt:= _
        xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, MatchCase:=True)
    If Not c Is Nothing Then
        firstAddress = c.Address
        Do
            LocRow = Val(Mid$(c.Address(RowAbsolute, ColumnAbsolute), 2))
            LocCol = 4
            DecoCell LocRow, LocCol, stot$(LangMode), True
            
            Set c = .FindNext(c)
        Loop While Not c Is Nothing And c.Address <> firstAddress
    End If
End With
End Sub

Public Sub InsertSTrow(xRow, xLines, FootRow)

   Dim stRow As Integer, LangMode As Integer
   Dim stRange$, CLRange$, BLRange$, stFormula$
   
    If CheckProtect(False) = True Then Exit Sub

    InitVariables
    LangMode = CurrentLang()

    If FootRow = 0 Then stRow = xRow + xLines + 1 Else stRow = FootRow
    stRange$ = RowsAddr$(stRow, stRow)
    CLRange$ = "B" + Trim$(Val(stRow)) + ":F" + Trim$(Val(stRow))
    BLRange$ = "F" + Trim$(Val(xRow)) + ":F" + Trim$(Val(xRow + xLines))
    stFormula$ = "=SUM(F" + Trim$(Val(xRow)) + ":F" + Trim$(Val(xRow + xLines)) + ")"
    
   Rows(stRange$).Select
   Selection.Insert Shift:=xlDown
   
   DecoInter CLRange$, 35
   DecoInter BLRange$, 35
   DecoCell stRow, 4, stot$(LangMode), True, False, xlRight, , , 11
   DecoCell stRow, 5, stFormula$, True, , xlCenter, True, True, 11
   DecoCell xRow + xLines + 1, 6, "'<<", True, , xlLeft, , True, 3

End Sub

Public Sub RemoveSTrow(stRow As Integer)

   If CheckProtect(False) = True Then Exit Sub

   stFormula$ = ActiveSheet.Cells(stRow, 5).Formula
   If UCase$(Left$(stFormula$, 5)) <> "=SUM(" Then Exit Sub
   stRange$ = Mid$(stFormula$, 6)
   stRange$ = ParseDivider$(stRange$, ":", 1)
   GrpBegin = Val(Mid$(stRange$, 2))
      
   Rows(RowsAddr$(stRow, stRow)).Select
   Selection.Delete Shift:=xlUp
                            
   BLRange$ = "F" + Trim$(Val(GrpBegin)) + ":F" + Trim$(Val(stRow - 1))
   DecoInter BLRange$, xlAutomatic
   
End Sub

Private Function STallowed() As Boolean

    If GetMode() = 2 Then
        STallowed = True
    Else
        STallowed = False
        MsgBox "You're able to operate with Subtotals in Final Offer", , "Warning !"
    End If

End Function

Private Function HasSubTotal() As Boolean
    Dim i As Integer
    HasSubTotal = False
    For i = FirstDataRow To MaximumROWS
        If qRowType(i) = 3 Then ' It has subtotal !
            HasSubTotal = True
            Exit Function
        End If
    Next
End Function

Public Sub addSingleST()
    
    Dim xLoc As Integer
    
    If UCase$(ActiveSheet.Name) <> "FORM" Then Exit Sub
    If CheckProtect(False) = True Then Exit Sub
    If GetMode() <> 2 Then Exit Sub
        
    SaveLocation True
    xLoc = Val(Mid$(ActiveCell.Address(RowAbsolute, ColumnAbsolute), 2))
    SUMxROW = FormulaRow(qsGT$)
    
    If qRowType(xLoc) <> 2 Then GoTo ex
    xLoc = xLoc + 1
    firstRow = xLoc
    
    While xLoc < SUMxROW
        rType = qRowType(xLoc)
        If rType = 2 Or rType = 3 Then GoTo ex
        If rType = 0 Then
            InsertSTrow firstRow, xLoc - firstRow - 1, 0
            GoTo ex
        End If
        xLoc = xLoc + 1
    Wend
ex:
   RestLocation True
    
End Sub

Public Sub removeSingleST()
    
    Dim xLoc As Integer
    
    If UCase$(ActiveSheet.Name) <> "FORM" Then Exit Sub
    If CheckProtect(False) = True Then Exit Sub
    If GetMode() <> 2 Then Exit Sub
    
    SaveLocation True
    xLoc = Val(Mid$(ActiveCell.Address(RowAbsolute, ColumnAbsolute), 2))
    firstRow = xLoc
    SUMxROW = FormulaRow(qsGT$)
    
    rType = qRowType(xLoc)
    If rType <> 1 And rType <> 2 And rType <> 3 Then GoTo ex
    
    While xLoc < SUMxROW
        rType = qRowType(xLoc)
        If rType = 0 Or rType = 4 Then GoTo ex
        If rType = 3 Then RemoveSTrow xLoc: GoTo ex
        xLoc = xLoc + 1
    Wend
ex:
   RestLocation True

End Sub

