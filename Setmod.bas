Attribute VB_Name = "Setmod"
Option Compare Text

Private Function IniExists(xPath$) As Boolean
   
   On Error GoTo FileAbsent
       Open xPath$ For Input As 2
       Close #2
       IniExists = True
   On Error GoTo 0
       Exit Function
      
FileAbsent:
    On Error GoTo 0
    IniExists = False

End Function

Public Sub ReadINI()
    
    Dim flUpdateINI As Boolean
    
ReadAgain:
    flUpdateINI = False
    If IniExists(INIpath$) = False Then
        writeDefINI
        Load iSET
        iSET.Show
        Exit Sub
    End If
    
    Erase INILines$
    Open INIpath$ For Input As 1
    Input #1, iniVerify$
    If Left$(iniVerify$, Len(IniTag$)) <> IniTag$ Then
        Close #1
        BrokenINI
        Exit Sub
    End If
       
   fileVersion = Val(Right$(iniVerify$, 4))
   If fileVersion < IniVersion Then flUpdateINI = True

   iniPos = 1
   While Not EOF(1)
      Input #1, iniStr$
      If iniStr$ = "" Then GoTo NextWend
      EqSignLoc = InStr(iniStr$, "=")
      If EqSignLoc = 0 Then
         INILines$(iniPos, 1) = iniStr$
         GoTo NextRound
      End If
      INILines$(iniPos, 1) = Trim$(Left$(iniStr$, EqSignLoc - 1))
      INILines$(iniPos, 2) = Trim$(Mid$(iniStr$, EqSignLoc + 1))
      INILines$(iniPos, 3) = "R"

NextRound:
      iniPos = iniPos + 1
NextWend:
        Wend
        Close #1
    If flUpdateINI = True Then writeDefINI: GoTo ReadAgain

End Sub

Public Sub WriteINI()

   If GetIniPar$("AutoSave", True) = "YES" Then PrevAS = True Else PrevAS = False
   Open INIpath$ For Output As 1
   Print #1, IniTag$ + " = Version " + Trim$(Str$(IniVersion))
   Print #1, ""
   iniPos = 1
   
   While INILines$(iniPos, 1) <> "[END]"
      If INILines$(iniPos, 1) = "" Then
         Print #1, ""
         GoTo oops
      End If
      Print #1, INILines$(iniPos, 1) + "=" + INILines$(iniPos, 2)
oops:
      iniPos = iniPos + 1
      If iniPos > maxINI Then GoTo ClFL
        
   Wend
ClFL:
   Print #1, ""
   Print #1, "[END]"
   Close #1

   UpdateEnviroment

End Sub

Public Function GetIniPar$(ParID$, Optional noReadINI As Boolean)
        
    If noReadINI = False Then ReadINI

    iniPos = 1
    While UCase$(INILines$(iniPos, 1)) <> "[END]"
       If LCase$(INILines$(iniPos, 1)) = LCase$(ParID$) Then
           GetIniPar$ = INILines$(iniPos, 2)
           Exit Function
       End If
       iniPos = iniPos + 1
       If iniPos >= maxINI Then Exit Function
    Wend

    GetIniPar$ = ""

End Function

Public Sub PutIniPar(ParID$, ParData$)
    
    ReadINI
    iniPos = 1
        
    While INILines$(iniPos, 1) <> "[END]"
        If INILines$(iniPos, 1) = ParID$ Then
            INILines$(iniPos, 2) = ParData$
            GoTo doWrite
            Exit Sub
        End If
      iniPos = iniPos + 1
    Wend

    INILines$(iniPos, 1) = ParID$
    INILines$(iniPos, 2) = ParData$
    INILines$(iniPos + 1, 1) = "[END]"
    INILines$(iniPos + 1, 2) = ""

doWrite:
    WriteINI

End Sub

Public Sub CreateINI()
    
   Erase INILines$
   Dim yn$(-1 To 1)
   
   yn$(-1) = "YES"
   yn$(1) = "YES"
   yn$(0) = "NO"
    
   PutIniPar "user", Trim$(iSET.ini_UN.Text)
   PutIniPar "email", Trim$(iSET.ini_UE.Text)
   PutIniPar "id", Trim$(UCase$(iSET.ini_UID.Text))
   PutIniPar "lastref", Trim$(Str$(Val(iSET.ini_LR.Text)))
 
With iSET
   PutIniPar "optOwnership", yn$(.optOwnership.Value)
   PutIniPar "AutoFit", yn$(.optAutoFit.Value)
   PutIniPar "AutoSave", yn$(.ASena.Value)
   PutIniPar "ASTime", Trim$(Str$(.ASvalue.Value))
   PutIniPar "ASAll", yn$(.ASall.Value)
   PutIniPar "ServerCounter", yn$(.optCounters.Value)
   PutIniPar "QLink", yn$(.optQlink.Value)
   PutIniPar "optSimp", yn$(.optSimp.Value)
   PutIniPar "Workplace", Trim$(Str$(.wpl.ListIndex))
   PutIniPar "wpOverride", yn$(.wpOverride.Value)
   PutIniPar "qlTCPIP", yn$(.sco1.Value)
   PutIniPar "noSplash", yn$(.noSplash.Value)
   PutIniPar "tsTop", .tsTop.Value
   PutIniPar "tsLeft", .tsLeft.Value
   PutIniPar "tiTop", .tiTop.Value
   PutIniPar "tiLeft", .tiLeft.Value
   PutIniPar "noSignCheck", yn$(.noSignCheck.Value)
   PutIniPar "disVatt", yn$(.disVatt.Value)
   PutIniPar "Ready2Buy", yn$(.Ready2Buy.Value)
   PutIniPar "BackupHTML", yn$(.BackupHTML.Value)
         
   If .tgo0.Value = True Then PutIniPar "qlTinMode", "0"
   If .tgo1.Value = True Then PutIniPar "qlTinMode", "1"
   If .tgo2.Value = True Then PutIniPar "qlTinMode", "2"
   
   If .mail0.Value = True Then PutIniPar "MailClient", "Express"
   If .mail1.Value = True Then PutIniPar "MailClient", "Outlook"
   
   If .sLevel0.Value = True Then PutIniPar "SimplifyLevel", "0"
   If .sLevel1.Value = True Then PutIniPar "SimplifyLevel", "1"
   If .sLevel2.Value = True Then PutIniPar "SimplifyLevel", "2"
   If .sLevel3.Value = True Then PutIniPar "SimplifyLevel", "3"

End With
        
End Sub

Private Sub BrokenINI()
    MsgBox "QFADDINS.INI is Broken", vbCritical + vbOKOnly, "WARNING!"
    Load iSET
    iSET.Show
End Sub

Public Sub VerifySettings()

    If GetIniPar("optOwnership") <> "YES" Then Exit Sub
    If Ownership() = True Then Exit Sub
    
    msgText$ = "Warning! You're not creator of this document" + Chr$(10) + Chr$(10)
    msgText$ = msgText$ + "Whould You like to change ownership" + Chr$(10)
    msgText$ = msgText$ + "and set new reference ?" + Chr$(10) + Chr$(10)
    x = MsgBox(msgText$, vbExclamation + vbYesNo, "Ownership Warning.")
    
    If x = 0 Or x = 7 Then Exit Sub
    Worksheets("Form").Cells(6, 3) = CurrentDateW$(CurrentLang(), 2)
    Worksheets("Form").Cells(7, 3) = NewReference$()
    MsgBox "Reference & Date were Changed.", vbInformation
    
End Sub

Public Function NewReference$()
    UID$ = UCase$(GetIniPar("id"))
    LRF$ = Trim$(Str$(Val(GetIniPar("lastref")) + 1))
    CYR$ = Trim$(Str$(year(Date)))
    NewReference = UID$ + "/" + LRF$ + "A/" + CYR$
End Function

Public Function Ownership() As Boolean

    machineowner$ = UCase$(GetIniPar$("id"))
    If UCase$(Left$(machineowner$, 1)) = "A" Then
        Ownership = True
        Exit Function
    End If
    If machineowner$ = DocumentOwner$() Then Ownership = True Else Ownership = False

End Function

Public Sub AfterSave(xResult As Boolean)
    
   Dim HasSheets As Integer, tBit As Integer
   If xResult = False Then Exit Sub
   
   HasSheets = SheetBits()
   tBit = HasSheets And 1
   If tBit < 0 Then Exit Sub
   If Ownership() = True Then
      If DocumentRefNo() > Val(GetIniPar$("lastref")) Then PutIniPar "lastref", Trim$(Str$(DocumentRefNo()))
   End If
   
SSCountersUpdate:
   tBit = HasSheets And 4
   If tBit > 0 Then TrackIncNo InvCountFile$, GetInvNo()
   tBit = HasSheets And 128
   If tBit > 0 Then TrackIncNo ConCountFile$, GetConNo()
    
End Sub

Public Function DocumentOwner$()
   DocumentOwner$ = Trim$(UCase$(Left$(Worksheets("Form").Cells(7, 3), 4)))
End Function

Public Function DocumentRefNo()
    DocumentRefNo = GetNO(Worksheets("Form").Cells(7, 3))
End Function

Public Sub writeDefINI()

On Error GoTo ErrHand
   Open INIpath$ For Output As 1
      Print #1, IniTag$ + " = Version " + Trim$(Str$(IniVersion))
      Print #1, ""
      Print #1, "user=guest"
      Print #1, "email=guest@risk.az"
      Print #1, "id=S000"
      Print #1, "lastref=1"
      Print #1, ""
      Print #1, "optOwnership=NO"
      Print #1, "AutoFit=NO"
      Print #1, ""
      Print #1, "AutoSave=YES"
      Print #1, "ASTime=10"
      Print #1, "ASAll=NO"
      Print #1, "ServerCounter=YES"
      Print #1, "QLink=YES"
      Print #1, "Workplace=0"
      Print #1, "wpOverride=YES"
      Print #1, "qlTinMode=0"
      Print #1, "qlTCPIP=0"
      Print #1, "tsTop=00.00"
      Print #1, "tsLeft=00.00"
      Print #1, "tiTop=00.00"
      Print #1, "tiLeft=00.00"
      Print #1, "[END]"
   Close #1
On Error GoTo 0
   ReadINI
   UpdateEnviroment
Exit Sub

ErrHand:
   On Error GoTo 0
   MsgBox "Excel/Windows problem detected!, unable to Create DefINI!", vbExclamation, "(QF) Critical Error!"
   
End Sub


Public Sub FillINIdata()

On Error GoTo ErrHand

NewTry:

With iSET
 .ini_UN.Value = GetIniPar$("user")
 .ini_UE.Value = GetIniPar$("email", True)
 .ini_UID.Value = GetIniPar$("id", True)
 .ini_LR.Value = GetIniPar$("lastref", True)
 If GetIniPar$("optOwnership", True) = "YES" Then .optOwnership.Value = True Else .optOwnership.Value = False
 If GetIniPar$("AutoSave", True) = "YES" Then .ASena.Value = True Else .ASena.Value = False
 .ASvalue.Value = GetIniPar$("ASTime", True)
 .AStime.Value = .ASvalue.Value
 If GetIniPar$("ASAll", True) = "YES" Then .ASall.Value = True Else .ASall.Value = False
 If GetIniPar$("AutoFit", True) = "YES" Then .optAutoFit.Value = True Else .optAutoFit = False
 If GetIniPar$("ServerCounter", True) = "YES" Then .optCounters.Value = True Else .optCounters.Value = False
 If GetIniPar$("optSimp", True) = "YES" Then .optSimp.Value = True Else .optSimp.Value = False
 If GetIniPar$("QLink", True) = "YES" Then .optQlink.Value = True Else .optQlink.Value = False
 .wpl.Selected(Val(GetIniPar$("Workplace", True))) = True
 If GetIniPar$("wpOverride", True) = "YES" Then .wpOverride.Value = True Else .wpOverride.Value = False
 If GetIniPar$("qlTCPIP", True) = "YES" Then .sco1.Value = True Else .sco0.Value = False
 If GetIniPar$("noSplash", True) = "YES" Then .noSplash.Value = True Else .noSplash.Value = False
 If GetIniPar$("noSignCheck", True) = "YES" Then .noSignCheck.Value = True Else .noSignCheck.Value = False
 If GetIniPar$("disVatt", True) = "YES" Then .disVatt.Value = True Else .disVatt.Value = False
 If GetIniPar$("Ready2BUY", True) = "YES" Then .Ready2Buy.Value = True Else .Ready2Buy.Value = False
 If GetIniPar$("BackupHTML", True) = "YES" Then .BackupHTML.Value = True Else .BackupHTML.Value = False
 
 .tsTop.Value = Val(GetIniPar$("tsTop", True))
 .tsLeft.Value = Val(GetIniPar$("tsLeft", True))
 .tiTop.Value = Val(GetIniPar$("tiTop", True))
 .tiLeft.Value = Val(GetIniPar$("tiLeft", True))
 Select Case Val(GetIniPar$("qlTinMode", True))
   Case 0
      .tgo0.Value = True
   Case 1
      .tgo1.Value = True
   Case 2
      .tgo2.Value = True
  End Select

 Select Case GetIniPar$("MailClient", True)
   Case "Express"
      .mail0.Value = True
   Case "Outlook"
      .mail1.Value = True
 End Select



 Select Case Val(GetIniPar$("SimplifyLevel", True))
   Case 0
      .sLevel0.Value = True
   Case 1
      .sLevel1.Value = True
   Case 2
      .sLevel2.Value = True
   Case 3
      .sLevel3.Value = True
  End Select

End With

On Error GoTo 0
Exit Sub

ErrHand:
    On Error GoTo 0
    writeDefINI
    ReadINI
    GoTo NewTry

End Sub

Public Sub UpdateEnviroment()
    If PrevAS = False Then If GetIniPar("AutoSave") = "YES" Then ReScheduleAS
End Sub

Public Sub ReadUsersINI()
    CurQFusers = 0
    Open usersINI$ For Input As #1
    Input #1, tempSTR$
    
    If Trim$(UCase$(tempSTR$)) <> UsersIniTag$ Then
        MsgBox "QFUSERS.INI is Broken", vbCritical + vbOKOnly, "WARNING!"
        Close #1
        Exit Sub
    End If
        
     While Not EOF(1)
      Input #1, tempSTR$
      If Trim$(tempSTR$) = "" Then GoTo nx5
      For x = 1 To 10
          qfusers$(CurQFusers, x) = ParseDivider$(tempSTR$, ":", x)
      Next
      CurQFusers = CurQFusers + 1
nx5: Wend
    Close #1

End Sub

Public Function GetQFemail$(qfID$, qfNAME$) 'qfID$ or qfNAME$
   ReadUsersINI
   For i = 1 To CurQFusers
       If Trim$(UCase$(qfusers$(i, 1))) = Trim$(UCase$(qfID$)) Or Trim$(UCase$(qfusers$(i, 2))) = Trim$(UCase$(qfID$)) Or Trim$(UCase$(qfusers$(i, 4))) = Trim$(UCase$(qfNAME$)) Then
           GetQFemail$ = qfusers$(i, 3): Exit Function
       End If
   Next
End Function

Public Function GetQFid$(qfEMAIL$, qfNAME$) '=or= logic compare
   ReadUsersINI
   For i = 1 To CurQFusers
       If Trim$(UCase$(qfusers$(i, 3))) = Trim$(UCase$(qfEMAIL$)) Or Trim$(UCase$(qfusers$(i, 4))) = Trim$(UCase$(qfNAME$)) Then
           GetQFid$ = Trim$(UCase$(qfusers$(i, 1))): Exit Function
       End If
   Next
End Function

Public Function GetQFidNew$(qfIDold$)
   ReadUsersINI
   For i = 1 To CurQFusers
       If Trim$(UCase$(qfusers$(i, 2))) = Trim$(UCase$(qfIDold$)) Or Trim$(UCase$(qfusers$(i, 1))) = Trim$(UCase$(qfIDold$)) Then
           GetQFidNew$ = qfusers$(i, 1):  Exit Function
       End If
   Next
   GetQFidNew$ = "Z000"
End Function


Public Function GetQFname$(qfID$, qfEMAIL$)
   ReadUsersINI
   For i = 1 To CurQFusers
       If Trim$(UCase$(qfusers$(i, 1))) = Trim$(UCase$(qfID$)) Or Trim$(UCase$(qfusers$(i, 2))) = Trim$(UCase$(qfID$)) Or Trim$(UCase$(qfusers$(i, 3))) = Trim$(UCase$(qfEMAIL$)) Then
           GetQFname$ = qfusers$(i, 4):  Exit Function
       End If
   Next
End Function

Public Sub SignatureMismatchCheck()
    
    ReadUsersINI
    
    Dim SNameFound As Boolean, SEmailFound As Boolean
    Dim SNameX As Integer, SNameY As Integer
    
    SNameFound = False:  SEmailFound = False
    SNameX = 0: SNameY = 0:  SemailX = 0:  SemailY = 0
    
    sRange$ = "a11:g300"
    Worksheets("Form").Activate
        
    For i = 1 To CurQFusers
        
SearchForName: '-----------------------------------------------------------------
        
        If SNameFound = True Then GoTo SearchForEmail
        
        Set c = ActiveSheet.Range(sRange$).Find(What:=qfusers$(i, 4), LookIn:=xlValues, LookAt:= _
        xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, MatchCase:=False)
    
        If c Is Nothing Then
            SNameX = 0
            SNameY = 0
            GoTo SearchForEmail
        End If
   
        SNameX = Val(Mid$(c.Address(RowAbsolute, ColumnAbsolute), 2))
        SNameY = Asc(UCase$(Left$(c.Address(RowAbsolute, ColumnAbsolute), 1))) - 64
        SNameFound = True

SearchForEmail: '-----------------------------------------------------------------

        If SEmailFound = True Then GoTo Nx6
        
        Set c = ActiveSheet.Range(sRange$).Find(What:=qfusers$(i, 3), LookIn:=xlValues, LookAt:= _
        xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, MatchCase:=False)
    
        If c Is Nothing Then
            SemailX = 0
            SemailY = 0
            GoTo Nx6
        End If
   
        SemailX = Val(Mid$(c.Address(RowAbsolute, ColumnAbsolute), 2))
        SemailY = Asc(UCase$(Left$(c.Address(RowAbsolute, ColumnAbsolute), 1))) - 64
Nx6:

        If SEmailFound = True And SNameFound = True Then Exit For
    Next

SearchFinished:

   If SNameFound = False And SEmailFound = False Then
       Exit Sub
   End If

   If SNameFound = False Then GoTo SECHP
   
   UsedQFname$ = Worksheets("form").Cells(SNameX, SNameY).Value
   MachineQFname$ = GetIniPar$("user")
   UN$ = Trim$(UCase$(UsedQFname$))
   mn$ = Trim$(UCase$(MachineQFname$))
   SignedQFID$ = GetQFid$("", UsedQFname$)
   DocumentQFID$ = DocumentOwner$()
   If SignedQFID$ = DocumentQFID$ Then GoTo SECHP
        
   SignedQFNAME$ = GetQFname(DocumentQFID$, "")
   SignedQFEMAIL$ = GetQFemail(DocumentQFID$, "")
      
   msgText$ = "Dear " + MachineQFname$ + "," + Chr$(10) + Chr$(10)
   msgText$ = msgText$ + "The current offer is signed by '" + UsedQFname$ + "'" + Chr$(10)
   msgText$ = msgText$ + "however document's reference ID:" + DocumentQFID$ + " is owned by '" + SignedQFNAME$ + "'" + Chr$(10) + Chr$(10)
   msgText$ = msgText$ + "Would you like to correct Signature & E-mail to" + Chr$(10)
   msgText$ = msgText$ + "'" + SignedQFNAME$ + "' / " + SignedQFEMAIL$ + " ?" + Chr(10)
    
   QnameChange = MsgBox(msgText$, vbExclamation + vbYesNo, "Siganture Mismatch !")
   If QnameChange = 7 Or 0 Then Exit Sub
        
   Range(CellAddr$(SNameX, SNameY, 0, 4)).ClearContents
   DecoCell SNameX, SNameY, SignedQFNAME$, True, True, xlLeft, , True
   DecoCell SNameX + 1, SNameY, SignedQFEMAIL$, True, True, xlLeft, , True
       
'Signature Email changing process ---------------------------------------
SECHP:

End Sub

Public Function GetQFnameT$(qfID$, qTagType As Integer)  '------------------------

'qTagType
'1=  name (en)
'2=  name (ru)
'3=  name (az)
'4=  pos. (en)
'5=  pos. (ru)
'6=  pos. (az)
'10= name & pos. (en)
'20= name & pos. (ru)
'30= name & pos. (az)

   Dim tmp$, tmp2$
   GetQFnameT$ = ""
   ReadUsersINI

   If qTagType < 10 Then
      qTagType = qTagType + 3
      GoSub searchID
      GetQFnameT$ = tmp$
      Exit Function
   End If

   If qTagType >= 10 Then
      qTagType = (qTagType / 10) + 3
      GoSub searchID
      tmp2$ = tmp$
      qTagType = qTagType + 3
      GoSub searchID
      GetQFnameT$ = tmp2$ + " - " + tmp$
   End If
    
Exit Function
    
searchID:
    For i = 1 To CurQFusers
        If Trim$(UCase$(qfusers$(i, 1))) = Trim$(UCase$(qfID$)) Then
            tmp$ = qfusers$(i, qTagType)
            Return
        End If
    Next
Return

End Function

Public Function SubmitterName$(LangMode)
   If LangMode = 1 Or LangMode = 3 Then SubmitterName$ = GetQFname$(DocumentOwner(), ""): Exit Function
   If LangMode = 2 Then SubmitterName$ = GetQFnameT$(DocumentOwner(), 2)
End Function


Public Sub TrackIncNo(docName$, newNo As Integer)
   If GetIniPar$("ServerCounter") <> "YES" Then Exit Sub
   If newNo > TrackGetNo(docName$) Then TrackPutNo docName$, newNo
End Sub

Public Sub TrackPutNo(docName$, newNo As Integer)
If GetIniPar$("ServerCounter") <> "YES" Then Exit Sub
    On Error Resume Next
      Open docName$ For Output As #1
        Print #1, newNo
      Close #1
    On Error GoTo 0
End Sub

Public Function TrackGetNo(docName$) As Integer
   Dim tmp$
   If GetIniPar$("ServerCounter") <> "YES" Then Exit Function
 On Error Resume Next
   Open docName$ For Input As #1
   Input #1, tmp$
   Close #1
 On Error GoTo 0
   TrackGetNo = Val(tmp$)
End Function

Public Sub ChangeDocCounters()
   Dim tmp1$, tmp2$
   tmp1$ = InputBox("Enter Last INVOICE Number:", "(QF) Counters", TrackGetNo(InvCountFile$))
   tmp2$ = InputBox("Enter Last CONTRACT Number:", "(QF) Counters", TrackGetNo(ConCountFile$))
   If tmp1$ <> "" And Val(tmp1$) <> 0 Then TrackPutNo InvCountFile$, Val(tmp1$)
   If tmp2$ <> "" And Val(tmp2$) <> 0 Then TrackPutNo ConCountFile$, Val(tmp2$)
End Sub

Public Sub HotKeysInfo()
   Dim i, mt$
   mt$ = "List of HotKeys available in Q!Forms:" + Chr$(10) + Chr$(10)
   For i = 1 To MaxHotKeys
      If HotKey$(i, 3) <> "" Then mt$ = mt$ + HotKey$(i, 3) + Chr$(10)
   Next
   MsgBox mt$, vbInformation, "(QF) HotKeys Information..."
End Sub
