Attribute VB_Name = "iniaddin"
Option Explicit
'-------------------------------------------------
Global Const QFversion          As Single = 12
Global Const QFbuild            As Integer = 600
Global Const QFcodeName$ = "Facilitator (Beta.1)"
'-------------------------------------------------
Public Const qsGT$ = "=SUM(F"
Public Const qsDIGI$ = "HowMuchIs"
Public Const qsVAT$ = "=ROUND(F"
Public Const qsSUMVAT$ = "=AVERAGE(F"
Public Const qsAREA$ = "F10:F500"
Public Const defNumb$ = "### ### ### ##0.##"
Public Const sdTitle$ = "(QF) Self-Diagnostic."

Public Const fsForm  As Integer = 1
Public Const fsAtt   As Integer = 2
Public Const fsInv   As Integer = 4
Public Const fsTI    As Integer = 8
Public Const fsTS    As Integer = 16
Public Const fsCM    As Integer = 32
Public Const fsDN    As Integer = 64
Public Const fsCON   As Integer = 128
Public Const fsACT   As Integer = 256

Global Const FormulaCol As Integer = 6

Public Const bNum       As Integer = 1
Public Const bText      As Integer = 2
Public Const bFormula   As Integer = 4
Public Const bDate      As Integer = 8
Public Const bFrmErr    As Integer = 64
Public Const bProcErr   As Integer = 128
Public Const bFrmSUM    As Integer = 256
Public Const bFrmDIGI   As Integer = 512
Public Const bFrmVAT    As Integer = 1024
Public Const bFrmSUMVAT As Integer = 2048

'general constants -------------------------------

Global Const MaximumROWS As Integer = 200
Global Const FirstDataRow = 11
Global Const TaxRoundLevel As Integer = 3
Global Const AdaptX As Integer = 9
Global Const AdaptY As Integer = 5
Global Const CalbTool As Integer = 11
Global Const AliveTime As Integer = 7

Global Const SysRoot$ = "C:\QFORMS"
Global Const ASaveRoot$ = "\RESCUE"
Global Const CacheRoot$ = "\CACHE"
Global Const QFMainPath$ = "C:\QFORMS\"
Global Const QFTemplatesPath$ = "C:\QFORMS\QMODS\"
Global Const QFTempPath$ = "C:\QFORMS\_temp\"
Global Const QFHTMLpath$ = "C:\QFORMS\_html_offers\"
Global Const usersINI$ = "C:\QFORMS\qfusers.ini"
Global Const msgFile$ = "C:\QFORMS\qfMsg.ini"
Global Const MainServer$ = "\\storage\QForms\"
Global Const DesignPath$ = "design\"
Global Const AltServer$ = "\\SQL\QForms\"
Global Const InvCountFile$ = "\\storage\qforms\qtrack\inv.ini"
Global Const ConCountFile$ = "\\storage\qforms\qtrack\contr.ini"

Global Const stickFile$ = ".xlt"
Global Const qfcon$ = "qfcon.xla"
Global Const qfais$ = "qfcon_ais.xla"
Global Const qfweb$ = "qfweb.xla"
Global Const qftem$ = "qf_template.xls"
Global Const INIpath$ = "C:\QFORMS\qfaddins.ini"

'contracts ---------
Global Const MaxParLines = 50
Global WizardCanceled As Boolean
Global LPVstart$
Global LPVend$
Global WizardSteps As Integer
Global CurrentWizardStep As Integer
Global ParLines$(MaxParLines, 10)
Global ConCase$(MaxParLines, 10)
Global TotalParLines As Integer

'system variables (ini-addin)-----------
Global Const MaxHotKeys As Integer = 14
Global SystemLocked As Boolean
Global HotKey$(MaxHotKeys, 3)
Global dwSign$(1 To 3)
Global InvSuffix$(3, 3)
Global QFCmodName$
Global pgName$(1 To 3)
Global CM$(10, 1 To 3)
Global DN$(10, 1 To 3)
Global AttTITLE$(1 To 3)
Global AttSubTITLE$(1 To 3)
Global ConTemplate$(3)
Global tot$(1 To 3)
Global stot$(1 To 3)
Global iSIGN$(1 To 2, 1 To 3, 0 To 2) ' id, lang, workplace
Global iPOS$(1 To 2, 1 To 3, 0 To 2) ' id, lang, workplace
Global xFNT$(4)
Global xcFNT$(1 To 3)
Global aCO$(1 To 3)
Global aiCO$(1 To 3)
Global coSign$(1 To 3)
Global aHEAD$(5, 1 To 3)
Global wiHEAD$(4, 1 To 3)
Global wHEAD$(4, 1 To 3)
Global ConvPay$(1 To 3)
Global Const MaxBrds As Integer = 6
Global xlBrd(MaxBrds) As Long
Global WkPlace$(0 To 2)
Global TotalGroups As Integer
Global Months$(1 To 12, 1 To 3, 1 To 2) ' month, LangMode, SizeMode

' Calc -------------
Public CalcCancel       As Boolean
Public CalcNoComments   As Boolean
Public CalcRoundMode    As Integer '1= no changes, 2=round, 3=round (.XX)
Public CalcOpMode       As Integer
Public CalcValue$
Public CalcCmt$(2)
Public CalcEuro         As Boolean
Public CalcAzmEuro      As Boolean

' Miscellaneous ------
Global AddGroupLoc  As Integer
Global EditBoxLocX  As Integer
Global EditBoxLocY  As Integer

'ServMode ------------
Public SRstack  As Integer
Public SavedRange$(100)
Public LastSched

'SetMode -------------
Public Const UsersIniTag$ = "[QFUSERS.INI]"
Public Const MaxQFusers As Integer = 200
Public Const IniTag$ = "[QFADDINSINI]"
Public Const IniVersion  As Single = 2.5
Public Const maxINI As Integer = 200
Global PrevAS As Boolean
Global INILines$(maxINI, 3) ' 1=Variable / '2=Parametr / '3=Status
Global CurQFusers As Integer
Global qfusers$(MaxQFusers, 10) ' ID, e-mail, name, tag

'Databases & Logic ---------
Public Const maxUniques As Integer = 1000
Public UniRowData$(maxUniques)
Public UniRowIndex(maxUniques) As Integer
Public UniScore(maxUniques) As Integer
Public TotalUni As Integer


Public Sub InitVariables()

dwSign$(1) = "E"
dwSign$(2) = "R"
dwSign$(3) = "A"

QFCmodName$ = "QFcon.xla"

InvSuffix$(1, 1) = "18% VAT"
InvSuffix$(2, 1) = "18% ���"
InvSuffix$(3, 1) = "18% ���"

InvSuffix$(1, 2) = "TOTAL, to be paid"
InvSuffix$(2, 2) = "����� � ������"
InvSuffix$(3, 2) = "�EK�N�"

CM$(1, 1) = "CASH MEMO"
CM$(1, 2) = "����������� �� ������"
CM$(1, 3) = "CASH MEMO"

DN$(1, 1) = "DELIVERY NOTE"
DN$(1, 2) = "����������� � ��������"
DN$(1, 3) = "DELIVERY NOTE"

DN$(2, 1) = "delivered."
DN$(2, 2) = "�����������"
DN$(2, 3) = "delivered."

DN$(3, 1) = "Note."
DN$(3, 2) = "����������"
DN$(3, 3) = "note."

AttTITLE$(1) = "ATTACHMENT #1 "
AttTITLE$(2) = "���������� N:1 "
AttTITLE$(3) = "1 No. - �� �����"

AttSubTITLE$(1) = "to the Contract # "
AttSubTITLE$(2) = "� �������� N: "
AttSubTITLE$(3) = " No.-�� ���������� ������"

ConTemplate$(1) = "con_en"
ConTemplate$(2) = "con_ru"
ConTemplate$(3) = "con_az"

xFNT$(1) = "Arial"
xFNT$(2) = "Vanta Light"
xFNT$(3) = "Az Times"
xFNT$(4) = "AZ_OLD"

xcFNT$(1) = "Arial"
xcFNT$(2) = "Arial"
xcFNT$(3) = "Az Times"

tot$(1) = "TOTAL ("
tot$(2) = "����� ("
tot$(3) = "���� ("
    
stot$(1) = "Sub Total:"
stot$(2) = "�����:"
stot$(3) = "����:"

aCO$(1) = "COMMERCIAL OFFER"
aCO$(2) = "������������ �����������"
aCO$(3) = "Q�YM�TL�R T�KL�F�"

aiCO$(1) = "I N V O I C E"
aiCO$(2) = "����-�������"
aiCO$(3) = "����� - �������"

coSign$(1) = "R.I.S.K. Company"
coSign$(2) = "��� 'R.I.S.K.'"
coSign$(3) = "''R.I.S.K.' E�F"

ConvPay$(1) = "Payment should be done in AZM according to the exchange rate at the NBA on the date of payment."
ConvPay$(2) = "������ ������ ���� ����������� � ������� �� ����� ��� �� ���� ������."
ConvPay$(3) = "������ ���������� ��������������� ��������� ����� ������ ������ ������ ����  ��� ������� ����������� ������ �������������."

aHEAD$(1, 1) = "  Description"
aHEAD$(2, 1) = "Qty."
aHEAD$(3, 1) = "Unit Price"
aHEAD$(4, 1) = "Total Price"
aHEAD$(5, 1) = "Delivery (days)"
aHEAD$(1, 2) = "  ������������ / ��������"
aHEAD$(2, 2) = "���-��"
aHEAD$(3, 2) = "����"
aHEAD$(4, 2) = "�����"
aHEAD$(5, 2) = "�������� (����)"
aHEAD$(1, 3) = "  ���"
aHEAD$(2, 3) = "S���"
aHEAD$(3, 3) = "Qiym�ti"
aHEAD$(4, 3) = "������"
aHEAD$(5, 3) = "T�h��� �������"

wHEAD$(1, 1) = "TO:"
wHEAD$(2, 1) = "ATTN:"
wHEAD$(3, 1) = "DATE:"
wHEAD$(1, 2) = "����:"
wHEAD$(2, 2) = "����:"
wHEAD$(3, 2) = "����:"
wHEAD$(1, 3) = "hara:"
wHEAD$(2, 3) = "�im�:"
wHEAD$(3, 3) = "Tarix:"

wiHEAD$(1, 1) = "Payee:"
wiHEAD$(2, 1) = "Account #"
wiHEAD$(3, 1) = "Corresp.A/C"
wiHEAD$(4, 1) = "Code:"
wiHEAD$(1, 2) = "����������:"
wiHEAD$(2, 2) = "���� #"
wiHEAD$(3, 2) = "���. ���� #"
wiHEAD$(4, 2) = "���:"
wiHEAD$(1, 3) = "������:"
wiHEAD$(2, 3) = "����� #"
wiHEAD$(3, 3) = "�.�.:"
wiHEAD$(4, 3) = "���:"

iSIGN$(1, 1, 0) = "Anar Aligioulov"
iSIGN$(2, 1, 0) = "Dilara Babayeva"
iSIGN$(1, 2, 0) = "���� ��������"
iSIGN$(2, 2, 0) = "������ �������"
iSIGN$(1, 3, 0) = "Anar ��������"
iSIGN$(2, 3, 0) = "Dilar� Babayeva"

iSIGN$(1, 1, 1) = "Ulvi Aligioulov"
iSIGN$(2, 1, 1) = "Fakhri Mamedov"
iSIGN$(1, 2, 1) = "����� ��������"
iSIGN$(2, 2, 1) = "����� �������"
iSIGN$(1, 3, 1) = "�������� �.�."
iSIGN$(2, 3, 1) = "�������� �.�."

iSIGN$(1, 1, 2) = "Anar Aligioulov"
iSIGN$(2, 1, 2) = "Dilara Babayeva"
iSIGN$(1, 2, 2) = "���� ��������"
iSIGN$(2, 2, 2) = "������ �������"
iSIGN$(1, 3, 2) = "Anar ��������"
iSIGN$(2, 3, 2) = "Dilar� Babayeva"

iPOS$(1, 1, 0) = "Director"
iPOS$(2, 1, 0) = "Chief Accountant"
iPOS$(1, 2, 0) = "��������"
iPOS$(2, 2, 0) = "������� ���������"
iPOS$(1, 3, 0) = "��������"
iPOS$(2, 3, 0) = "��� �������"

iPOS$(1, 1, 1) = "Service Center Manager"
iPOS$(2, 1, 1) = "Financial Executive Officer"
iPOS$(1, 2, 1) = "����������� ������-�������"
iPOS$(2, 2, 1) = "���.������������ �� ���������� �����"
iPOS$(1, 3, 1) = "������ ���������� ������"
iPOS$(2, 3, 1) = "������ ���������� ��������� ������� ������ ���� �������"

iPOS$(1, 1, 2) = "Director"
iPOS$(2, 1, 2) = "Chief Accountant"
iPOS$(1, 2, 2) = "��������"
iPOS$(2, 2, 2) = "������� ���������"
iPOS$(1, 3, 2) = "��������"
iPOS$(2, 3, 2) = "��� �������"

pgName$(1) = "Page "
pgName$(2) = "���. "
pgName$(3) = ""

'---- hot keys -------
HotKey$(1, 1) = "%{INSERT}"
HotKey$(1, 2) = "addSingleST"
HotKey$(1, 3) = "ALT+INSERT = Add Subtotal to the selected Group"
HotKey$(2, 1) = "%{DELETE}"
HotKey$(2, 2) = "removeSingleST"
HotKey$(2, 3) = "ALT+DELETE = Remove Subtotal from the selected Group"
HotKey$(3, 1) = "{F10}"
HotKey$(3, 2) = "qfINIT"
HotKey$(3, 3) = "F10 = Re-Initialize QF"
HotKey$(4, 1) = "^%{F10}"
HotKey$(4, 2) = "ChangeDocCounters"
HotKey$(7, 1) = "^%n"
HotKey$(7, 2) = "CreateBlankTemplateSilent"
HotKey$(7, 3) = "CTRL+ALT+N = Open blank QF Template"
HotKey$(8, 1) = "^%i"
HotKey$(8, 2) = "DoAddGroup"
HotKey$(8, 3) = "CTRL+ALT+I = Insert Group"
HotKey$(9, 1) = "^%v"
HotKey$(9, 2) = "VFY"
HotKey$(9, 3) = "CTRL+ALT+V = Verify & Tune-up"
HotKey$(10, 1) = "^%s"
HotKey$(10, 2) = "SaveCurrent"
HotKey$(10, 3) = "CTRL+ALT+S = Save."
HotKey$(11, 1) = "%z"
HotKey$(11, 2) = "RollBack"
HotKey$(11, 3) = "ALT+Z = Roll Back Calculations"
HotKey$(12, 1) = "%v"
HotKey$(12, 2) = "PasteSp"
HotKey$(12, 3) = "ALT+V = Paste Special - Values Only"
HotKey$(13, 1) = "%c"
HotKey$(13, 2) = "CopySp"
HotKey$(13, 3) = "ALT+C = Copy (Equal to CTRL+C)"
HotKey$(14, 1) = "%b"
HotKey$(14, 2) = "OpenEditBox"
HotKey$(14, 3) = "ALT+B = Edit Box."

xlBrd(1) = xlEdgeLeft
xlBrd(2) = xlEdgeRight
xlBrd(3) = xlInsideVertical
xlBrd(4) = xlInsideHorizontal
xlBrd(5) = xlEdgeBottom
xlBrd(6) = xlEdgeTop

WkPlace$(0) = "Sales / Project"
WkPlace$(1) = "Service"
WkPlace$(2) = "Accounting"

Months$(1, 1, 1) = "January":   Months$(1, 1, 2) = "Jan."
Months$(2, 1, 1) = "February":  Months$(2, 1, 2) = "Feb."
Months$(3, 1, 1) = "March":     Months$(3, 1, 2) = "Mar."
Months$(4, 1, 1) = "April":     Months$(4, 1, 2) = "Apr."
Months$(5, 1, 1) = "May":       Months$(5, 1, 2) = "May"
Months$(6, 1, 1) = "June":      Months$(6, 1, 2) = "June"
Months$(7, 1, 1) = "July":      Months$(7, 1, 2) = "July"
Months$(8, 1, 1) = "August":    Months$(8, 1, 2) = "Aug."
Months$(9, 1, 1) = "September": Months$(9, 1, 2) = "Sep."
Months$(10, 1, 1) = "October":  Months$(10, 1, 2) = "Oct."
Months$(11, 1, 1) = "November": Months$(11, 1, 2) = "Nov."
Months$(12, 1, 1) = "December": Months$(12, 1, 2) = "Dec."

Months$(1, 2, 1) = "������":    Months$(1, 2, 2) = "���."
Months$(2, 2, 1) = "�������":   Months$(2, 2, 2) = "���."
Months$(3, 2, 1) = "�����":     Months$(3, 2, 2) = "���."
Months$(4, 2, 1) = "������":    Months$(4, 2, 2) = "���."
Months$(5, 2, 1) = "���":       Months$(5, 2, 2) = "���."
Months$(6, 2, 1) = "����":      Months$(6, 2, 2) = "���."
Months$(7, 2, 1) = "����":      Months$(7, 2, 2) = "���."
Months$(8, 2, 1) = "�������":   Months$(8, 2, 2) = "���."
Months$(9, 2, 1) = "��������":  Months$(9, 2, 2) = "���."
Months$(10, 2, 1) = "�������":  Months$(10, 2, 2) = "���."
Months$(11, 2, 1) = "������":   Months$(11, 2, 2) = "���."
Months$(12, 2, 1) = "�������":  Months$(12, 2, 2) = "���."

Months$(1, 3, 1) = "Yanvar":    Months$(1, 3, 2) = "Yan."
Months$(2, 3, 1) = "Fevral":    Months$(2, 3, 2) = "Fev."
Months$(3, 3, 1) = "Mart":      Months$(3, 3, 2) = "Mar."
Months$(4, 3, 1) = "Aprel":     Months$(4, 3, 2) = "Apr."
Months$(5, 3, 1) = "May":       Months$(5, 3, 2) = "May."
Months$(6, 3, 1) = "�yun":      Months$(6, 3, 2) = "�yn."
Months$(7, 3, 1) = "�yul":      Months$(7, 3, 2) = "�yl."
Months$(8, 3, 1) = "Avgust":    Months$(8, 3, 2) = "Avg."
Months$(9, 3, 1) = "Sentyabr":  Months$(9, 3, 2) = "Sen."
Months$(10, 3, 1) = "Oktyabr":  Months$(10, 3, 2) = "Okt."
Months$(11, 3, 1) = "Noyabr":   Months$(11, 3, 2) = "Noy."
Months$(12, 3, 1) = "Dekabr":   Months$(12, 3, 2) = "Dek."

End Sub
