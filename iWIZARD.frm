VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} iWIZARD 
   Caption         =   "Contract Wizard"
   ClientHeight    =   4995
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6060
   OleObjectBlob   =   "iWIZARD.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "iWIZARD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub c1_Change()
    CaseChanged
End Sub

Private Sub c2_Change()
    CaseChanged
End Sub

Private Sub c3_Change()
    CaseChanged
End Sub

Private Sub CB_Cancel_Click()
    WizardCanceled = True
    modContracts.WizardCancel
End Sub

Private Sub CB_Next_Click()
    modContracts.WizardNext
End Sub

Private Sub CB_Prev_Click()
    modContracts.WizardPrev
End Sub

Private Sub cOriginal_Change()
    CaseChanged
End Sub

Private Sub eLPV_Click()
    Select Case eLPV.Value
        Case True
            iWIZARD.Height = 268.5
            InitLPV
            UpdateLPV
        Case False
            iWIZARD.Height = 181.5
    End Select
    MyAnswer.SetFocus
    
End Sub

Private Sub MyAnswer_Change()
    UpdateLPV
End Sub

Private Sub MyAnswer_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
   If KeyCode = 13 Or KeyCode = 10 Then modContracts.WizardNext
End Sub

Private Sub UserForm_Activate()
FixProc "iWIZARD.Activate" '##-##'
    Application.ScreenUpdating = True
    LangMode = CurrentLang()
    MyAnswer.Font.Name = xFNT$(LangMode)
    LPV.Font.Name = xFNT$(LangMode)
    eLPV.Value = False
    iWIZARD.Height = 181.5
    eLPV.Value = True
    DoEvents
End Sub

Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)
    If CloseMode <> 1 Then Cancel = 1
End Sub
