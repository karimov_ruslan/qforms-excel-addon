VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "EClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Option Compare Text
Public WithEvents App As Application
Attribute App.VB_VarHelpID = -1

Private Sub App_WorkbookActivate(ByVal Wb As Workbook)
   If IsQF(Wb.Name) = True Then
      Application.Caption = "Q!F"
   Else
      Application.Caption = "Excel"
   End If
End Sub

Private Sub App_WorkbookBeforeClose(ByVal Wb As Workbook, Cancel As Boolean)
   OffLoadTemplates
End Sub

Private Sub App_WorkbookBeforeSave(ByVal Wb As Workbook, ByVal SaveAsUI As Boolean, Cancel As Boolean)
   Dim HasSheets As Integer, tBit As Integer
   If Wb.Name = qftem$ And SaveAsUI = False Then
      Cancel = True
      MsgBox "You can't Save QF Template !", vbExclamation, "(QF) Warning !"
      Exit Sub
   End If
   HasSheets = SheetBits(Wb.Name)
   tBit = HasSheets And fsForm
   If tBit > 0 Then AfterSave True
End Sub

Private Sub App_WorkbookOpen(ByVal Wb As Workbook)
  
   Dim shp, appVer As Single
   If Wb.Name = qftem$ Then migInit: Exit Sub
   If IsQF(Wb.Name) = False Then Exit Sub
  
  SaveLocation True
   Worksheets("Form").Activate
   appVer = Val(Application.Version)
   If Int(appVer) = 10 Then
      ActiveSheet.PageSetup.PrintArea = ""
   Else
      ActiveSheet.PageSetup.PrintArea = "$A:$G"
   End If
   migInit True
   For Each shp In Worksheets("form").Shapes
      If shp.Name = "Text Box 1" Then
         ActiveSheet.Name = "_form_"
         Range("A1").Select
         RestLocation True
         Exit Sub
      End If
   Next
   VFY True
   RestLocation True
End Sub

