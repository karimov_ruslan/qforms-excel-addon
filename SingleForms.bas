Attribute VB_Name = "SingleForms"
Option Compare Text

Public Sub InsertCashMemo()

   SaveLocation True
   RenewSheet "CashMemo"
    
   With Worksheets("CashMemo")
       .Activate
       .Columns("G:G").EntireColumn.Hidden = True
       .Columns("F:F").EntireColumn.Hidden = False
       .Columns("D:D").EntireColumn.Hidden = False
       .Columns("E:E").ColumnWidth = 13.14
       .Columns("F:F").ColumnWidth = 17.43
   End With
    
   Range("A2:C3").ClearContents
   uLine "A3:C3", 0
   AddLogo
   
   Cells(8, 2) = ""
   Cells(7, 2) = "No.:"
   Cells(7, 3).Font.Bold = True
   
   Rows("8:8").EntireRow.AutoFit
   Rows("5:5").EntireRow.Hidden = True
   
   LangMode = CurrentLang()
   If LangMode = 2 Then tmpSize = 15 Else tmpSize = 18
   Cells(7, 3) = "CM~" + Worksheets("Form").Cells(7, 3)
   DecoCell 1, 3, CM$(1, LangMode), True, False, xlLeft
   Cells(1, 3).Font.Size = tmpSize
      
   Set shps = Worksheets("CashMemo").Shapes
   For Each shp In shps
       If shp.Top > 200 Then shp.Delete
   Next
    
ChangePAIDwords: '---------------------------------

   Dim GTrow As Integer, GTcol As Integer, CurrMode As Integer

   CurrMode = CurrentCurrency()
   GTrow = FormulaRow(qsGT$, "CashMemo")
   GTcol = FormulaCol
   If GTrow = 0 Then GoTo Finalize
   DecoCell GTrow, 4, "TOTAL PAID (" + crSign$(CurrMode, 1) + "):", True, , xlRight
   If GetVATsubMode() > 0 Then
      DecoCell GTrow + 1, 4, "18% VAT (" + crSign$(CurrMode, 1) + "):", True, , xlRight
      DecoCell GTrow + 2, 4, "GRAND TOTAL PAID (" + crSign$(CurrMode, 1) + "):", True, , xlRight
   End If

Finalize: '----------------------------------------
    
   DoPageSetup
   RestLocation True

End Sub

Public Sub InsertDeliveryNote()
   
   Dim TheEnd As Integer, tmpSize As Integer
   SaveLocation True
   RenewSheet "DeliveryNote"
   
   TheEnd = FormulaRow(qsGT$)
   LangMode = CurrentLang()

   With Worksheets("DeliveryNote")
       .Activate
       .Columns("G:G").EntireColumn.Hidden = False
       .Columns("F:F").EntireColumn.Hidden = True
       .Columns("D:D").EntireColumn.Hidden = False
       .Columns("E:E").EntireColumn.Hidden = True
       .Columns("G:G").ColumnWidth = 24
       .Columns("C:C").ColumnWidth = 60
       .Cells(8, 2) = ""
       .Cells(7, 2) = "No.:"
       .Cells(7, 3).Font.Bold = True
   End With
   
   Range("A2:C3").ClearContents
   uLine "A3:C3", 0
   AddLogo

   DecoCell 9, 7, DN$(3, LangMode), , , xlCenter
    
   Rows("8:8").EntireRow.AutoFit
   Rows("5:5").EntireRow.Hidden = True
        
   If LangMode = 2 Then tmpSize = 15 Else tmpSize = 18
   Cells(7, 3) = "DN~" + Worksheets("Form").Cells(7, 3)
   DecoCell 1, 3, DN$(1, LangMode), True, False, xlLeft, , , , , , tmpSize
   
   For i = FirstDataRow To TheEnd - 2
      Cells(i, 7) = DN$(2, LangMode)
      Cells(i, 7).Font.Name = xFNT$(LangMode)
   Next
    
   DeleteRange$ = RowsAddr$(TheEnd, 100)
   Rows(DeleteRange$).Select
   Selection.Delete Shift:=xlUp
     
   If LangMode = 2 Then fFlag = 1: fsz = 9 Else fsz = 10: fFlag = 0
   DecoCell TheEnd + 2, 3, GetIniPar$("user") + "   on behalf of   R.I.S.K. Company", False, True, xlLeft
   DecoCell TheEnd + 6, 3, Worksheets("form").Cells(5, 3) + "   on behalf of   " + Worksheets("form").Cells(4, 3), False, True, xlLeft
   uLine CellAddr$(TheEnd + 2, 3), 1
   uLine CellAddr$(TheEnd + 2, 7), 1
   uLine CellAddr$(TheEnd + 6, 3), 1
   uLine CellAddr$(TheEnd + 6, 7), 1
    
   DoPageSetup
   RestLocation True
  
End Sub

Private Sub RenewSheet(shName$)
   
   Dim ws
   For Each ws In ActiveWorkbook.Worksheets
      If ws.Name = shName$ Then
         Application.DisplayAlerts = False
         Worksheets(shName$).Delete
         Application.DisplayAlerts = True
      End If
   Next
    
   Worksheets("Form").Activate
   Cells.Select
   Selection.Copy
   Sheets.Add After:=Worksheets(Worksheets.Count)
   ActiveSheet.Name = shName$
   ActiveSheet.Paste
   ActiveWindow.DisplayGridlines = False
   ActiveSheet.Range("A1").Select
   RemoveGroups shName$
   Worksheets(shName$).Columns("J:O").EntireColumn.Hidden = True

End Sub
