Attribute VB_Name = "qMigrate"
Option Compare Text
Option Explicit

Public Sub migInit(Optional DontSaveLoc As Boolean)
 If DontSaveLoc = False Then SaveLocation True
  RepairLinks
  MigTemplate
   On Error Resume Next
      Worksheets("form").Columns("J:O").EntireColumn.Hidden = True
   On Error GoTo 0
 If DontSaveLoc = False Then RestLocation True
    
End Sub




'Public Sub migBuildProps(qTemplateVer As Integer)
'
'On Error Resume Next
'   With ActiveWorkbook.CustomDocumentProperties
'         .Add Name:="qMode", _
'            LinkToContent:=False, _
'            Type:=msoPropertyTypeNumber, _
'            Value:=AdaptGetMode()
'         .Add Name:="qLang", _
'            LinkToContent:=False, _
'            Type:=msoPropertyTypeNumber, _
'            Value:=AdaptCurrLang()
'         .Add Name:="qCurr", _
'           LinkToContent:=False, _
'            Type:=msoPropertyTypeNumber, _
'            Value:=AdaptCurrCurr()
'         .Add Name:="qVAT", _
'            LinkToContent:=False, _
'            Type:=msoPropertyTypeNumber, _
'            Value:=AdaptCurrVAT()
'         .Add Name:="qVersion", _
'            LinkToContent:=False, _
'            Type:=msoPropertyTypeNumber, _
'            Value:=qTemplateVer
'   End With
'On Error GoTo 0
'End Sub

Private Sub RepairLinks()
   Dim aLinks, i As Integer, newLink$
On Error GoTo ex
   aLinks = ActiveWorkbook.LinkSources(xlExcelLinks)
   If Not IsEmpty(aLinks) Then
      For i = 1 To UBound(aLinks)
        If InStr(1, aLinks(i), "qf") > 0 Then
           newLink$ = Application.VBE.VBProjects("QFADDINS").filename
           ActiveWorkbook.ChangeLink Name:=aLinks(i), newName:=newLink$, Type:=xlExcelLinks
        End If
      Next i
   End If
ex:
   On Error GoTo 0
End Sub


Public Sub MigTemplate()

   Dim templVer As Integer

   templVer = ActiveWorkbook.CustomDocumentProperties("qVersion").Value
   If templVer >= 3 Then Exit Sub

DoRebuild: '-----------------------------------------------------------------
   
   ActiveWorkbook.CustomDocumentProperties("qVersion").Value = 3
   
   RemoveOldLogo
   CorrectMainForm
   AddLogo
  
   If GetMode() = 3 Then
      ReconstractContractForm
      Cells(2, 3) = Cells(7, 6)
      Cells(3, 3) = Cells(6, 6)
      Range("E6:F7").Interior.ColorIndex = xlNone
      Range("E6:F7").ClearContents
   End If


End Sub

Public Sub AddLogo()

   Range("A1").Select
   
   ActiveSheet.Pictures.Insert(MainServer$ + DesignPath$ + "rlogo.gif").Select
   Selection.ShapeRange.ScaleWidth 0.33, msoFalse, msoScaleFromTopLeft
   Selection.ShapeRange.ScaleHeight 0.33, msoFalse, msoScaleFromTopLeft
   Selection.ShapeRange.IncrementLeft 312
   Selection.Name = "Picture 100"
   Selection.Placement = xlFreeFloating

   Range("A1").Select

   ActiveSheet.Shapes.AddTextbox(msoTextOrientationHorizontal, 318#, 54#, 190#, 40#).Select
   
   With Selection.ShapeRange
      .Fill.Visible = msoFalse
      .Fill.Transparency = 0#
      .Line.Transparency = 0#
      .Line.Visible = msoFalse
   End With
   With Selection
      .Name = "Text Box 100"
      .Placement = xlFreeFloating
      .PrintObject = True
      .Characters.Text = "24 S.Vurgun st. Baku - 370000, Azerbaijan" & Chr(10) & "tel: +(99412) 973737, fax: +(99412) 981993" & Chr(10) & "http://www.risk.az"
   End With
   With Selection.Font
      .Name = "Arial"
      .FontStyle = "Normal"
      .Size = 10
      .ColorIndex = xlAutomatic
   End With

   Range("A1").Select

End Sub

Public Sub RemoveOldLogo()
    ActiveSheet.Shapes("Picture 21").Select
    Selection.Delete
    ActiveSheet.Shapes("Text Box 5").Select
    Selection.Delete
End Sub

Public Sub CorrectMainForm()

    Rows("2:3").RowHeight = 12.75
    Rows("2:3").Select
    With Selection.Font
        .Name = "Arial"
        .ColorIndex = xlAutomatic
        .Size = 10
    End With

End Sub
