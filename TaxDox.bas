Attribute VB_Name = "TaxDox"
Option Explicit

Public Sub DoRenewAll()
   DoTaxSheet
   DoTaxInvoice
   TaxCrossCheck
End Sub

Public Sub InsertTaxInvoice()
   AddDocFromTemplate "TaxInv", "TaxInv"
End Sub

Public Sub LinkTaxInvoice(ConvRate, TaxMode As Integer)

    Dim destSheet$, SyncDATA$, SyncFORMULA$
    Dim SUMxROW, SUMxCOL, SyncConvDATA
    Dim dtLEN, SourceRow, i
    Dim rBegin As Integer, rEnd As Integer, rLines As Integer, xDP
    Dim totFORMULA$, selRNG$
    Dim decFont$, II$, IssuedBy$
    Dim vatFormula$, dgwRANGE$, dgwFORMULA$, dgwOLrange$
    Dim SumFrmPos As Integer, pOffset As Integer, nbLine As Integer
    Dim NewGTrange$, NewGTformula$
    
destSheet$ = RealShName$("TaxInv")

   Application.ScreenUpdating = False

   Worksheets("Form").Activate
     
   rBegin = 13
   rEnd = GetLastRow()
   rLines = rEnd - FirstDataRow + 1

'--- Link Styles ---------------
    
   Worksheets(destSheet$).Activate
   selRNG$ = "B" + Trim$(Str$(rBegin)) + ":O" + Trim$(Str$(rEnd + 3))
   Worksheets(destSheet$).Activate
   Range(selRNG$).Select
   With Selection
       .WrapText = True
       .VerticalAlignment = xlTop
       .Font.Name = "Times New Roman"
       .Font.Size = 9
   End With
    
   Rows(RowsAddr$(rBegin, rEnd + 3)).EntireRow.AutoFit
   AddGrid xlHairline, True
   Range("A1").Select
   Rows("1:1").RowHeight = 70

'Sync data
        
    Dim bl$
    bl$ = "'-----"
       
    SourceRow = FirstDataRow - 1
    
  With Worksheets(destSheet$)
    For i = 1 To rLines
        SyncDATA$ = Worksheets("Form").Cells(SourceRow + i, 2).Value
        .Cells(rBegin - 1 + i, 2).Value = SyncDATA$
        SyncDATA$ = Worksheets("Form").Cells(SourceRow + i, 3).Value
        .Cells(rBegin - 1 + i, 3).Value = SyncDATA$
        SyncDATA$ = Worksheets("Form").Cells(SourceRow + i, 4).Value
        .Cells(rBegin - 1 + i, 5).Value = SyncDATA$
        .Cells(rBegin - 1 + i, 6).Value = Round(Worksheets("Form").Cells(SourceRow + i, 5).Value * ConvRate, 0)
        SyncFORMULA$ = "=E" + Trim$(Str$(rBegin - 1 + i)) + "*F" + Trim$(Str$(rBegin - 1 + i)) + "/1000"
        .Cells(rBegin - 1 + i, 7).Formula = SyncFORMULA$
        .Cells(rBegin - 1 + i, 8).Value = bl$
        .Cells(rBegin - 1 + i, 9).Value = bl$
        
        SyncFORMULA$ = "=G" + Trim$(Str$(rBegin - 1 + i))
        .Cells(rBegin - 1 + i, 10).Formula = SyncFORMULA$
        
        If TaxMode = 1 Then
            SyncFORMULA$ = "=J" + Trim$(Str$(rBegin - 1 + i))
            .Cells(rBegin - 1 + i, 11).Formula = SyncFORMULA$
            .Cells(rBegin - 1 + i, 12).Value = bl$
            .Cells(rBegin - 1 + i, 13).Value = bl$
            SyncFORMULA$ = "=ROUND(G" + Trim$(Str$(rBegin - 1 + i)) + "/100*18" + "," + Trim$(Str$(TaxRoundLevel)) + ")"
            .Cells(rBegin - 1 + i, 14).Formula = SyncFORMULA$
            SyncFORMULA$ = "=K" + Trim$(Str$(rBegin - 1 + i)) + "+N" + Trim$(Str$(rBegin - 1 + i))
            .Cells(rBegin - 1 + i, 15).Formula = SyncFORMULA$
            GoTo nx1
        End If
        
        If TaxMode = 2 Then
            SyncFORMULA$ = "=J" + Trim$(Str$(rBegin - 1 + i))
            .Cells(rBegin - 1 + i, 12).Formula = SyncFORMULA$
            .Cells(rBegin - 1 + i, 11).Value = bl$
            .Cells(rBegin - 1 + i, 13).Value = bl$
            .Cells(rBegin - 1 + i, 14).Value = "'0"
            SyncFORMULA$ = "=J" + Trim$(Str$(rBegin - 1 + i))
            .Cells(rBegin - 1 + i, 15).Formula = SyncFORMULA$
            GoTo nx1
        End If
        
        If TaxMode = 3 Then
            SyncFORMULA$ = "=J" + Trim$(Str$(rBegin - 1 + i))
            .Cells(rBegin - 1 + i, 13).Formula = SyncFORMULA$
            .Cells(rBegin - 1 + i, 11).Value = bl$
            .Cells(rBegin - 1 + i, 12).Value = bl$
            .Cells(rBegin - 1 + i, 14).Value = "'0"
            SyncFORMULA$ = "=J" + Trim$(Str$(rBegin - 1 + i))
            .Cells(rBegin - 1 + i, 15).Formula = SyncFORMULA$
            GoTo nx1
        End If
nx1:
    Next
  End With
        
    Dim bLine As Integer
    decFont$ = "AZ_OLD"
    II$ = Chr$(34)

' fill empty rows ---------------------------------
    
    Dim RowAbsolute, ColumnAbsolute, pbLoc As Integer
    Dim pbLocRng$
    
    bLine = rBegin + rLines

    Worksheets(destSheet$).Activate
    Worksheets(destSheet$).PageSetup.PrintArea = "$A$1:$O$100"
    ActiveWindow.View = xlPageBreakPreview
    pbLocRng$ = ActiveSheet.HPageBreaks(1).Location.Address(RowAbsolute, ColumnAbsolute)
    ActiveWindow.View = xlNormalView
    Worksheets(destSheet$).PageSetup.PrintArea = ""
    pbLoc = Val(Mid$(pbLocRng$, 2))
    
    If pbLoc - bLine < 4 Or bLine >= pbLoc Then pOffset = -1: GoTo DoNx
    pOffset = pbLoc - bLine - 3
            
    Rows(RowsAddr$(bLine, bLine + pOffset)).Select
    Selection.Insert Shift:=xlDown
    Range("a1").Select

    Dim FR, SR, NFR
    Set SR = Range("B" + Trim$(Str$(bLine)))
    Set FR = Range("B" + Trim$(Str$(bLine)) + ":O" + Trim$(Str$(bLine)))
    Set NFR = Range("B" + Trim$(Str$(bLine)) + ":O" + Trim$(Str$(bLine + pOffset)))
    Cells(bLine, 2) = "~~~"
    SR.AutoFill Destination:=FR, Type:=xlFillValues
    FR.HorizontalAlignment = xlCenter
    FR.AutoFill Destination:=NFR, Type:=xlFillValues
    NFR.HorizontalAlignment = xlCenter

DoNx:
    nbLine = bLine + pOffset + 1
    
' insert formulas ---------------------------------
    
   Worksheets(destSheet$).Activate
   totFORMULA$ = "=SUM(G" + Trim$(Str$(rBegin)) + ":G" + Trim$(Str$(bLine - 1)) + ")"
   Cells(nbLine, 7).Formula = totFORMULA$
   Cells(nbLine, 2).Value = "X"
   Cells(nbLine, 4).Value = "X"
   Cells(nbLine, 5).Value = "X"
   Cells(nbLine, 6).Value = "X"
   Cells(nbLine, 6).HorizontalAlignment = xlCenter
   DecoCell nbLine, 3, "�EK���:", True, , xlRight, , , , , 4, 10
   Cells(nbLine, 8).Value = "X"
   Cells(nbLine, 9).Value = bl$
   totFORMULA$ = "=SUM(J" + Trim$(Str$(rBegin)) + ":J" + Trim$(Str$(bLine - 1)) + ")"
   Cells(nbLine, 10).Formula = totFORMULA$
   Cells(2, 4).Value = Worksheets("form").Cells(4, 3).Value
    
   If TaxMode = 1 Then
       totFORMULA$ = "=SUM(K" + Trim$(Str$(rBegin)) + ":K" + Trim$(Str$(bLine - 1)) + ")"
       Cells(nbLine, 11).Formula = totFORMULA$
       Cells(nbLine, 12).Value = bl$
       Cells(nbLine, 13).Value = bl$
       Columns("L:M").HorizontalAlignment = xlCenter
       totFORMULA$ = "=SUM(N" + Trim$(Str$(rBegin)) + ":N" + Trim$(Str$(bLine - 1)) + ")"
       Cells(nbLine, 14).Formula = totFORMULA$
       GoTo Ns1
   End If
    
   If TaxMode = 2 Then
       totFORMULA$ = "=SUM(L" + Trim$(Str$(rBegin)) + ":L" + Trim$(Str$(bLine - 1)) + ")"
       Cells(nbLine, 12).Formula = totFORMULA$
       Cells(nbLine, 11).Value = bl$
       Cells(nbLine, 13).Value = bl$
       Cells(nbLine, 14).Value = "'0"
       Columns("M:O").HorizontalAlignment = xlCenter
       Columns("K:K").HorizontalAlignment = xlCenter
       GoTo Ns1
   End If
        
   If TaxMode = 3 Then
       totFORMULA$ = "=SUM(M" + Trim$(Str$(rBegin)) + ":M" + Trim$(Str$(bLine - 1)) + ")"
       Cells(nbLine, 13).Formula = totFORMULA$
       Cells(nbLine, 11).Value = bl$
       Cells(nbLine, 12).Value = bl$
       Cells(nbLine, 14).Value = "'0"
       Columns("N:O").HorizontalAlignment = xlCenter
       Columns("K:L").HorizontalAlignment = xlCenter
       GoTo Ns1
   End If
Ns1:
   totFORMULA$ = "=SUM(O" + Trim$(Str$(rBegin)) + ":O" + Trim$(Str$(bLine - 1)) + ")"
   Cells(nbLine, 15).Formula = totFORMULA$

End Sub

Public Function InsertTaxSheet() 'returning ConvRate
    
    Dim ConvRate As Integer, tempVal$, ws, tsName$, prevName$

Application.ScreenUpdating = False

   ConvRate = 1
   If CurrentCurrency() = 1 Then
       tempVal$ = InputBox("Current offer is made in USD !" + Chr$(10) + "Please enter 1$/AZM Rate:", "Convertion is Required...", "4500")
       If tempVal$ = "" Or "0" Then Exit Function
       ConvRate = Val(Trim$(tempVal))
   End If

   prevName$ = "TaxSheet"
   For Each ws In ActiveWorkbook.Worksheets
      If InStr(1, ws.Name, "TaxSheet") > 0 Then
         prevName$ = RealShName$("TaxSheet")
         ws.Delete
         Exit For
      End If
   Next
       
   ActiveWorkbook.Worksheets("Form").Activate
   Sheets.Add After:=Worksheets(Worksheets.Count)
   ActiveSheet.Name = prevName$
   ActiveWindow.DisplayGridlines = False
   ActiveSheet.Range("A1").Select
    
   ChDocName prevName$, "TaxSheet"
   tsName$ = RealShName$("TaxSheet")
   Worksheets(tsName$).Activate

   Columns("I:I").ColumnWidth = 0.92
   Columns("H:H").ColumnWidth = 11.29
   Columns("G:G").ColumnWidth = 11.86
   Columns("F:F").ColumnWidth = 13
   Columns("E:E").ColumnWidth = 8
   Columns("D:D").ColumnWidth = 5.71
   Columns("C:C").ColumnWidth = 35
   Columns("B:B").ColumnWidth = 5
   Columns("B:B").HorizontalAlignment = xlCenter
   Columns("A:A").ColumnWidth = 1.43
   Columns("D:H").HorizontalAlignment = xlCenter
   Rows("8:8").RowHeight = 15
   Rows("7:7").RowHeight = 51
   Rows("6:6").RowHeight = 15   '15
   Rows("5:5").RowHeight = 15.75 '13.5
   Rows("4:4").RowHeight = 16.5 '12.75
   Rows("3:3").RowHeight = 12.75 '6.75
   Rows("2:2").RowHeight = 12.75
   Rows("1:1").RowHeight = 75
   Columns("J:O").EntireColumn.Hidden = True
   Cells.Select
   Selection.VerticalAlignment = xlTop
   Range("A1").Activate
    
   Dim tsLeft As Single, tsTop As Single
   tsLeft = Round((Val(GetIniPar$("tsLeft")) / 2.54), 2)
   tsTop = Round((Val(GetIniPar$("tsTop")) / 2.54), 2)
    
On Error GoTo ErrHand
   With ActiveSheet.PageSetup
       Application.StatusBar = Application.StatusBar + "  (page setup)"
       .Orientation = xlPortrait
       .LeftMargin = Application.InchesToPoints(0.25 + tsLeft)
       .RightMargin = Application.InchesToPoints(0.25)
       .TopMargin = Application.InchesToPoints(0.25 + tsTop)
       .BottomMargin = Application.InchesToPoints(2.2)
       .HeaderMargin = Application.InchesToPoints(0.25)
       .FooterMargin = Application.InchesToPoints(0.25)
       .PaperSize = xlPaperA4
   End With
On Error GoTo 0
    
    InsertTaxSheet = ConvRate

Exit Function

ErrHand:
   MsgBox "Incorrect Page Margins, Check Settings!", vbExclamation, "(QF) Diagnostic..."
   Resume Next
  
End Function

Public Sub LinkTaxSheet(ConvRate)

    Dim destSheet$, SyncDATA$, SyncFORMULA$
    Dim SUMxROW As Integer, SUMxCOL As Integer, SyncConvDATA
    Dim dtLEN As Integer, SourceRow As Integer, i As Integer
    Dim rBegin As Integer, rEnd As Integer, rLines As Integer, xDP
    Dim totFORMULA$, selRNG$
    Dim II$, IssuedBy$
    Dim vatFormula$, dgwRANGE$, dgwFORMULA$, dgwOLrange$
    Dim SumFrmPos As Integer, VatSubMode As Integer
    Dim NewGTrange$, NewGTformula$

destSheet$ = RealShName$("TaxSheet")
Application.ScreenUpdating = False

    Worksheets("form").Activate
    rBegin = 9
    rEnd = GetLastRow()
    rLines = rEnd - FirstDataRow + 1
    selRNG$ = "B" + Trim$(Str$(rBegin)) + ":H" + Trim$(Str$(rEnd - 2))

    Worksheets(destSheet$).Activate
    Cells.Font.Name = "Times New Roman"
    Cells.Font.Bold = True

On Error Resume Next
    Range(selRNG$).Select
    Selection.WrapText = True
    Rows(RowsAddr$(rBegin, rEnd)).EntireRow.AutoFit
    AddGrid xlHairline
On Error GoTo 0

   SourceRow = FirstDataRow - 1
   VatSubMode = GetVATsubMode()
    
   For i = 1 To rLines
      With Worksheets(destSheet$)
        SyncDATA$ = Worksheets("Form").Cells(SourceRow + i, 2).Value
        .Cells(rBegin - 1 + i, 2).Value = SyncDATA$
        SyncDATA$ = Worksheets("Form").Cells(SourceRow + i, 3).Value
        .Cells(rBegin - 1 + i, 3).Value = SyncDATA$
        SyncDATA$ = Worksheets("Form").Cells(SourceRow + i, 4).Value
        .Cells(rBegin - 1 + i, 5).Value = SyncDATA$
        .Cells(rBegin - 1 + i, 6).Value = Round(Worksheets("Form").Cells(SourceRow + i, 5).Value * ConvRate, 0)
        SyncFORMULA$ = "=E" + Trim$(Str$(rBegin - 1 + i)) + "*F" + Trim$(Str$(rBegin - 1 + i))
        .Cells(rBegin - 1 + i, 7).Formula = SyncFORMULA$
        If VatSubMode = 0 Or VatSubMode = 2 Then
            SyncFORMULA$ = "0"
        Else
            SyncFORMULA$ = "=ROUND(G" + Trim$(Str$(rBegin - 1 + i)) + "/100*18" + ",0)"
        End If
        .Cells(rBegin - 1 + i, 8).Formula = SyncFORMULA$
      End With
   Next

   Worksheets(destSheet$).Activate
   II$ = Chr$(34)
   SumFrmPos = rEnd
   
   totFORMULA$ = "=SUM(G" + Trim$(Str$(rBegin)) + ":G" + Trim$(Str$(rEnd - 1)) + ")"
   vatFormula$ = "=SUM(H" + Trim$(Str$(rBegin)) + ":H" + Trim$(Str$(rEnd - 1)) + ")"
   dgwOLrange$ = "B" + Trim$(Str$(rEnd + 3)) + ":H" + Trim$(Str$(rEnd + 3))
    
   With Range(dgwOLrange$).Borders(xlEdgeTop)
       .LineStyle = xlDot
       .Weight = xlThin
       .ColorIndex = xlAutomatic
   End With
      
   DecoCell SumFrmPos, 6, "���� (AZM):", True, , xlRight, , , , , 3, 9
   DecoCell SumFrmPos, 7, totFORMULA$, True, , xlRight, True, True, , True
   DecoCell SumFrmPos + 1, 6, InvSuffix$(3, 1) + " (" + crSign$(2, 3) + "):", True, , xlRight, , , , , 3, 9
   DecoCell SumFrmPos + 1, 7, vatFormula$, True, , xlRight, True, True, , True
   Cells(SumFrmPos, 8).Value = "'=========="
   Cells(SumFrmPos + 1, 8).Value = "'=========="
   DecoCell SumFrmPos + 2, 6, InvSuffix$(3, 2) + " (" + crSign$(2, 3) + "):", True, , xlRight, , , , , 3, 9
   NewGTrange$ = GetRange$(SumFrmPos, 7) + ":" + GetRange$(SumFrmPos + 1, 7)
   NewGTformula$ = "=SUM(" + NewGTrange$ + ")"
   DecoCell SumFrmPos + 2, 7, NewGTformula$, True, , xlRight, True, True, , True
   Cells(SumFrmPos + 2, 8).Value = "'=========="

   dgwRANGE$ = "G" + Trim$(Str$(SumFrmPos + 2))
   dgwFORMULA$ = "=HowMuchIs(" + dgwRANGE$ + "," + II$ + "Manat" + II$ + "," + II$ + "A" + II$ + ")"
   IssuedBy$ = "    " + GetQFnameT$(DocumentOwner(), 30)
    
   DecoCell SumFrmPos + 3, 3, dgwFORMULA$, True, , xlLeft, True, , , , 3
   DecoCell 4, 3, IssuedBy$, True, , , , , , , 3
   DecoCell 6, 3, "........... , ���. XX N�. XXXXXX, " + Format(Date, "DD.MM.YY") + " , .............", True, , , , , , , 3
   DecoCell 8, 8, "'18% ���", True, , , , , , , 3

   PutDate 2, 7, 1
   Range("A1").Select
   Application.ScreenUpdating = True
    
End Sub

Public Sub TaxCrossCheck()
   
   Dim FrTotal As Currency, FrVat As Currency, FrVatSum As Currency
   Dim TiTotal As Currency, TiVat As Currency, TiVatSum As Currency
   Dim TsTotal As Currency, TsVat As Currency, TsVatSum As Currency
   Dim ShBits As Integer, HasTI As Integer, HasTS As Integer
   Dim mtTotal$, mtVat$, mtVatSum$, tmpCapt$, tmpIcon As Long
   Dim tmpLoc1 As Integer, mt$, tmpFoot$, tiName$, tsName$
   Dim VATmode As Integer, HasProblems As Boolean
   
   If CurrentCurrency() = 1 Then MsgBox "Can't Check USD/AZM Simultaneously !", vbInformation, "(QF) Cross Check": Exit Sub
   
   Worksheets("form").Activate
   SaveLocation True
      
   ShBits = SheetBits()
   HasTI = ShBits And 8
   HasTS = ShBits And 16
   VATmode = GetVATmode()
      
   FrTotal = Cells(FormulaRow(qsGT$), 6).Value
   mtTotal$ = FrTotal & "   ----- Main Form" & Chr$(10)
   If VATmode = 1 Then
      tmpLoc1 = FormulaRow("=AVERAGE(F")
      If tmpLoc1 = 0 Then GoTo sWrong
      FrVat = Cells(tmpLoc1 - 1, 6).Value
      FrVatSum = Cells(tmpLoc1, 6).Value
      mtVat$ = FrVat & "   ----- Main Form" & Chr$(10)
      mtVatSum$ = FrVatSum & "   ----- Main Form" & Chr$(10)
   End If
      
   If HasTI > 0 Then
      tiName$ = RealShName$("TaxInv")
      With Worksheets(tiName$)
        tmpLoc1 = FormulaRow("=SUM(G", tiName$, "G3:G200")
        If tmpLoc1 = 0 Then GoTo sWrong
        TiTotal = .Cells(tmpLoc1, 7).Value * 1000
        TiVatSum = .Cells(tmpLoc1, 15).Value * 1000
        mtTotal$ = mtTotal$ & TiTotal & "   ----- TaxInvoice" & Chr$(10)
        mtVatSum$ = mtVatSum$ & TiVatSum & "   ----- TaxInvoice" & Chr$(10)
        If VATmode = 1 Then
          TiVat = .Cells(tmpLoc1, 14).Value * 1000
          mtVat$ = mtVat$ & TiVat & "   ----- TaxInvoice" & Chr$(10)
        End If
      End With
        If FrTotal - TiTotal <> 0 Then HasProblems = True
        If VATmode = 1 Then
            If FrVat - TiVat <> 0 Then HasProblems = True
            If FrVatSum - TiVatSum <> 0 Then HasProblems = True
        End If
   End If
  
   If HasTS > 0 Then
      tsName$ = RealShName$("TaxSheet")
      With Worksheets(tsName$)
        tmpLoc1 = FormulaRow("=SUM(H", tsName$, "G13:G200")
        If tmpLoc1 = 0 Then GoTo sWrong
        TsTotal = .Cells(tmpLoc1 - 1, 7).Value
        TsVatSum = .Cells(tmpLoc1 + 1, 7).Value
        mtTotal$ = mtTotal$ & TsTotal & "   ----- TaxSheet" & Chr$(10)
        mtVatSum$ = mtVatSum$ & TsVatSum & "   ----- TaxSheet" & Chr$(10)
        If VATmode = 1 Then
          TsVat = .Cells(tmpLoc1, 7).Value
          mtVat$ = mtVat$ & TsVat & "   ----- TaxSheet" & Chr$(10)
        End If
      End With
        If FrTotal - TsTotal <> 0 Then HasProblems = True
        If VATmode = 1 Then
            If FrVat - TsVat <> 0 Then HasProblems = True
            If FrVatSum - TsVatSum <> 0 Then HasProblems = True
        End If
   End If
  
   If HasProblems = True Then
      tmpCapt$ = "(QF) Cross Check: PROBLEMS DETECTED !!"
      tmpFoot$ = "---------- DIFFERENCES DETECTED ------------"
      tmpIcon = vbExclamation
   Else
      tmpCapt$ = "(QF) Cross Check: Everything Clear."
      tmpIcon = vbOKOnly
      tmpFoot$ = "-------------- Everything Correct ---------------"
   End If
   
      mt$ = mt$ + "-- Total Comparision -----------------------------" + Chr$(10)
      mt$ = mt$ + mtTotal$ + Chr$(10)
   If VATmode = 1 Then
      mt$ = mt$ + "-- VAT Comparision ------------------------------" + Chr$(10)
      mt$ = mt$ + mtVat$ + Chr$(10)
      mt$ = mt$ + "-- VAT+Total Comparision ----------------------" + Chr$(10)
      mt$ = mt$ + mtVatSum$ + Chr$(10)
   End If
      mt$ = mt$ + tmpFoot$
   
   RestLocation True
   MsgBox mt$, tmpIcon, tmpCapt$
   
Exit Sub

sWrong:
   MsgBox "Wrong VAT Fromula(s) Detected !", vbCritical
   RestLocation True
End Sub
