Attribute VB_Name = "modContracts"
Option Explicit
Option Compare Text
Global Const MaxParLines = 50
Global WizardCanceled As Boolean
Global LPVstart$
Global LPVend$
Global WizardSteps As Integer
Global CurrentWizardStep As Integer
Global ParLines$(MaxParLines, 10)
Global ConCase$(MaxParLines, 10)
Global TotalParLines As Integer

Public Sub ContractsMC()
FixProc "modContracts.ContractsMC" '##-##'

If ErrHandMode = False Then GoTo NoErrHand
On Error GoTo GlobalErrorHandler

NoErrHand:
   Select Case CommandBars.ActionControl.Parameter
      Case "1"
         PrepEverything
      Case "2"
         RunWizard
      Case "3"
         doAtt
      Case "4"
         doInv
      Case "5"
         doAct
      Case "6"
         DoRenewAll
      Case "7"
         DoTaxSheet
      Case "8"
         DoTaxInvoice
      Case "9"
         TaxCrossCheck
   End Select

Exit Sub

GlobalErrorHandler:

   FixProc "#GlobalErrorHandler#:modContracts.ContractsMC"
   Select Case LogThisError()
      Case vbAbort
         End
      Case vbRetry
         Resume
      Case vbIgnore
         Resume Next
   End Select

End Sub

Public Sub PrepEverything()
FixProc "modContracts.PrepEverything" '##-##'
    RunWizard
    doAtt
    doInv
End Sub

Public Sub RunWizard()
FixProc "modContracts.RunWizard" '##-##'
    qReConnect
    If preVerify(True) = False Then Exit Sub
    RequestCTemplate
    Erase ParLines$, ConCase$
    InitParLines
    CurrentWizardStep = 1
    WizardUpdate
    Application.ScreenUpdating = True
    Load iWIZARD
    iWIZARD.Show
End Sub

Private Sub DoCell(xRow, xCol, shName$)
FixProc "modContracts.DoCell" '##-##'

    Dim LangMode As Integer, i As Integer, DraftData$, tag$
        
    LangMode = CurrentLang()
    DraftData$ = Workbooks(QFCmodName$).Worksheets(ConTemplate$(LangMode)).Cells(xRow, xCol)
    If Trim$(DraftData$) = "" Then Worksheets(shName$).Cells(xRow, xCol) = "": Exit Sub

    For i = 1 To TotalParLines
        tag$ = ParLines$(i, 3)
        DraftData$ = Replace(DraftData$, tag$, ParLines$(i, 4), 1, -1, vbBinaryCompare)
    Next i
    
    Worksheets(shName$).Cells(xRow, xCol) = DraftData$

End Sub

Private Sub InitParLines()
FixProc "modContracts.InitParLines" '##-##'

   Dim LangMode As Integer, CurrMode As Integer
   Dim i As Integer, SkipTo As Integer
   Dim xCol As Integer, xRow As Integer
   Dim xRange$, errCount As Integer
       
   LangMode = CurrentLang()
   CurrMode = CurrentCurrency()
   
   i = 2
   On Error GoTo ErrHand98
R: While Workbooks(QFCmodName$).Worksheets("con_set").Cells(i, 1) <> "$$end$$"
   On Error GoTo 0
   
       If Trim$(Workbooks(QFCmodName$).Worksheets("con_set").Cells(i, 4 + LangMode)) = "SKIP" Then
           SkipTo = SkipTo - 1
           GoTo wnd
       End If

      With Workbooks(QFCmodName$).Worksheets("con_set")
        ParLines$(i - 1 + SkipTo, 1) = Trim$(.Cells(i, 1).Value) 'MessageEN
        ParLines$(i - 1 + SkipTo, 2) = Trim$(.Cells(i, 2).Value) 'MessageRU
        ParLines$(i - 1 + SkipTo, 3) = Trim$(LCase$(.Cells(i, 3).Value)) 'TAG
        ParLines$(i - 1 + SkipTo, 5) = Trim$(.Cells(i, 4).Value) 'Type
        ParLines$(i - 1 + SkipTo, 8) = Trim$(.Cells(i, 8).Value) 'SKIP MODE
        
        If ParLines$(i - 1 + SkipTo, 3) = "<<no>>" Then
            ParLines$(i - 1 + SkipTo, 4) = TrackGetNo(qfServer$ + ConCountFN$) + 1
            GoTo NextCycle
        End If
        
        If ParLines$(i - 1 + SkipTo, 5) = "DYN" Then
            xRange$ = Trim$(.Cells(i, 4 + LangMode))
            xRow = RowAddr(xRange$)
            xCol = ColAddr(xRange$)
            ParLines$(i - 1 + SkipTo, 4) = Worksheets("Form").Cells(xRow, xCol).Value
            GoTo NextCycle
        End If
                
        If ParLines$(i - 1 + SkipTo, 5) = "STAT" Then
            If ParLines$(i - 1 + SkipTo, 4) <> "" Then GoTo NextCycle
            ParLines$(i - 1 + SkipTo, 4) = Trim$(.Cells(i, 4 + LangMode))
            CollectConCase i, SkipTo
            GoTo NextCycle
        End If
     End With
        
        If ParLines$(i - 1 + SkipTo, 5) = "ABS" Then
         With Workbooks(QFCmodName$).Worksheets("con_set").Cells(i, 6)
            If .Value = "Total Amount" Then GoSub GetTotal: GoTo NextCycle
            If .Value = "DigiWords" Then GoSub GetDigiwords: GoTo NextCycle
            If .Value = "Currency Type" Then GoSub GetCurrency: GoTo NextCycle
         End With
        End If
        
NextCycle:

        If Trim$(ParLines$(i - 1 + SkipTo, 4)) = "" Then ParLines$(i - 1, 4) = String$(50, "_")
wnd:
        i = i + 1
    Wend
        
        WizardSteps = i - 2
        TotalParLines = WizardSteps + SkipTo
Exit Sub

'------ GoSubs --------

    Dim SUMxROW As Integer, SUMxCOL As Integer
    Dim xDWRow As Integer, xDWCol As Integer

GetTotal:

    If GetVATmode() = 1 Then
        SUMxROW = FormulaRow(qsVAT$) + 1
    Else
        SUMxROW = FormulaRow(qsGT$)
    End If
    
    SUMxCOL = FormulaCol
    If SUMxROW = 0 Or SUMxCOL = 0 Then ParLines$(i - 1, 4) = "": Return
    ParLines$(i - 1, 4) = Worksheets("Form").Cells(SUMxROW, SUMxCOL).Value
Return

GetDigiwords:
    xDWRow = FormulaRow(qsDIGI$)
    xDWCol = FormulaCol
    If xDWCol = 0 Or xDWRow = 0 Then ParLines$(i - 1, 4) = "": Return
    ParLines$(i - 1, 4) = Trim$(Worksheets("Form").Cells(xDWRow, xDWCol).Value)
Return

'---------------------
GetCurrency:
    ParLines$(i - 1, 4) = CurrWords$(LangMode, CurrMode)
Return

ErrHand98:
  On Error GoTo 0
  errCount = errCount + 1
  If errCount > 3 Then GoTo BadDay
  qMsg 1, vbExclamation
  RequestCTemplate
  GoTo R

BadDay:
  qMsg 2, vbCritical
  End

End Sub

Public Sub WizardNext()
FixProc "modContracts.WizardNext" '##-##'

    Dim NewData$
    ParLines$(CurrentWizardStep, 4) = Trim$(iWIZARD.MyAnswer.Text)
    
   If CurrentWizardStep = WizardSteps Then ' wizard finished
     iWIZARD.Hide
     Unload iWIZARD
     doCon
     With Worksheets("Form")
      NewData$ = GetTagData("<<no>>")
      If NewData$ <> "" Then .Cells(2, 3).Value = NewData$
      NewData$ = GetTagData("<<date>>")
      If NewData$ <> "" Then .Cells(6, 3).Value = NewData$
      NewData$ = GetTagData("<<purchaser>>")
      If NewData$ <> "" Then .Cells(4, 3).Value = NewData$
      .Range("C4:C7").NumberFormat = "@"
      .Range("F7").NumberFormat = "@"
     End With
     Exit Sub
   End If
    
CheckSkip:
    CurrentWizardStep = CurrentWizardStep + 1
    If ParLines$(CurrentWizardStep, 8) = "SKIP" Then GoTo CheckSkip
    WizardUpdate

End Sub

Public Sub WizardPrev()
FixProc "modContracts.WizardPrev" '##-##'

    If CurrentWizardStep <= 1 Then WizardUpdate: CurrentWizardStep = 1: Exit Sub
    CurrentWizardStep = CurrentWizardStep - 1
    WizardUpdate

End Sub

Public Sub WizardCancel()
FixProc "modContracts.WizardCancel" '##-##'
    CurrentWizardStep = 1
    iWIZARD.Hide
    Unload iWIZARD
End Sub

Public Sub WizardUpdate()
FixProc "modContracts.WizardUpdate" '##-##'

    Dim LangMode As Integer, CurrMode As Integer

    LangMode = CurrentLang()
    CurrMode = CurrentCurrency()
    
  With iWIZARD
    
    .Caption = "Contract Wizard, Mode: " + ModeNames$(CurrentLang, CurrentCurrency)
    If CurrentWizardStep = WizardSteps Then .CB_Next.Caption = " ~Finish~ " Else .CB_Next.Caption = "Next "
    If CurrentWizardStep = 1 Then .CB_Prev.Enabled = False Else .CB_Prev.Enabled = True
    
    .L_EN.Caption = ParLines$(CurrentWizardStep, 1)
    .L_RU.Caption = ParLines$(CurrentWizardStep, 2)
    .MyAnswer.Text = ParLines$(CurrentWizardStep, 4)

    UpdateCases CurrentWizardStep

    InitLPV
    UpdateLPV
    .MyAnswer.SetFocus
  
  End With

End Sub

Private Sub DoStyle(xRow, xCol, shName$)
FixProc "modContracts.DoStyle" '##-##'
   
   Dim styleTag$, LangStyleTag$, LangMode As Integer
   LangMode = CurrentLang()
   styleTag$ = Trim$(LCase$(Workbooks(QFCmodName$).Worksheets(ConTemplate$(LangMode)).Cells(xRow, xCol + 1)))
   If styleTag$ = "" Then Exit Sub
   LangStyleTag$ = styleTag$ + Trim$(Str$(LangMode))
   Worksheets(shName$).Range(GetRange$(xRow, xCol)).Style = LangStyleTag$
    
End Sub

Private Sub CollectConCase(ParStep, SkipTo)
FixProc "modContracts.CollectConCase" '##-##'

   Const FisrtCaseCol As Integer = 8 '(-1 for add langNo count)
   Dim i As Integer
    
   If ParLines$(ParStep - 1 + SkipTo, 8) = "NOCASE" Then
       ConCase$(ParStep - 1 + SkipTo, 0) = ""
       Exit Sub
   End If

   With Workbooks(QFCmodName$).Worksheets("con_set")
    
   If Trim$(.Cells(ParStep, FisrtCaseCol + 1).Value) = "" Then
       ConCase$(ParStep - 1 + SkipTo, 0) = ""
       Exit Sub
   End If
   
   For i = 1 To 3
        ConCase$(ParStep - 1 + SkipTo, (3 * i + 1) - 3) = Trim$(.Cells(ParStep, (FisrtCaseCol + (3 * i + 1) - 3)).Value)
        ConCase$(ParStep - 1 + SkipTo, (3 * i + 2) - 3) = Trim$(.Cells(ParStep, (FisrtCaseCol + (3 * i + 2) - 3)).Value)
        ConCase$(ParStep - 1 + SkipTo, (3 * i + 3) - 3) = Trim$(.Cells(ParStep, (FisrtCaseCol + (3 * i + 3) - 3)).Value)
        If ConCase$(ParStep - 1 + SkipTo, (3 * i + 1) - 3) <> "" Then ConCase$(ParStep - 1 + SkipTo, 0) = Trim$(Str$(i))
   Next
   
   End With

End Sub

Private Sub UpdateCases(WizStep)
FixProc "modContracts.UpdateCases" '##-##'

    Dim Opts$
    Opts$ = ConCase$(WizStep, 0)
    
 With iWIZARD
  .cOriginal.Value = True
  .cOriginal.Enabled = False
  .c1.Enabled = False
  .c2.Enabled = False
  .c3.Enabled = False
    
   Select Case Opts$
       Case "1"
         .cOriginal.Enabled = True
         .c1.Enabled = True
      Case "2"
         .cOriginal.Enabled = True
         .c1.Enabled = True
         .c2.Enabled = True
      Case "3"
         .cOriginal.Enabled = True
         .c1.Enabled = True
         .c2.Enabled = True
         .c3.Enabled = True
    End Select
  End With

End Sub

Public Sub CaseChanged()
FixProc "modContracts.CaseChanged" '##-##'

    Dim LangMode As Integer
    
    LangMode = CurrentLang()
    ConCase$(CurrentWizardStep, 10) = ParLines$(CurrentWizardStep, 4)
       
   With iWIZARD
      If .cOriginal.Value = True Then .MyAnswer.Text = ConCase$(CurrentWizardStep, 10)
      If .c1.Value = True Then .MyAnswer.Text = ConCase$(CurrentWizardStep, LangMode)
      If .c2.Value = True Then .MyAnswer.Text = ConCase$(CurrentWizardStep, 3 + LangMode)
      If .c3.Value = True Then .MyAnswer.Text = ConCase$(CurrentWizardStep, 6 + LangMode)
      .MyAnswer.SetFocus
   End With

End Sub

Public Sub UpdateLPV()
FixProc "modContracts.UpdateLPV" '##-##'
    If iWIZARD.eLPV.Value = False Then Exit Sub
    iWIZARD.LPV.Caption = LPVstart$ + iWIZARD.MyAnswer.Text + LPVend$
End Sub

Public Sub InitLPV()
FixProc "modContracts.InitLPV" '##-##'

    Dim tag$, cellData$, CurrentLine As Integer, LangMode As Integer
    Dim TagLen As Integer, TagPos As Integer
    
    If iWIZARD.eLPV.Value = False Then Exit Sub
    
    LPVstart$ = ""
    LPVend$ = ""
    tag$ = ParLines$(CurrentWizardStep, 3)
    CurrentLine = 1
    LangMode = CurrentLang()
    
    While cellData$ <> "$$end$$"
        cellData$ = Workbooks(QFCmodName$).Worksheets(ConTemplate$(LangMode)).Cells(CurrentLine, 2)
        TagLen = Len(tag$)
        TagPos = InStr(cellData$, tag$)
        If TagPos = 0 Then GoTo wnd
        If Len(Trim$(tag$)) = Len(Trim$(cellData$)) Then
            LPVstart$ = ""
            LPVend$ = ""
            Exit Sub
        End If

        LPVstart$ = Left$(cellData$, TagPos - 1)
        LPVend$ = Mid$(cellData$, TagPos + TagLen)
        Exit Sub
        
wnd:    CurrentLine = CurrentLine + 1
    Wend

        LPVstart$ = "[ NOT FOUND ]"
        LPVend$ = "[ TAG: " + tag$ + " ]"

End Sub

Private Sub TunePages()
FixProc "modContracts.TunePages" '##-##'
    
    Dim CoCycles As Integer, i As Integer, pbLoc As Integer
    Dim pbLocRng$, shName$, RowAbsolute, ColumnAbsolute, z As Integer
  
  shName$ = RealShName$("Contract")
    
  Worksheets(shName$).Activate
  ActiveWindow.View = xlPageBreakPreview
  CoCycles = 1

ReqRestart:
   
    If CoCycles > 10 Then GoTo GetOut
    For i = 1 To ActiveSheet.HPageBreaks.Count
        pbLocRng$ = ActiveSheet.HPageBreaks(i).Location.Address(RowAbsolute, ColumnAbsolute)
        pbLoc = Val(Mid$(pbLocRng$, 2))
        CoCycles = CoCycles + 1
        For z = 1 To 4
           If ActiveSheet.Cells(pbLoc - z, 2).Font.Size > 10 Then
                Set ActiveSheet.HPageBreaks(i).Location = Range("A" + Trim$(Str$(pbLoc - z)))
                GoTo nx1
            End If
        Next
nx1:
    Next

GetOut:
  ActiveWindow.View = xlNormalView
  
End Sub

Private Sub InsertContract()
FixProc "modContracts.InsertContract" '##-##'
   AddDocFromTemplate "Contract", ""
   Columns("A:A").ColumnWidth = 0.33
   Columns("B:B").ColumnWidth = 90
End Sub

Private Function GetTagData$(tagId$)
FixProc "modContracts.GetTagData$" '##-##'

   Dim i As Integer
   For i = 1 To TotalParLines
       If Trim$(UCase$(ParLines$(i, 3))) = Trim$(UCase$(tagId$)) Then
           GetTagData$ = ParLines$(i, 4)
           Exit Function
       End If
   Next
   GetTagData$ = ""

End Function

Private Sub doCon()
FixProc "modContracts.doCon" '##-##'

   Dim LangMode As Integer, CurrentLine As Integer, shName$, newNo As Integer
   
   If preVerify(True) = False Then Exit Sub
   
   Application.ScreenUpdating = False
   Application.StatusBar = "(QF) Please Wait, Writing Contract..."
   InsertContract
   shName$ = RealShName$("Contract")
   Worksheets(shName$).Activate
   DoPageSetup
   SetConStyles
   Worksheets(shName$).Activate
   Worksheets(shName$).Range("A1:C100").Select
   Selection.Cells = ""
   Selection.Style = "Normal"
   LangMode = CurrentLang()
   CurrentLine = 1
       
   While Workbooks(QFCmodName$).Worksheets(ConTemplate$(LangMode)).Cells(CurrentLine, 2) <> "$$end$$"
       Application.StatusBar = "(QF) Contract Creation: Writing Paragraph..." + Str$(CurrentLine)
       DoCell CurrentLine, 2, shName$
       DoStyle CurrentLine, 2, shName$
       CurrentLine = CurrentLine + 1
   Wend
   Worksheets(shName$).Activate
   Rows("1:200").EntireRow.AutoFit

   Application.StatusBar = "(QF) Contract Creation: Tunning Page Breaks..."
   TunePages
   Worksheets(shName$).Activate
   AddDocHist shName$
   newNo = TrackGetNo(qfServer$ + ConCountFN$) + 1
   ActiveSheet.Name = "Contract-" & newNo
   Worksheets(FormTab$).Cells(2, 3).Value = newNo
   Range("B1").Select
   Application.ScreenUpdating = True
   Application.StatusBar = ""
    
End Sub

Private Function ModeNames$(curr As Integer, lang As Integer)
   Dim tmp As Integer, ModeNamesInt$(1 To 3, 1 To 2, 1 To 2) ' language , currency, currTag
   ModeNamesInt$(1, 1, 1) = "English / USD"
   ModeNamesInt$(1, 1, 2) = "English / EURO"
   ModeNamesInt$(1, 2, 1) = "English / AZM"
   ModeNamesInt$(1, 2, 2) = "English / AZM"
   ModeNamesInt$(2, 1, 1) = "Russian / USD"
   ModeNamesInt$(2, 1, 2) = "Russian / EURO"
   ModeNamesInt$(2, 2, 1) = "Russian / AZM"
   ModeNamesInt$(2, 2, 2) = "Russian / AZM"
   ModeNamesInt$(3, 1, 1) = "Azeri / USD"
   ModeNamesInt$(3, 1, 2) = "Azeri / EURO"
   ModeNamesInt$(3, 2, 1) = "Azeri / AZM"
   ModeNamesInt$(3, 2, 2) = "Azeri / AZM"
   If curr = 2 Then tmp = 1 Else tmp = CurrTag()
   ModeNames$ = ModeNamesInt$(curr, lang, tmp)
End Function

Private Function CurrWords$(curr As Integer, lang As Integer)
   Dim tmp As Integer, CurrWordsInt$(1 To 3, 1 To 2, 1 To 2) ' language , currency, currTag
   CurrWordsInt$(1, 1, 1) = "US Dollars"
   CurrWordsInt$(1, 1, 2) = "EURO"
   CurrWordsInt$(1, 2, 1) = "Azerbaijan's Manats"
   CurrWordsInt$(1, 2, 2) = "Azerbaijan's Manats"
   CurrWordsInt$(2, 1, 1) = "�������� ���"
   CurrWordsInt$(2, 1, 2) = "����"
   CurrWordsInt$(2, 2, 1) = "� ������� ��������������� ����������"
   CurrWordsInt$(2, 2, 2) = "� ������� ��������������� ����������"
   CurrWordsInt$(3, 1, 1) = "AB� Dollari"
   CurrWordsInt$(3, 1, 2) = "EURO"
   CurrWordsInt$(3, 2, 1) = "���������� ��������������� ���������"
   CurrWordsInt$(3, 2, 2) = "���������� ��������������� ���������"
   If curr = 2 Then tmp = 1 Else tmp = CurrTag()
   CurrWords$ = CurrWordsInt$(curr, lang, tmp)
End Function

Private Sub SetConStyles()
FixProc "modContracts.SetConStyles" '##-##'
   
   Dim st, i As Integer
   For Each st In ActiveWorkbook.Styles
       If Left$(st.Name, 1) = "<" Then st.Delete
   Next

' Style <n> ------------------------------------
   For i = 1 To 3
       ActiveWorkbook.Styles.Add Name:="<n>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<n>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 10
           .Font.Bold = False
           .Font.Italic = False
           .HorizontalAlignment = xlJustify
           .WrapText = True
       End With
' Style <b> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<b>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<b>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 10
           .Font.Bold = True
           .Font.Italic = False
           .HorizontalAlignment = xlJustify
           .WrapText = True
       End With
' Style <h1> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<h1>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<h1>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 16
           .Font.Bold = True
           .Font.Italic = False
           .HorizontalAlignment = xlCenter
           .WrapText = True
       End With
' Style <h2> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<h2>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<h2>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 12
           .Font.Bold = True
           .Font.Italic = False
           .HorizontalAlignment = xlCenter
           .WrapText = True
       End With
' Style <i> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<i>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<i>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 11
           .Font.Bold = True
           .Font.Italic = True
           .HorizontalAlignment = xlRight
           .WrapText = True
       End With
' Style <ls> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<ls>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<ls>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .IncludeBorder = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 10
           .Font.Bold = False
           .Font.Italic = False
           .HorizontalAlignment = xlLeft
           .Borders(xlBottom).LineStyle = xlContinuous
           .Borders(xlBottom).Weight = xlHairline
           .Borders(xlBottom).ColorIndex = 1
           .WrapText = True
       End With
' Style <rs> ------------------------------------
       ActiveWorkbook.Styles.Add Name:="<rs>" + Trim$(Str$(i))
       With ActiveWorkbook.Styles("<rs>" + Trim$(Str$(i)))
           .IncludeFont = True
           .IncludeAlignment = True
           .IncludeBorder = True
           .Font.Name = xcFNT$(i)
           .Font.Size = 10
           .Font.Bold = False
           .Font.Italic = False
           .HorizontalAlignment = xlRight
           .Borders(xlTop).LineStyle = xlContinuous
           .Borders(xlTop).Weight = xlHairline
           .Borders(xlTop).ColorIndex = 1
           .WrapText = True
       End With
   Next
End Sub

Public Sub RequestCTemplate()
FixProc "modContracts.RequestCTemplate" '##-##'
    Load iRCT: iRCT.Show
End Sub
